@echo off
rem ShowSelf

Title Actualizar sistema
mode CON COLS=65 LINES=25
color 0F
cd C:\entorno\simse\Scripts
call activate.bat
cd C:\simse

echo __________________________________________________
echo Descargando cambios desde internet
echo __________________________________________________
git status
git pull
echo __________________________________________________
echo Aplicando cambios
cd simse
python3 manage.py migrate
python3 manage.py actualizar
python manage.py migrate
python manage.py actualizar
echo __________________________________________________


pause 
exit