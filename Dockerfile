FROM python:3.6.2-alpine3.6
ARG REQUIREMENTS
RUN apk update
RUN apk add --no-cache \
        gcc \
        postgresql-dev \
        postgresql-client \        
        bash \
        zlib-dev \
        jpeg-dev \
        linux-headers \
        mc util-linux \
        file

RUN apk add --no-cache musl-dev

RUN mkdir /code
ADD ./ /code/
WORKDIR /code
RUN pip3.6 install -r requirements/development.txt
EXPOSE 80:80