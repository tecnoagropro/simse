@ECHO OFF
FOR /F "delims=" %%A IN (.env) DO CALL:Tratar "%%A"
GOTO:EOF

:Tratar
   SET Reg=%~1

	for /f "tokens=1,2,3 delims= " %%a in ("%Reg%") do (
	  rem if defined ruta goto :EOF
	  set BEFORE_UNDERSCORE=%%a
	  set AFTER_UNDERSCORE=%%b
	  set ruta=%%c
	  echo %ruta%
	  rem if "%BEFORE_UNDERSCORE%"  EQU  "VIRTUALENV" goto:si
	  CALL %ruta%\Scripts\activate.bat
	  GOTO:EOF
	)
   GOTO:EOF

:si 
	rem ECHO %ruta%

:EOF
	rem exit