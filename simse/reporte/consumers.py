import json
import logging
from channels import Channel
from channels.sessions import channel_session

log = logging.getLogger(__name__)


@channel_session
def ws_connect(message):
   message.reply_channel.send({
     "text": json.dumps({
       "action": "reply_channel",
       "reply_channel": message.reply_channel.name,
      })
    })

@channel_session
def ws_receive(message):
    
  try:
     data = json.loads(message["text"])
  except ValueError:
     log.debug('ws message isn"t json text=%s', message["text"])
     return

  if data:
    reply_channel = message.reply_channel.name
    Channel(reply_channel).send({
        "text": json.dumps ({
            "action": "completed",
            "content": "short_process"
        })