from django.conf.urls import include, url
from django.views.decorators.csrf import csrf_exempt

from common.decorators import Token

from .views import *
from .ajax import *

urlpatterns = [
    url(r'^offline.appcache$', app_cache, name="app_cache"),  
    url(r'^reporte/listar$', Token(lista_reportes), name="lista_reportes"),
    url(r'^reporte/cierre$', Token(cierre), name="cierre"),
    url(r'^reporte/administrador_ventas$', administrador_ventas, name="administrador_ventas"),
    url(r'^reporte/ventas_detallado$', ventas_detallado, name="ventas_detallado"),
    
    url(r'^reporte/detalle/cierre/(?P<pk>[0-9\.]+)/$', detalle_cierre, name="detalle_cierre"),
    url(r'^reporte/detalle/cierre_cliente/(?P<pk>[0-9\.]+)/$', cierre_cliente, name="cierre_cliente"),

    url(r'^ajax/listar_reporte_local$', csrf_exempt(ListarReporteLocal.as_view()), name="listar_reporte_local_ajax"),
    url(r'^ajax/listar_reporte_local_encargado/(?P<pk>[0-9\.]+)/$', csrf_exempt(ListarReporteLocalEncargado.as_view()),
    	name="listar_reporte_local_encargado_ajax"),

    url(r'^ajax/listar_reporte_cierre$', csrf_exempt(ListarCierres.as_view()),
        name="listar_reporte_cierre_ajax"),

    url(r'^ajax/listar_reporte_cierre_encargado/(?P<pk>[0-9\.]+)/$', csrf_exempt(ListarCierresEncargado.as_view()),
        name="listar_reporte_cierre_encargado_ajax"),
    url(r'^ajax/detalle_reporte_cierre_encargado/(?P<pk>[0-9\.]+)/$', csrf_exempt(ListarCierreDetalle.as_view()),
        name="detalle_reporte_cierre_encargado_ajax"),

    url(r'^ajax/dcc/(?P<pk>[0-9\.]+)/$', csrf_exempt(ListarCierreClienteDetalle.as_view()),
        name="detalle_reporte_cierre_cliente_ajax"),



    url(r'^ajax/listar_reporte_venta_detalle$', csrf_exempt(ListarReporteVentaDetalle.as_view()),
        name="listar_reporte_venta_detalle_ajax"),


]

