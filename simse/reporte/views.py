from django.shortcuts import render
from django.http import JsonResponse, HttpResponse

from django.shortcuts import get_object_or_404
from django.contrib.auth.decorators import permission_required


def app_cache(request):
    c=open('offline.txt','r')
    return HttpResponse(c.read(), content_type='text/cache-manifest')


def dias_trabajo_empleados(request):
    return render(request, 'dias_trabajo_empleados.html', {})


def reporte_encargado(request, pk):
    context = {
        'temporada': pk
    }
    return render(request,'reporte_encargado.html',context)


def administrador_ventas(request):
    return render(request, 'administrador_ventas_reporte.html', {})

def ventas_detallado(request):
    return render(request, 'listar_ventas_cliente_detallado.html', {})


def cierre(request):
    return render(request,'listar_cierre.html',{}) 

def lista_reportes(request):
    return render(request,'lista_reportes.html',{}) 

def detalle_cierre(request,pk):
    return render(request,'detalle_cierre.html',{'pk':pk}) 


def cierre_cliente(request,pk):
    return render(request,'detalle_cierre_cliente.html',{'pk':pk}) 