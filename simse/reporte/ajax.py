from django.http import JsonResponse
from common.mixins import DataTable, Botones
from django.db.models import F, Count, Sum 
from accounts.models import Persona
from encargado.models import (Encargado, VentaClienteProducto, CierreEncargado,
DetalleCierreProducto, DetalleCierreCliente, DetalleCierre, Cliente)
from inventario.models import Producto, AsignacionStock, Stock


class ListarReporteLocal(DataTable):
    default_context = False
    model = Producto
    columns = ['producto','cant_vendida','cant_disponible',
               'recaudado','venta']
    order_columns = columns
    max_display_length = 1000

    def prepare_results(self, qs):
        qs = self.get_queryset()
        json_data = []
        for item in qs:
            if item['cant_vendida'] > 0:
                json_data.append([
                    str(item['nombre']),
                    str(item['cant_vendida']),
                    str(item['venta']),
                    str(item['cant_disponible']), 
                    str(item['pendiente_venta']),
                    
                ])
        return json_data  


    def get_queryset(self):
        list = []
        productos = Producto.objects.activos()
        search = self.request.POST.get(u'search[value]', None)

        if search:
            productos = Producto.objects.activos(nombre__icontains = search)

        for producto in productos:
            cant_vendida = 0
            cant_disponible = 0
            venta = 0

            try:

                encargado = Encargado.objects.get(pk=self.pk)
                vp = VentaClienteProducto.objects.activos(stock__stock__producto=producto,
                                                          venta__encargado=encargado)
                cant_vendida = vp.aggregate(suma=Sum('cantidad'))['suma']
                venta = vp.aggregate(suma=Sum('monto__monto'))['suma']
                cant_disponible = AsignacionStock.objects.activos(stock__producto=producto,
                                                 encargado=encargado,aceptado=True)\
                                  .aggregate(suma=Sum('cantidad_actual'))['suma']

            except (Encargado.DoesNotExist, AttributeError) as e:
                vp = VentaClienteProducto.objects.activos(stock__stock__producto=producto)
                cant_vendida = vp.aggregate(suma=Sum('cantidad'))['suma']
                venta = vp.aggregate(suma=Sum('monto__monto'))['suma']
                cant_disponible = AsignacionStock.objects.activos(stock__producto=producto,
                                                                  aceptado=True)\
                                  .aggregate(suma=Sum('cantidad_actual'))['suma']

            try:
                pendiente_venta = producto.mi_costo_actual * cant_disponible
            except TypeError as e:
                pendiente_venta = 0
                

            producto_dict = {
                'nombre':producto.nombre,
                'cant_vendida':cant_vendida if cant_vendida else 0,
                'cant_disponible':cant_disponible if cant_vendida else 0,
                'pendiente_venta':pendiente_venta if pendiente_venta else 0,
                'venta':venta if venta else 0
            }
            list.append(producto_dict)

        return list

    def filter_queryset(self, qs):
        return qs    

    def get_initial_queryset(self):
        return self.model.objects.activos()


class ListarReporteLocalEncargado(ListarReporteLocal):
    pass



class ListarCierres(DataTable):
    model = CierreEncargado
    columns = ['fecha_inicio','fecha_fin','encargado.persona.primer_nombre',
               'total.monto','diferencia','total_neto','botones']
    order_columns = columns
    max_display_length = 1000

    def render_column(self, row, column):
        if column == 'fecha_inicio':
            return row.fecha_inicio.strftime("%d-%m-%Y")

        elif column == 'fecha_fin':  
            return row.fecha_fin.strftime("%d-%m-%Y")

        elif column == 'botones':
            btn = Botones()   
            btn.href('#/reporte/detalle/cierre/'+str(row.id),'success','chart-bar')   
            btn.click("eliminar_dato('"+str(row.id)+"')" , 'danger', 'recycle')   
            return str(btn)

        elif column == 'total_neto':
            deuda = (row.total.monto - row.pagado.monto)
            try:
                total_neto = row.total.monto - (deuda + row.pago_encargados.monto)
            except AttributeError as e:
                total_neto = row.total.monto - (deuda)
            return str(total_neto)   

        elif column == 'diferencia':
            if row.pagado and row.total:
                r = row.total.monto - row.pagado.monto
            elif row.total:
                r = row.total.monto 
            else:
                r = 0
            return str(r)

        else:
           return super(ListarCierres, self).render_column(row, column)

    def filter_queryset(self, qs):
        search = self.request.POST.get(u'search[value]', None)
        if search:
            qs = qs
          
        return qs                

    def get_initial_queryset(self):
        query = self.model.objects.filter(condicion=True)
        try:
            encargado = Encargado.objects.get(pk=self.pk)
            return query.filter(encargado=encargado)
        except:
            return query



class ListarCierresEncargado(ListarCierres):
    pass     


class ListarCierreDetalle(DataTable):
    model = DetalleCierreProducto
    columns = ['producto.nombre','cantidad', 'venta.monto','botones']
    order_columns = columns
    max_display_length = 1000

    # def filter_queryset(self, qs):
    #     search = self.request.POST.get(u'search[value]', None)
    #     if search:
    #         qs = qs
    #     #print (qs.distinct('cierre__venta__encargado_id').order_by())
    #     return qs
  

    def get_initial_queryset(self):
        query = self.model.objects.activos(cierre=self.pk)
        return query

    def render_column(self, row, column):
        if column == 'botones':
            btn = Botones() 
            btn.click("ventana_clientes('/reporte/detalle/cierre_cliente/"+str(row.id)+"/')" , 'success', 'chart-bar')
            return btn()
        else:
           return super(ListarCierreDetalle, self).render_column(row, column)


class ListarCierreClienteDetalle(DataTable):
    model = DetalleCierreCliente
    columns = ['cliente.nombre','cliente.telefono', 'venta.monto', 'pendiente.monto']
    order_columns = columns
    max_display_length = 1000

    def get_initial_queryset(self):
        query = self.model.objects.activos(cierre=self.pk)
        return query


class ListarReporteVentaDetalle(DataTable):
    model = Cliente
    columns = ['nombre','cantidad_productos','monto_productos','deudas','detalles']
    order_columns = columns
    max_display_length = 1000


    def filter_queryset(self, qs):
        search = self.request.POST.get(u'search[value]', None)
        if search:
            qs = qs
        return qs

    def get_initial_queryset(self):
        query = self.model.objects.activos()
        return query

    def render_column(self, row, column):

        vp = VentaClienteProducto.objects.activos(venta__cliente__pk=row.id,
             created__range=[self.start,self.end])

        if column == 'cantidad_productos':

            productos = []
            for v in vp:
                if v.stock.stock.producto.pk not in productos:
                    productos.append(v.stock.stock.producto.pk)

            return str(len(productos))

        elif column == 'monto_productos': 
            venta = vp.aggregate(suma=Sum('monto__monto'))['suma']
            if not venta:
                return str(0)
            return str(venta)

        elif column == 'deudas':

            ventas = []
            for v in vp:
                if v.venta not in ventas:
                    ventas.append(v.venta)

            deuda = 0
            for v in ventas:
                if not v.esta_pagada:
                    deuda += v.restante

            return str(deuda)

        elif column == 'detalles':
            btn = Botones()   
            url = '#/encargado/ventas/listar_cliente/iframe/'+str(row.id)
            btn.click("modal('"+str(url)+"')",'success','chart-bar')               
            return str(btn)

        else:
           return super(ListarReporteVentaDetalle, self).render_column(row, column)        