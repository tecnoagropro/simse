from uuid import UUID
from rest_framework.generics import  RetrieveAPIView,\
    get_object_or_404
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework import pagination
from rest_framework.response import Response
from rest_framework import status, viewsets

from django.db.models import Sum
from django.contrib.auth import get_user_model
from common.mixins import BaseAPIView, BasePostAPIView, BorrarView
from accounts.models import Persona
from inventario.models import Stock, AsignacionStock
from encargado.models import Encargado, VentaClienteProducto, CierreEncargado,\
    Cuenta, TransaccionHistory, PagoVenta
from inventario.models import Costo
from .serializers import ReporteCierreSerializer


class ReporteLocalViewSet(BaseAPIView):
    """
    get:
        Obtener reporte
    """

    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):

        data = {
        }
        return Response(data, status=status.HTTP_200_OK)        


class ReporteCierreViewSet(BasePostAPIView):
    """
    post:
        El encargado hace un cierre
    """
    serializer_class = ReporteCierreSerializer
    permission_classes = (IsAuthenticated,)
    queryset = Encargado.objects.filter(condicion=True)
    verify_success = False

    def get_exception(self, exc):
        raise exc
        return {
            'error_message':str(exc)
        }    


class CierreViewSet(viewsets.ModelViewSet):
    """

    list:
        Lista todos los cierres principales

    retrive:
        Detalle de un cierre

    """
    serializer_class = ReporteCierreSerializer
    permission_classes = (IsAuthenticated,)
    queryset = CierreEncargado.objects.all()


class ReporteLocalEncargadoViewSet(RetrieveAPIView, BaseAPIView):
    """
    get:
        Obtener reporte /reporte/administrador_ventas 
        no reporte de cierre
    """

    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):

        params = request.GET
        enc_id = self.kwargs['pk']
        qs = VentaClienteProducto.objects.filter(
                 venta__encargado_id=enc_id, condicion=True)  

        persona_encargado = Encargado.objects.get(pk=enc_id).persona
        enc_cuenta = Cuenta.objects.get(persona=persona_encargado)
        ganancia = 0
        pagos = 0
        total_pendiente = 0
        ya_venta = []
        ## Metodos de pago
        por_transferencia = 0
        por_efectivo = 0
        por_punto_venta = 0
        por_cheques = 0
        por_otros = 0

        for venta in qs:
            if venta.venta.pagado:
                if venta.venta.id not in ya_venta:

                    metodos_pago = PagoVenta.objects.filter(condicion=True, venta=venta.venta)
                    for m_pago in metodos_pago:
                        if m_pago.metodo.id == 3:
                            por_transferencia += m_pago.monto.monto
                        if m_pago.metodo.id == 4:
                            por_efectivo += m_pago.monto.monto
                        if m_pago.metodo.id == 5:
                            por_punto_venta += m_pago.monto.monto
                        if m_pago.metodo.id == 6:
                            por_cheques += m_pago.monto.monto
                        if m_pago.metodo.id == 7:
                            por_otros += m_pago.monto.monto

                    pagos += venta.venta.pagado.monto
                    ya_venta.append(venta.venta.id)

            ganancia += venta.monto.monto


        asignaciones = AsignacionStock.objects.activos(encargado_id=enc_id,
                                                       aceptado=True)
        for a in asignaciones:
            total_pendiente += a.pendiente_por_vender

        deuda = ganancia - pagos
        pago_encargados = 0

        pagos_enc = TransaccionHistory.objects.activos(
                    destino=enc_cuenta)
        
        pagos_enc = pagos_enc.aggregate(t=Sum('monto__monto'))
        try:
            pago_encargados = Costo.adquirir(pagos_enc['t']).monto
        except Exception as e:
            pass

        total_neto = ganancia - (deuda + pago_encargados)

        data = {
            'ganancia' : ganancia,
            'deuda' : deuda,
            'total_neto' : total_neto,
            'pago_encargados' : pago_encargados,
            'total_pendiente' : total_pendiente,
            'por_transferencia': por_transferencia,
            'por_efectivo': por_efectivo,
            'por_punto_venta': por_punto_venta,
            'por_cheques': por_cheques,
            'por_otros': por_otros
        }

        return Response(data, status=status.HTTP_200_OK)    