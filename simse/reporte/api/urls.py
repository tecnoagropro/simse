from django.conf.urls import include, url
from rest_framework import routers
from rest_framework.routers import SimpleRouter
from .viewsets import ReporteLocalEncargadoViewSet,\
  ReporteLocalViewSet, ReporteCierreViewSet, CierreViewSet

router = SimpleRouter()
router.register("cierre", CierreViewSet)

urlpatterns = [
	
	url(r'^reporte/local/encargado/(?P<pk>\d+)$', ReporteLocalEncargadoViewSet.as_view(),
		name="reporte-local-encargado"),
	url(r'^reporte/local/$', ReporteLocalViewSet.as_view(),
		name="reporte-local-encargado"),	

    url(r'^reporte/cierre/$', ReporteCierreViewSet.as_view(),
        name="reporte-cierre"),   

    url(r'^', include(router.urls)),             
]
