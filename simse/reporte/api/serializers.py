from django.contrib.auth import get_user_model
from django.urls import resolve
from django.utils.translation import ugettext as _
from django.db.models import Sum, Count
from django.utils import timezone
from rest_framework import serializers
from rest_framework.serializers import HyperlinkedModelSerializer
from rest_framework.generics import get_object_or_404
from rest_framework.compat import set_many
from rest_framework.utils import model_meta
from rest_framework.fields import SerializerMethodField

from accounts.models import Persona
from common.serializers import QueryFieldsMixin
from accounts.models import Persona
from encargado.models import CierreEncargado, VentaClienteProducto, TransaccionHistory,\
    Cliente, DetalleCierre, Venta, Cuenta, DetalleCierreProducto, DetalleCierreCliente,\
    PagoVenta
from encargado.api.serializers import EncargadoSesion
from inventario.models import Costo, AsignacionStock, CostoProducto


class ReporteCierreSerializer(QueryFieldsMixin,
                              HyperlinkedModelSerializer):

    total = serializers.DecimalField(max_digits=30,
                                     decimal_places=5,
                                     read_only=True,
                                     source='total.monto')
    pagado = serializers.DecimalField(max_digits=30,
                                     decimal_places=5,
                                     read_only=True,
                                     source='pagado.monto')
    pago_encargados = SerializerMethodField()
    cantidad_productos_por_vender = serializers.DecimalField(max_digits=30,
                                     decimal_places=5,
                                     read_only=True,
                                     source='cantidad_productos_por_vender.monto') 
    cantidad_productos_inventario = serializers.DecimalField(max_digits=30,
                                     decimal_places=5,
                                     read_only=True,
                                     source='cantidad_productos_inventario.monto')                                    
    cantidad_productos_vendido = serializers.IntegerField(read_only=True)
    fecha_inicio = serializers.DateField(read_only=True,format="%d/%m/%Y")
    fecha_fin = serializers.DateField(read_only=True,format="%d/%m/%Y")
    fecha_fin_cierre = serializers.DateField(required=False, input_formats=["%d/%m/%Y","%d-%m-%Y"])
    deuda = SerializerMethodField()
    encargado = serializers.HiddenField(default=EncargadoSesion())

    por_transferencia = serializers.DecimalField(max_digits=30,
                                     decimal_places=3,
                                     read_only=True,
                                     source='por_transferencia.monto')
    por_efectivo = serializers.DecimalField(max_digits=30,
                                     decimal_places=3,
                                     read_only=True,
                                     source='por_efectivo.monto')
    por_cheques = serializers.DecimalField(max_digits=30,
                                     decimal_places=3,
                                     read_only=True,
                                     source='por_cheques.monto')
    por_otros = serializers.DecimalField(max_digits=30,
                                     decimal_places=3,
                                     read_only=True,
                                     source='por_otros.monto')

    por_punto_venta = serializers.DecimalField(max_digits=30,
                                     decimal_places=3,
                                     read_only=True,
                                     source='por_punto_venta.monto')                                                                                                                   
    def validate(self, attr):
        enc = attr.get('encargado')
        hoy = timezone.now()
        from datetime import date
        f = attr.get('fecha_fin_cierre', None)
        if f:
            t = f.day
            lc = CierreEncargado.objects.filter(
                 condicion=True, encargado=enc,
                 created__day=t,
                 created__month=f.month,
                 created__year=f.year)

            if lc.count() > 0:
                raise serializers.ValidationError({
                'mensaje':'Ya existe reporte para esta fecha'
                })
        else:

            t = date.today().day
            lc = CierreEncargado.objects.filter(
                 condicion=True, encargado=enc,
                 created__day=t,
                 created__month=hoy.month,
                 created__year=hoy.year)

            if lc.count() > 0:
                raise serializers.ValidationError({
                'mensaje':'Ya existe reporte para esta fecha'
                })
        
        return super(ReporteCierreSerializer, self).validate(attr)

    def get_pago_encargados(self, obj):
        r = obj.pago_encargados.monto if obj.pago_encargados else 0
        return r

    def get_deuda(self, obj):
        return obj.total.monto - obj.pagado.monto

    def create(self, validated_data):
        enc = validated_data.get('encargado')
        end = timezone.now()
        fecha_ultimo_cierre = None
       
        try:
            cierre = CierreEncargado.objects.filter(
                            condicion=True, encargado=enc).latest()
            start = cierre.fecha_fin
            fecha_ultimo_cierre = start
            qs = VentaClienteProducto.objects.filter(
                 venta__encargado=enc, condicion=True,
                 created__range=[start,end])

        except:
            qs = VentaClienteProducto.objects.filter(
                 venta__encargado=enc, condicion=True)            
            start = qs.first().created
            from datetime import timedelta
            if validated_data.get('fecha_fin_cierre', False):
                end = validated_data.get('fecha_fin_cierre')
            end = end + timedelta(days=1)
            qs = qs.filter(created__range=[start,end])
        
        ganancia = 0
        pagos = 0
        ya_venta = []
        ya_clientes = []
        pago_encargados = 0
        cantidad_vender_dinero = 0
        cantidad_productos_vendido = 0
        cantidad_productos_inventario = 0

        try:
            
            cantidad_vender_dinero = AsignacionStock.objects.filter(
                    encargado=enc, condicion=True,
                    cantidad_actual__gt=0,
                    aceptado=True).monto_por_vender()

            cantidad_productos_inventario = AsignacionStock.objects.filter(
                    encargado=enc, condicion=True,
                    cantidad_actual__gt=0,
                    aceptado=True).aggregate(cant=Sum('cantidad_actual'))

        except Exception as e:
            pass


        ## Metodos de pago
        por_transferencia = 0
        por_efectivo = 0
        por_punto_venta = 0
        por_cheques = 0
        por_otros = 0
        for venta in qs:
            if venta.venta.pagado:
                if venta.venta.id not in ya_venta:
                    metodos_pago = PagoVenta.objects.filter(condicion=True, venta=venta.venta)
                    for m_pago in metodos_pago:
                        #print ("metodo pago %s" % str(m_pago.metodo.id))
                        if m_pago.metodo.id == 3:
                            por_transferencia += m_pago.monto.monto
                        if m_pago.metodo.id == 4:
                            por_efectivo += m_pago.monto.monto
                        if m_pago.metodo.id == 5:
                            por_punto_venta += m_pago.monto.monto
                        if m_pago.metodo.id == 6:
                            por_cheques += m_pago.monto.monto
                        if m_pago.metodo.id == 7:
                            por_otros += m_pago.monto.monto

                    pagos += venta.venta.pagado.monto
                    ya_venta.append(venta.venta.id)
            cantidad_productos_vendido += venta.cantidad
            ganancia += venta.monto.monto

        enc_cuenta = Cuenta.objects.get(persona=enc.persona)

        pagos_enc = TransaccionHistory.objects.activos(
                    destino=enc_cuenta,created__range=[start,end])
        pagos_enc = pagos_enc.aggregate(t=Sum('monto__monto'))
        try:
            pago_encargados = Costo.adquirir(pagos_enc['t'])
        except:
            pass

        ce = CierreEncargado()
        ce.encargado = enc
        ce.fecha_inicio = start
        ce.fecha_fin = end 
        ce.total = Costo.adquirir(ganancia)
        ce.pagado = Costo.adquirir(pagos)
        ce.cantidad_productos_por_vender = Costo.adquirir(cantidad_vender_dinero)
        ce.cantidad_productos_inventario = Costo.adquirir(cantidad_productos_inventario['cant'])
        ce.cantidad_productos_vendido = cantidad_productos_vendido
        ce.pago_encargados = pago_encargados if pago_encargados != 0 else None 

        ce.por_transferencia = Costo.adquirir(por_transferencia)
        ce.por_efectivo = Costo.adquirir(por_efectivo)
        ce.por_punto_venta = Costo.adquirir(por_punto_venta)
        ce.por_cheques = Costo.adquirir(por_cheques)
        ce.por_otros = Costo.adquirir(por_otros)
        ce.save()


        k = 'stock__stock__producto__id'
        if fecha_ultimo_cierre:
            productos = VentaClienteProducto.objects.filter(
                        venta__encargado=enc,
                        created__range=[fecha_ultimo_cierre,end]).values(k).distinct(k).order_by()
        else:
            productos = VentaClienteProducto.objects.filter(
                        venta__encargado=enc).values(k).distinct(k).order_by()

        for p in productos:
            if fecha_ultimo_cierre:
                p_ventas = VentaClienteProducto.objects.filter(
                            venta__encargado=enc,
                            stock__stock__producto__id=p[k],
                            created__range=[fecha_ultimo_cierre,end])
            else:
                p_ventas = VentaClienteProducto.objects.filter(
                            venta__encargado=enc,
                            stock__stock__producto__id=p[k]
                            )

            dcp = DetalleCierreProducto()
            dcp.cierre = ce
            dcp.producto_id = p[k]
            dcp.cantidad = p_ventas.count()
            cp=CostoProducto.objects.filter(producto_id=p[k]).last()
            r = cp.monto.monto * dcp.cantidad
            dcp.venta = Costo.adquirir(r)
            dcp.save()
            
            for p_v in p_ventas:
                if fecha_ultimo_cierre:
                    c_ventas = VentaClienteProducto.objects.filter(
                            venta__cliente=p_v.venta.cliente,
                            created__range=[fecha_ultimo_cierre,end])
                else:
                    c_ventas = VentaClienteProducto.objects.filter(
                            venta__cliente=p_v.venta.cliente
                            )

                try:
                    DetalleCierreCliente.objects.get(
                        cliente=p_v.venta.cliente,
                        cierre=dcp
                        )
                except Exception as e:
                    m1 = c_ventas.aggregate(s=Sum('monto__monto'))['s']
                    m2 = c_ventas.aggregate(s=Sum('venta__pagado__monto'))['s']
                    m3 = m1 - m2 if m2 else 0

                    dcc = DetalleCierreCliente()
                    dcc.cliente = p_v.venta.cliente 
                    dcc.cierre = dcp
                    dcc.pagado = Costo.adquirir(m2 if m2 else 0)
                    dcc.venta = Costo.adquirir(m1)
                    dcc.pendiente = Costo.adquirir(m3)
                    dcc.save()
                  
        return validated_data

    class Meta:
        model = CierreEncargado
        fields = ('id','cantidad_productos_vendido','fecha_inicio','fecha_fin',
                  'total','pagado','pago_encargados','cantidad_productos_por_vender',
                  'deuda','cantidad_productos_inventario','fecha_fin_cierre',
                  'encargado','por_transferencia','por_efectivo',
                  'por_punto_venta','por_cheques','por_otros')