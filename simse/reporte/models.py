from django.db import models
from common.models import UserOneBaseModel, BaseModel, UserRelationBaseModel, Permiso
from django.utils.translation import ugettext_lazy as _
from accounts.models import Persona

class Inversionista(UserOneBaseModel):
    
    permisos = models.ManyToManyField(Permiso, blank=True)
    persona = models.OneToOneField(Persona)

    def __str__(self):
        return str(self.persona)

