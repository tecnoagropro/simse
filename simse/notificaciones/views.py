from django.shortcuts import render
from django.http import JsonResponse
from .one_signal_sdk import OneSignalSdk
from django.conf import settings
from common.utils import get_server_url

def notificacion_configurar(request):
    return render(request, 'configurar_notificaciones.html', {
    	'ONESIGNAL_APP_ID':settings.ONESIGNAL_APP_ID
    	})


def notificacion_web(request):
    one_signal = OneSignalSdk(settings.ONESIGNAL_API_KEY,settings.ONESIGNAL_APP_ID)

    #r = one_signal.get_players(settings.ONESIGNAL_API_KEY)
    #print (r.json())

    base_url = get_server_url(request)
    url = base_url + '/notificacion_web/?mensaje=Jovan eres tu'
    url = base_url + '/notificacion_web/mensaje/'
    mensaje = request.GET.get('mensaje','Mensaje de prueba')
    noti = one_signal.create_notification(contents='Desde python',url=url)
    #print (noti.json())
    return render(request, 'notificacion_web.html', {
    	'mensaje':mensaje
    	})

