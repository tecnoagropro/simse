from django.conf.urls import include, url
from django.views.decorators.csrf import csrf_exempt

from . import views

urlpatterns = [
    url(r'^notificacion_configurar/$', views.notificacion_configurar, name='notificacion_configurar'),
    url(r'^notificacion_web/$', views.notificacion_web, name='notificacion_web'),
]
