import json
import jwt
import logging
from django.contrib.auth.mixins import AccessMixin
from django.conf import settings
from django.contrib.auth.views import redirect_to_login
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.shortcuts import resolve_url
from django.utils.six.moves.urllib.parse import urlparse


from datetime import datetime, date, timedelta
from rest_framework.decorators import list_route
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet
from django_datatables_view.base_datatable_view import DatatableMixin
from django_datatables_view.mixins import JSONResponseView,JSONResponseMixin, LazyEncoder
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.base import TemplateView
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode
from django.contrib.auth import get_user_model
from rest_framework_jwt.utils import jwt_decode_handler
from rest_framework import status
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError
from django.utils import timezone

logger = logging.getLogger(__name__)
User = get_user_model()

class BaseAPIView(APIView):
    """
    Base API View 
    """

    def get_serializer_context(self):
        """
        Extra context provided to the serializer class.
        """
        return {
            'request': self.request,
            'view': self,
        }

    def get_serializer_class(self):
        """
        Return the class to use for the serializer.
        Defaults to using `self.serializer_class`.
        You may want to override this if you need to provide different
        serializations depending on the incoming request.
        (Eg. admins get full serialization, others get basic serialization)
        """
        assert self.serializer_class is not None, (
            "'%s' should either include a `serializer_class` attribute, "
            "or override the `get_serializer_class()` method."
            % self.__class__.__name__)
        return self.serializer_class

    def get_serializer(self, *args, **kwargs):
        """
        Return the serializer instance that should be used for validating and
        deserializing input, and for serializing output.
        """
        serializer_class = self.get_serializer_class()
        kwargs['context'] = self.get_serializer_context()
        return serializer_class(*args, **kwargs)


class BasePostAPIView(BaseAPIView):

    verify_success = False

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            try:
                data = serializer.save()
                if self.verify_success:
                    try:
                        if not data['success']:
                            resp = {
                                    "success": False,
                                    "errors":data['errors']
                                    }
                            return Response(resp, status=status.HTTP_400_BAD_REQUEST)   
                        else:
                            raise KeyError
                    except KeyError as e:
                        if getattr(data,"id",None):
                            resp = {"success": True, "id": data.id}
                        else:
                            resp = {"success": True}
                        return Response(resp, status=status.HTTP_201_CREATED)                        
                
                else:
                    if getattr(data,"id",None):
                        resp = {"success": True, "id": data.id}
                    else:
                        resp = {"success": True}
                    return Response(resp, status=status.HTTP_201_CREATED)                    
            
            except Exception as e:
                if getattr(self,"get_exception",None):
                    resp = self.get_exception(e)
                else:
                    resp = {
                        'error_message':str(e)
                    }
                return Response(resp, status=status.HTTP_400_BAD_REQUEST)


class PostSerializerProcessViewMixin():
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(request.user.profile, data=request.data)
        if serializer.is_valid(raise_exception=True):
            obj = serializer.save()
            return Response({"success": True})


class PutSerializerProcessViewMixin():
    def put(self, request, *args, **kwargs):
        serializer = self.get_serializer(request.user.profile, data=request.data)
        if serializer.is_valid(raise_exception=True):
            obj = serializer.save()
            return Response({"success": True})


class MultipleChoicesMixins():
    choices_response = {}

    def get_choices_response(self):
        return self.choices_response

    def reformat_object(self, array):
        return [{"id": obj[0], "name": obj[1]} for obj in array]

    def get_data(self):
        return {key: self.reformat_object(value) for key, value in self.get_choices_response().items()}


class MultipleChoicesAPIView(MultipleChoicesMixins, APIView):
    def get(self, request, version, format=None):
        return Response(self.get_data())




class MultipleChoicesViewSet(MultipleChoicesMixins, ModelViewSet):
    @list_route(["get"])
    def form(self, request, **kwargs):
        return Response({key: self.reformat_object(value) for key, value in self.get_choices_response().items()})



class Botones(object):

    def __call__(self):
        return self.separador.join(self.botones)

    def __init__(self, **kw):
        self.template = '<a href="{link}" type="button" class="btn btn-{class_css}" title="{title}"><i class="fa fa-{ico}"></i></a>'
        self.template_btn = '<a type="button" class="btn btn-{class_css}" onclick="{funcion}" title="{title}"><i class="fa fa-{ico}"></i></a>'
        self.botones = []
        self.separador = '&nbsp'
        
    def href(self, link, class_css, ico,title=''):
        self.botones.append(self.template.format(link=link,class_css=class_css,ico=ico,title=title))

    def click(self, funcion, class_css, ico,title=''):
        self.botones.append(self.template_btn.format(funcion=funcion,class_css=class_css,ico=ico,title=title))

    def __str__(self):
        return self.separador.join(self.botones)


class DataTable(DatatableMixin, JSONResponseMixin, TemplateView):

    default_context = True
    total_records = None
    total_display_records = None

    def get_initial_queryset(self):
        try:
            return self.model.objects.activos()
        except:
            return self.model.objects.filter(condicion=True)
        
        
    def get_context_data(self, *args, **kwargs):

        if self.default_context:
            return super().get_context_data(*args, **kwargs)

        try:
            self.initialize(*args, **kwargs)

            qs = self.get_initial_queryset()
            
            if not self.total_records:
                total_records = qs.count()
            else:
                total_records = self.total_records

            qs = self.filter_queryset(qs)

            # number of records after filtering
            
            qs = self.ordering(qs)
            qs = self.paging(qs)

            # prepare output data
            aaData = self.prepare_results(qs)
            if not self.total_display_records:
                total_display_records = len(aaData)
            else:
                total_display_records = self.total_display_records

            if self.pre_camel_case_notation:

                ret = {'sEcho': int(self._querydict.get('sEcho', 0)),
                       'iTotalRecords': total_records,
                       'iTotalDisplayRecords': total_display_records,
                       'aaData': aaData
                       }
            else:

                ret = {'draw': int(self._querydict.get('draw', 0)),
                       'recordsTotal': total_records,
                       'recordsFiltered': total_display_records,
                       'data': aaData
                       }

        except Exception as e:
            logger.exception(str(e))

            if settings.DEBUG:
                import sys
                from django.views.debug import ExceptionReporter
                reporter = ExceptionReporter(None, *sys.exc_info())
                text = "\n" + reporter.get_traceback_text()
            else:
                text = "\nAn error occured while processing an AJAX request."

            if self.pre_camel_case_notation:
                ret = {'sError': text,
                       'text': text,
                       'aaData': [],
                       'sEcho': int(self._querydict.get('sEcho', 0)),
                       'iTotalRecords': 0,
                       'iTotalDisplayRecords': 0, }
            else:
                ret = {'error': text,
                       'data': [],
                       'recordsTotal': 0,
                       'recordsFiltered': 0,
                       'draw': int(self._querydict.get('draw', 0))}
        return ret

    def get(self, request, *args, **kwargs):
        token = self.request.META.get("HTTP_AUTHORIZATION")
        try:
            datos = jwt_decode_handler(token)
            
            request.user = User.objects.get(id=datos['user_id'])
            self.usuario = request.user
            
            if 'pk' in kwargs:
                self.pk = kwargs['pk']

            params = self.request.GET
            try:
                date_array = params['start'].split('-')
                self.start = date(int(date_array[2]), int(date_array[1]), int(date_array[0]))
                self.start = datetime.combine(self.start, datetime.min.time())
            except Exception as e:
                self.start = (timezone.now() - timedelta(days=965))
              
            try:
                date_array = params['end'].split('-')
                self.end = date(int(date_array[2]), int(date_array[1]), int(date_array[0]))
                self.end = self.end + timedelta(days=1)
                self.end = datetime.combine(self.end, datetime.min.time())                
            except Exception as e:
                self.end = timezone.now() + timedelta(days=1)  

                
            return JSONResponseMixin.get(self, request=request, *args,**kwargs)

        except Exception as e:
            if isinstance(e, IndexError):
                msg = 'Error con algun dato de entrada. IndexError.'
            else:
                msg = 'Debe iniciar session nuevamente.'


            response = {'result': 'error',
                        'sError': str(e),
                        'text': msg}
            raise e
            dump = json.dumps(response, cls=LazyEncoder)
            return self.render_to_response(dump)


class BorrarView(BaseAPIView):

    def delete(self, request, *args, **kwargs):
        instance = self.get_object()
        if request.query_params.get('borrar', None):
            instance.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)

        if instance.condicion:
            instance.condicion = False
        else:
            instance.condicion = True
        instance.save()
        return Response(status=status.HTTP_204_NO_CONTENT)



def redirect(request):
    path = request.build_absolute_uri()
    resolved_login_url = resolve_url(settings.LOGIN_URL)
    # If the login url is the same scheme and net location then just
    # use the path as the "next" url.
    login_scheme, login_netloc = urlparse(resolved_login_url)[:2]
    current_scheme, current_netloc = urlparse(path)[:2]
    if ((not login_scheme or login_scheme == current_scheme) and
            (not login_netloc or login_netloc == current_netloc)):
        path = request.get_full_path()

    return redirect_to_login(
    path, resolved_login_url, REDIRECT_FIELD_NAME)


class JWTMixin(AccessMixin):

    jwt = settings.NAME_JWT

    def validate(self, request):
        if self.jwt in request.COOKIES:
            try:
                jwt = request.COOKIES[self.jwt]
                decode = jwt_decode_handler(jwt)
                user = User.objects.get(username=decode['username'])
                return True
            except Exception as e:
                print (e)
                return False

        return False
        
    def dispatch(self, request, *args, **kwargs):
        if not self.validate(request):

            response = redirect(request)
            if self.jwt in request.COOKIES:
                del request.COOKIES[self.jwt]
                response.delete_cookie(self.jwt)
            return response
        return super().dispatch(request, *args, **kwargs)