from rest_framework import serializers
from .models import Permiso, Pais, Nota

class BaseModelSerializer(serializers.ModelSerializer):
    pass


class QueryFieldsMixin(object):
    # https://github.com/wimglenn/djangorestframework-queryfields/blob/master/drf_queryfields/mixins.py
    # If using Django filters in the API, these labels mustn't conflict with any model field names.
    include_arg_name = 'fields'
    exclude_arg_name = 'fields!'

    # Split field names by this string.  It doesn't necessarily have to be a single character.
    # Avoid RFC 1738 reserved characters i.e. ';', '/', '?', ':', '@', '=' and '&'
    delimiter = ','

    def __init__(self, *args, **kwargs):
        super(QueryFieldsMixin, self).__init__(*args, **kwargs)

        try:
            request = self.context['request']
            method = request.method
        except (AttributeError, TypeError, KeyError):
            # The serializer was not initialized with request context.
            return

        if method != 'GET':
            return

        try:
            query_params = request.query_params
        except AttributeError:
            # DRF 2
            query_params = getattr(request, 'QUERY_PARAMS', request.GET)

        includes = query_params.getlist(self.include_arg_name)
        include_field_names = {name for names in includes for name in names.split(self.delimiter) if name}

        excludes = query_params.getlist(self.exclude_arg_name)
        exclude_field_names = {name for names in excludes for name in names.split(self.delimiter) if name}

        if not include_field_names and not exclude_field_names:
            # No user fields filtering was requested, we have nothing to do here.
            return

        serializer_field_names = set(self.fields)

        fields_to_drop = serializer_field_names & exclude_field_names
        if include_field_names:
            fields_to_drop |= serializer_field_names - include_field_names

        for field in fields_to_drop:
            self.fields.pop(field)



class PermisoSerializer(QueryFieldsMixin,serializers.ModelSerializer):

    code= serializers.CharField(required=True)
    name= serializers.CharField(required=True)

    def create(self, validate_data):

        user = self.context['request'].user
        code = validate_data.get('code', None)
        name = validate_data.get('name', None)

        u.user_permissions.add(permission)
        resp = {
            'task_id': str('task_id')
        }        

        return resp


    class Meta:
        model = Permiso
        fields = ('code','name')        



class PaisSerializer(serializers.ModelSerializer):

    class Meta:
        model = Pais
        fields = ('id','pais',)   


class NotaSerializer(serializers.ModelSerializer):

    def create(self, validate_data):
        user = self.context['request'].user

        try:
            nota = Nota.objects.get(persona__user=user)
            nota.nota = validate_data.get('nota')
            nota.save()
        except Nota.DoesNotExist:
            nota = Nota()
            nota.persona = user.persona_set
            nota.nota = validate_data.get('nota')
            nota.save()

        return nota 

    class Meta:
        model = Nota
        fields = ('nota',)           