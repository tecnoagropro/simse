from rest_framework.renderers import JSONRenderer


class PrettyJSONRenderer(JSONRenderer):
    def get_indent(self, accepted_media_type, renderer_context):
        if renderer_context["request"].GET.get("pretty"):
            return 4
        return super().get_indent(accepted_media_type, renderer_context)