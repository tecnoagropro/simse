import coreapi
import coreschema
from django.http import Http404
from django.utils import six
from django.utils.translation import ugettext_lazy as _
from rest_framework import exceptions, status
from rest_framework.compat import set_rollback
from rest_framework.exceptions import PermissionDenied, ValidationError
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.renderers import CoreJSONRenderer
from rest_framework.response import Response
from rest_framework.schemas import SchemaGenerator
from rest_framework.views import APIView
from rest_framework_swagger import renderers
from rest_framework.generics import ListAPIView, RetrieveAPIView,\
    RetrieveUpdateDestroyAPIView, CreateAPIView, RetrieveUpdateAPIView
from rest_framework.permissions import IsAuthenticated
from django.views.generic import TemplateView
from .mixins import BaseAPIView, JWTMixin
from .serializers import PermisoSerializer, PaisSerializer, NotaSerializer
from .models import Permiso, Pais, Nota
from django.template import loader
from rest_framework import viewsets
from django.conf import settings


def custom_exception_handler(exc, context):

    if isinstance(exc, ValidationError):
        if isinstance(exc.detail, (list, dict)):
            data = {"errors": exc.detail}
        else:
            data = {'message': exc.detail}
        return Response(data, status=exc.status_code)

    if isinstance(exc, exceptions.APIException):
        headers = {}
        if getattr(exc, 'auth_header', None):
            headers['WWW-Authenticate'] = exc.auth_header
        if getattr(exc, 'wait', None):
            headers['Retry-After'] = '%d' % exc.wait

        if isinstance(exc.detail, (list, dict)):
            data = {"errors": exc.detail}
        else:
            data = {'message': exc.detail}

        set_rollback()
        return Response(data, status=exc.status_code, headers=headers)

    elif isinstance(exc, Http404):
        msg = _('Not found.')
        data = {'message': six.text_type(msg)}

        set_rollback()
        return Response(data, status=status.HTTP_404_NOT_FOUND)

    elif isinstance(exc, PermissionDenied):
        msg = _('Permission denied.')
        data = {'message': six.text_type(msg)}

        set_rollback()
        return Response(data, status=status.HTTP_403_FORBIDDEN)

    return None



class MySchemaGenerator(SchemaGenerator):
    title = 'REST API Index'

    def get_link(self, path, method, view):
        link = super(MySchemaGenerator, self).get_link(path, method, view)
        link._fields += self.get_core_fields(view)
        return link

    def get_core_fields(self, view):
        if hasattr(view, "get_core_fields"):
            return getattr(view, 'get_core_fields')(coreapi, coreschema)
        return ()


class SwaggerSchemaView(APIView):
    _ignore_model_permissions = True
    exclude_from_schema = True
    permission_classes = [AllowAny]
    renderer_classes = [
        CoreJSONRenderer,
        renderers.OpenAPIRenderer,
        renderers.SwaggerUIRenderer
    ]

    def get(self, request):
        generator = MySchemaGenerator(
            title="Moto Servicios Express API",
            # url=url,
            # patterns=patterns,
            # urlconf=urlconf
        )
        schema = generator.get_schema(request=request)

        if not schema:
            raise exceptions.ValidationError(
                'The schema generator did not return a schema Document'
            )

        return Response(schema)


class PaisesViewSet(viewsets.ModelViewSet):
    serializer_class = PaisSerializer
    queryset = Pais.objects.all()
    permission_classes = (IsAuthenticated,)

    def get_permissions(self):
        if self.request.method.lower() == "get":
            return [AllowAny()]
        return super(PaisesViewSet, self).get_permissions()


class NotasViewSet(viewsets.ModelViewSet):
    serializer_class = NotaSerializer
    queryset = Nota.objects.all()
    permission_classes = (IsAuthenticated,)



class HomeView(JWTMixin, TemplateView):
    template_name = 'base.html'


class View404(TemplateView):
    template_name = '404.html'


class ConfigDjangoJs(TemplateView):
    template_name = 'config_django_js.html'
    
    def get_context_data(self):

        c = super(ConfigDjangoJs,self).get_context_data()
        c['debug'] = 'true' if settings.DEBUG else 'false'
        c['icono_dashboard'] = 'fa fa-tachometer-alt'
        c['jwt_name'] = settings.NAME_JWT
        return c


class View500(TemplateView):
    template_name = '500.html'


class HomeView(JWTMixin, TemplateView):
    template_name = 'base.html'


class PermisoViewSet(BaseAPIView):
    """
    post:
    Crea un nuevo permiso en la app

    """
    permission_classes = (IsAuthenticated,)
    serializer_class = PermisoSerializer

    def post(self, request, *args, **kwargs):

        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            data = serializer.save()
            resp = {}
            return Response(resp, status=status.HTTP_201_CREATED)
