from django.utils.translation import ugettext_lazy as _


class M(object):
	"""

	Clase de atributos para los mensajes en las vistas.

	"""
	hola = _("Hola.")
	bienvenido = _("Bienvenido.")
	pago_mayor = _("El monto a pagar no puede ser mayor que la deuda.")
	sin_fondos = _("La cuenta de origen no tiene fondos suficientes.")
	misma_cuenta = _("La cuenta de origen no puede ser la misma que la de destino.")
	stock_superado_error = _("Stock superado ó no aceptado.")
	stock_superado_venta = _("La cantidad a vender supera al stock.")
	stock_superado = _("Cantidad en stock superada.")
	sin_stock = _("No tiene stock para el producto.")
	sin_costo = _("Este producto (%s) no tiene costo, debe agregar uno.")
	icono_modificar = _("Icono para modificar.")
	url_d_admin = _("URL de redirección de dashboard admin.")
	url_d_encargado = _("URL de redirección de dashboard encargado.")
	cantidad_inválida = _("Cantidad inválida.")


