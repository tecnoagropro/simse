from django.conf import settings
from django.contrib.auth.decorators import user_passes_test
from django.shortcuts import  redirect
from rest_framework_jwt.utils import jwt_decode_handler
from django.contrib.auth import get_user_model
from dynamic_preferences.registries import global_preferences_registry
global_preferences = global_preferences_registry.manager()

def authenticated_redirect(function=None, login_url=settings.LOGIN_REDIRECT_URL):
    """
    Decorator for views that checks that the user is logged in, redirecting
    to the log-in page if necessary.
    """
    actual_decorator = user_passes_test(lambda u: u.is_authenticated() == False, login_url)
    if function:
        return actual_decorator(function)
    return actual_decorator


class Token(object):

    def __init__(self, vista):
        self.vista = vista
        if hasattr(vista, 'permisos'):
            self.permisos = vista.permisos

    def __call__(self, *args, **kw):
        request = args[0]
        if settings.NAME_JWT in request.COOKIES:
            jwt = request.COOKIES[settings.NAME_JWT]
            try:
                datos = jwt_decode_handler(jwt)
                request.user = get_user_model().objects.get(id=datos['user_id'])
                if hasattr(self,'permisos'):
                    if request.user.is_superuser:
                        return self.vista(*args,**kw)
                    if request.user.has_perms(self.permisos):
                        return self.vista(*args,**kw)
                    return self.sin_permisos(request)

                return self.vista(*args,**kw)
            except Exception as e:
                print (e)
                del request.COOKIES[settings.NAME_JWT]
                response = self.token_invalido(request)
                response.delete_cookie(settings.NAME_JWT)
                return response

        return self.token_invalido(request)
        
    def token_invalido(self, request):
        context = {}
        return redirect('accounts:login')

    def sin_permisos(self, request):
        context = {}
        return redirect(global_preferences['URL_REDIRECCIONAR_ENCARGADO'])        