METHOD_OVERRIDE_HEADER = 'HTTP_X_HTTP_METHOD_OVERRIDE'

ALLOWED_METHODS = ["PUT", "PATCH", "DELETE"]


class MethodOverrideMiddleware(object):
    def __init__(self, get_response):
        self.get_response = get_response
        # One-time configuration and initialization.

    def __call__(self, request):
        response = self.get_response(request)
        return response

    def process_view(self, request, view_func, view_args, view_kwarg):
        if request.method != 'POST':
            return
        if METHOD_OVERRIDE_HEADER not in request.META:
            return
        if METHOD_OVERRIDE_HEADER in ALLOWED_METHODS:
            request.method = request.META[METHOD_OVERRIDE_HEADER]
