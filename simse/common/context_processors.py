from django.conf import settings
from .models import Nota

def baseurl(request):
    """
    Return a BASE_URL template context for the current request.
    """

    scheme = '//'
    try:
        nota = Nota.objects.filter(persona__user=request.user).last()
    except Exception as e:
        nota = ''

    return {'BASE_URL': scheme + request.get_host(),
            "DEBUG": settings.DEBUG,
            "WEB_VERSION": 1,
            "NOTA":nota if nota else ''
            }