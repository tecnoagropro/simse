from django.conf.urls import url, include
from rest_framework import routers
from rest_framework.routers import SimpleRouter
from .views import *

router = SimpleRouter()
router.register("paises", PaisesViewSet)
router.register("notas", NotasViewSet)

urlpatterns = [
	url(r'^', include(router.urls)),
]
