import hashlib
import random

from django.core.mail import EmailMultiAlternatives
from django.template import loader
from django.core.mail import EmailMessage
from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.utils.decorators import available_attrs
from django.utils.six import wraps


def has_permission(user, perm):
    try:
        if user.is_superuser:
            return True
    except:        
        return False

    if user.profile and getattr(user.profile, perm, False):
        return getattr(user.profile, perm, False)
    return getattr(user, perm, False)


def has_permission_403(user, perm):
    if not has_permission(user, perm):
        # return HttpResponseForbidden()
        raise PermissionDenied


def permission(permission_str):
    def decorator(func):
        @wraps(func, assigned=available_attrs(func))
        def inner(request, *args, **kwargs):
            has_permission_403(request.user, permission_str)
            return func(request, *args, **kwargs)

        return inner

    return decorator
    
def send_mail(subject_template_name, email_template_name,
              context, from_email, to_email, html_email_template_name=None):
    """
    Sends a django.core.mail.EmailMultiAlternatives to `to_email`.
    """

    subject = loader.render_to_string(subject_template_name, context)
    # # Email subject *must not* contain newlines
    subject = ''.join(subject.splitlines())
    body = loader.render_to_string(email_template_name, context)


    # email_message = EmailMultiAlternatives(subject, body, from_email, [to_email])
    # if html_email_template_name is not None:
    #     html_email = loader.render_to_string(html_email_template_name, context)
    #     email_message.attach_alternative(html_email, 'text/html')


    correo = EmailMessage(subject,body,[to_email])
    correo.send()

def generate_random_token(extra=None, hash_func=hashlib.sha256):
    if extra is None:
        extra = []
    bits = extra + str(random.SystemRandom().getrandbits(512))
    return hash_func("".join(bits).encode("utf-8")).hexdigest()



def pk_y_temporada(instancia, kwargs):
    
    pk = kwargs.get('pk')
    ultima_temporada = instancia.objects.last()
    temporada = kwargs.get('temporada',ultima_temporada.id)

    return pk, temporada



def get_server_url(request=None):
    if request is None:
        return settings.SERVER_URL[:-1]
    return "%s://%s" % (
        'https' if request.is_secure() else 'http', request.get_host())
    