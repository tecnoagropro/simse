from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _


class BaseManager(models.Manager):
    def activos(self, **extra_filter):
        filters = {
            'condicion':True
        }
        filters.update(extra_filter)
        return super(BaseManager, self).get_queryset().filter(**filters)

    def inactivos(self):
        return super(BaseManager, self).get_queryset().filter(condicion=False)


class BaseModel(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    condicion = models.BooleanField(default=True)
    objects = BaseManager()

    class Meta:
        abstract = True


class UserByBaseModel(BaseModel):
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, blank=False, null=False,
                                   related_name="created_%(app_label)s_%(class)s_set")
    updated_by = models.ForeignKey(settings.AUTH_USER_MODEL, blank=False, null=False,
                                   related_name="modified_%(app_label)s_%(class)s_set") 

    class Meta:
        abstract = True


class NameBaseModel(BaseModel):
    name = models.CharField(max_length=100)

    class Meta:
        abstract = True
        ordering = ("name",)

    def __str__(self):
        return self.name


class UserOneBaseModel(BaseModel):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    class Meta:
        abstract = True

    def __str__(self):
        return str(self.user)


class UserRelationBaseModel(BaseModel):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    class Meta:
        abstract = True


class Permiso(BaseModel):
    """Los permisos de la app

    ejemplos:

    permisos para encargados ->
        ENCARGADO_ALL                 (todos los permisos para encargados)
        ENCARGADO_ALL_{:id}           (todos los permisos para todos los encargados)
        ENCARGADO_MODIFICAR_{:id}     (Modificar a un encargado)

    permisos para empleados ->   E_ID_{:id}

        EMPLEADO_ALL                 (todos los permisos para empleado)
        EMPLEADO_ALL_{:id}           (todos los permisos para todos los empleado)
        EMPLEADO_MODIFICAR_{:id}     (Modificar a un EMPLEADO)

    """
    name = models.CharField(max_length=30)
    code = models.CharField(max_length=30)

    class Meta:
        ordering = ("name",)


class Pais(BaseModel):
    pais = models.CharField(max_length=50)

    def __str__(self):
        return self.pais

    class Meta:
        verbose_name = 'Pais'
        verbose_name_plural = 'Paises'
        ordering = ('pais', )


class Estado(BaseModel):
    estado = models.CharField(max_length=50)
    pais = models.ForeignKey(Pais)

    def __str__(self):
        return self.estado

    class Meta:
        verbose_name = 'Estado'
        verbose_name_plural = 'Estados'
        ordering = ('estado', )


class Ciudad(BaseModel):
    ciudad = models.CharField(max_length=50)
    estado = models.ForeignKey(Estado)

    def __str__(self):
        return self.ciudad

    class Meta:
        verbose_name = 'Ciudad'
        verbose_name_plural = 'Ciudades'
        ordering = ('ciudad', )


class ConfiguracionGeneral(BaseModel):
    """Por ejemplo establecer un nombre para exportar 
    por defecto"""

    clave = models.CharField(max_length=80)
    valor = models.CharField(max_length=500)

    def __str__(self):
        return str(self.clave)


class ConfiguracionPersona(BaseModel):
    """Por ejemplo establecer un nombre para exportar 
    solo para esta persona"""

    clave = models.CharField(max_length=80)
    valor = models.CharField(max_length=500)
    persona = models.ForeignKey('accounts.Persona')

    def __str__(self):
        return str(self.clave)


class Nota(BaseModel):
    persona = models.ForeignKey('accounts.Persona')
    nota = models.TextField()

    def __str__(self):
        return self.nota

    class Meta:
        ordering = ("-id",)
        get_latest_by = 'persona'




# class Municipio(models.Model):
#     estado = models.ForeignKey('Estado')
#     municipio = models.CharField(max_length=100)

#     def __str__(self):
#         return str(self.municipio.encode('utf-8'))

#     class Meta:
#         verbose_name = 'Municipio'
#         verbose_name_plural = 'Municipios'


# class Parroquia(models.Model):
#     municipio = models.ForeignKey('Municipio')
#     parroquia = models.CharField(max_length=100)

#     def __str__(self):
#         return str(self.parroquia.encode('utf-8'))

#     class Meta:
#         verbose_name = 'Parroquía'
#         verbose_name_plural = 'Parroquías'

# class PrefijoTelefonico(models.Model):
    
#     municipio = models.ForeignKey(Municipio)
#     prefijo = models.PositiveSmallIntegerField()

#     def __str__(self):
#         return str(self.prefijo)

#     class Meta:
#         verbose_name = 'Prefijo telefonico'
#         verbose_name_plural = 'Prefijos telefonicos'

# class prefijoCelular(models.Model):
    
#     prefijo = models.PositiveSmallIntegerField()

#     def __str__(self):
#         return str(self.prefijo)
        
#     class Meta:
#         verbose_name = 'Prefijo celular'
#         verbose_name_plural = 'Prefijos celulares'

# class Banco(models.Model):
    
#     banco = models.CharField(max_length=300,unique=True)
#     estatus = models.BooleanField(default=True)
#     prefijoTarjeta = models.PositiveSmallIntegerField()
#     prefijoCuenta = models.PositiveSmallIntegerField()
#     creadoPor = models.IntegerField(null=True)

#     def __str__(self):
#         return str(self.banco.encode('utf-8'))
        
#     class Meta:
#         verbose_name = 'Banco'
#         verbose_name_plural = 'Bancos'