from django.contrib import admin, messages
from .models import *

class BaseAdmin(admin.ModelAdmin):
    date_hierarchy = 'created'
    actions = ['cambiar_activacion']

    def cambiar_activacion(self, request, queryset):
        model = self.model._meta.verbose_name
        n = queryset.count()
        self.message_user(request,"%(count)d %(model)s cambiadas." % {"count": n ,"model":model}, messages.SUCCESS)   
        for item in queryset:
            if item.condicion:
                item.condicion = False
            else:
                item.condicion = True
            item.save()
        return queryset
    cambiar_activacion.short_description = 'Cambia el estado de la activacion'

    def get_search_fields_(self, request):
        search_fields = []
        if hasattr(self.model,'nombre'):
            search_fields.append('nombre')

        stock = getattr(self.model,'stock',None)
        if stock:
            if hasattr(stock,'producto'):
                search_fields.append('producto')

        if hasattr(self.model,'producto'):
            search_fields.append('producto')

        if hasattr(self.model,'primer_nombre'):
            search_fields.append('primer_nombre')

        if hasattr(self.model,'primer_apellido'):
            search_fields.append('primer_apellido')

        print ('search_fields',search_fields)
        return search_fields

@admin.register(Permiso)
class PermisoAdmin(BaseAdmin):
    pass

@admin.register(Pais)
class PaisAdmin(BaseAdmin):
    pass

@admin.register(Estado)
class EstadoAdmin(BaseAdmin):
    pass

@admin.register(Ciudad)
class CiudadAdmin(BaseAdmin):
    pass


@admin.register(Nota)
class NotaAdmin(BaseAdmin):
    list_display = ('persona','nota')    