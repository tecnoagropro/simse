from .models import TransaccionHistory, Cuenta
from inventario.models import Costo

def nueva_transaccion(*args, **kwargs):

    tipo = kwargs.pop('tipo', None)
    monto = kwargs.pop('monto', None)
    destino = kwargs.pop('destino', None)
    generado = kwargs.pop('generado', True)
    dia_trabajo = kwargs.pop('dia_trabajo', None)
    origen = kwargs.pop('origen', None)
    descripcion = kwargs.pop('descripcion', None)
    nota = ''

    if descripcion is None and tipo == 'CR':
        nota = 'Credito a cuenta %s ' % destino['id']
    
    if descripcion is None and tipo == 'DB':
        nota = 'Debito a cuenta %s ' % destino['id']

    if descripcion:
        nota = descripcion

    if not isinstance(destino, Cuenta):
        destino = Cuenta.objects.get(id=destino['id'])

    transaccion = TransaccionHistory()

    if origen:
        transaccion.origen_id = origen['id']

    if dia_trabajo:
        transaccion.dias_trabajo = dia_trabajo

    transaccion.destino = destino
    transaccion.descripcion = nota
    transaccion.monto, c = Costo.objects.get_or_create(monto=monto) 
    transaccion.generado = generado
    transaccion.tipo = tipo
    transaccion.save()
    nuevo_monto = destino.monto.monto + monto
    destino.monto, c = Costo.objects.get_or_create(monto=nuevo_monto)
    destino.save()

    return transaccion


def revertir_transaccion(transaccion):
    
    if 'Credito' in transaccion.descripcion:
        tipo_inverso = '-'
        nota = 'Reverso de transaccion %d - Debito a cuenta %s' % (transaccion.id, transaccion.destino.id)
    else:
        tipo_inverso = '+'
        nota = 'Reverso de transaccion %d - Credito a cuenta %s' % (transaccion.id, transaccion.destino.id)

    monto = transaccion.monto * (-1)
    destino = transaccion.destino
    dia_trabajo = transaccion.dias_trabajo
    descripcion = nota

    destino.monto += monto
    destino.save()

    return nueva_transaccion(**{
        'tipo':tipo_inverso,
        'monto':monto,
        'destino':destino,
        'dia_trabajo':dia_trabajo,
        'descripcion':nota
    })