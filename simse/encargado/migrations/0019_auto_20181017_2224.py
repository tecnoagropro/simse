# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2018-10-18 02:24
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('inventario', '0018_trasladoencargado'),
        ('encargado', '0018_cierreencargado_cantidad_productos_inventario'),
    ]

    operations = [
        migrations.CreateModel(
            name='DetalleCierreCliente',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('condicion', models.BooleanField(default=True)),
            ],
            options={
                'ordering': ('-id',),
                'get_latest_by': 'id',
            },
        ),
        migrations.CreateModel(
            name='DetalleCierreProducto',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('condicion', models.BooleanField(default=True)),
                ('cantidad', models.DecimalField(decimal_places=2, max_digits=30)),
                ('cierre', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='encargado.CierreEncargado')),
                ('pagado', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='cierre_detalle_producto_pagado_set', to='inventario.Costo')),
                ('pendiente', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='cierre_detalle_producto_pendiente_set', to='inventario.Costo')),
                ('producto', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='inventario.Producto')),
                ('venta', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='cierre_detalle_producto_venta_set', to='inventario.Costo')),
            ],
            options={
                'ordering': ('-id',),
                'get_latest_by': 'id',
            },
        ),
        migrations.AlterField(
            model_name='ventaclienteproducto',
            name='venta',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='productos_ventas', to='encargado.Venta'),
        ),
        migrations.AddField(
            model_name='detallecierrecliente',
            name='cierre',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='encargado.DetalleCierreProducto'),
        ),
        migrations.AddField(
            model_name='detallecierrecliente',
            name='cliente',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='encargado.Cliente'),
        ),
        migrations.AddField(
            model_name='detallecierrecliente',
            name='pagado',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='cierre_detalle_cliente_pagado_set', to='inventario.Costo'),
        ),
        migrations.AddField(
            model_name='detallecierrecliente',
            name='pendiente',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='cierre_detalle_cliente_pendiente_set', to='inventario.Costo'),
        ),
        migrations.AddField(
            model_name='detallecierrecliente',
            name='venta',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='cierre_detalle_cliente_venta_set', to='inventario.Costo'),
        ),
    ]
