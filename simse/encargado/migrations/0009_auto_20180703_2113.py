# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2018-07-03 21:13
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('encargado', '0008_auto_20180703_0326'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='venta',
            options={'ordering': ('-id',)},
        ),
        migrations.AlterModelOptions(
            name='ventaclienteproducto',
            options={'ordering': ('-venta',)},
        ),
        migrations.AlterField(
            model_name='venta',
            name='comentario',
            field=models.TextField(blank=True, null=True),
        ),
    ]
