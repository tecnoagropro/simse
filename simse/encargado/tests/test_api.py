import json
import pytest

from mixer.backend.django import mixer

from datetime import timedelta, datetime
from django.urls import reverse
from django.utils import timezone
from django.http import QueryDict
from rest_framework.test import APIClient
from rest_framework import status

from .fixtures import Fixtures

@pytest.mark.django_db(transaction=True)
class TestActivityFeedViewSet(Fixtures):


    def test_not_allowed_methods(self, api_factory_authenticate,data_action):
        """
        Methods not allowed for api:feed-feed, api:feed-feed-detail,
        api:feed-action-detail
        """

        request = api_factory_authenticate.patch(
            reverse("api:feed-feed", args=("v1",)))
        assert request.status_code == status.HTTP_405_METHOD_NOT_ALLOWED

        request = api_factory_authenticate.put(
            reverse("api:feed-feed", args=("v1",)))
        assert request.status_code == status.HTTP_405_METHOD_NOT_ALLOWED

        request = api_factory_authenticate.put(
            reverse("api:feed-feed-detail", kwargs={'pk': 1, 'version': 'v1'}))
        assert request.status_code == status.HTTP_405_METHOD_NOT_ALLOWED


    def test_all_feed_user(self, api_factory_authenticate,
                           data_all_feed):

        request = api_factory_authenticate.get(
            reverse("api:feed-feed", args=("v1",)))

        assert request.status_code == status.HTTP_200_OK
        assert len(request.json()) == 2


        query_dictionary = QueryDict('', mutable=True)
        query_dictionary.update(
            {
                'world': 'true'
            }
        )

        url = '{base_url}?{querystring}'.format(
            base_url=reverse("api:feed-feed", args=("v1",)),
            querystring=query_dictionary.urlencode()
        )

        request = api_factory_authenticate.get(url)

        assert request.status_code == status.HTTP_200_OK
        assert len(request.json()) == 3

    def test_all_feed_friends(self, data_friends, data_rating_friends):

        factory = APIClient()
        factory.force_authenticate(data_friends[0]) #jean

        query_dictionary = QueryDict('', mutable=True)
        query_dictionary.update(
            {
                'world': 'true'
            }
        )

        url = '{base_url}?{querystring}'.format(
            base_url=reverse("api:feed-feed", args=("v1",)),
            querystring=query_dictionary.urlencode()
        )

        request = factory.get(url)

        assert request.status_code == status.HTTP_200_OK
        print (len(request.json()) )


    def test_get_feed(self, api_factory_authenticate, data_all_feed):

        request = api_factory_authenticate.get(
            reverse("api:feed-feed-detail", kwargs={
                'pk': data_all_feed.pk, 'version': 'v1'}
            ))

        assert request.status_code == status.HTTP_200_OK


    def test_feed(self, api_factory_authenticate,
                        data_rating):

        action = mixer.blend("activityfeed.Action", verb='Pay', description='description')
        new_feed = {
            'url': 'url',
            'content':'Fulanito pago',
            'action':reverse("api:feed-action-detail", kwargs={'pk': action.pk, 'version': 'v1'}),
            'rating': reverse("api:rating_object", kwargs={
                'rating_url': data_rating.rating_url, 'version': 'v1'
            })
        }

        request = api_factory_authenticate.post(
            reverse("api:feed-feed", args=("v1",)), data=new_feed)

        assert request.status_code == status.HTTP_201_CREATED
        
