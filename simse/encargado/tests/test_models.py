from datetime import timedelta
import json

import pytest
from django.conf import settings
from django.db.models.base import ModelBase
from django.utils import timezone
from mixer.backend.django import mixer

from activityfeed import models
from ratings.models import Rating

@pytest.mark.django_db
class TestActivityFeed:

    activity = None

    @pytest.fixture
    def user_a(self):
        return mixer.blend(settings.AUTH_USER_MODEL, username="testa",
                           password="testa", first_name="jovan", last_name="pacheco")

    @pytest.fixture
    def data_rating(self, user_a):

        visitor = mixer.blend("tracking.Visitor",
                              session_key="visitor", ip_address='192.1.1.1',
                              user=user_a, user_agent='fake', referrer='referrer', url='url',
                              page_views=1, session_start=timezone.now(), last_update=timezone.now())

        category = mixer.blend("activities.ActivityCategory",
                               title="category", created_by=user_a, updated_by=user_a)

        activity = mixer.blend("activities.Activity",
                               timezone='America/Denver', category=category, created_by=user_a,
                               caption='caption', content='content', updated_by=user_a)

        activity_ocurrence = mixer.blend("activities.ActivityOccurrence",
                                         start=timezone.now(), original_start=timezone.now(),
                                         purchased_quantity=mixer.RANDOM(1, 5, 2), activity=activity,
                                         end=timezone.now(), original_end=timezone.now())

        return Rating.objects.rate(
            activity=activity_ocurrence, to_user=user_a,
            from_user=user_a, user_provider=user_a,
            score=4, visitor=visitor
        )

    def test_models_django(self, data_rating):
        for model in dir(models):
            model_obj = getattr(models, model)
            if isinstance(model_obj, ModelBase):
                if not model_obj._meta.abstract:
                    if len(model_obj.objects.all()) > 0:
                        name = str(model_obj.objects.all()[0])
                        assert type(name) == str

    def test_feed_model(self, user_a, data_rating):

        action = mixer.blend(
            models.Action, description='description', verb='pay')
        feed = mixer.blend(models.Feed, content='content', action=action,
                           rating=data_rating)

        assert models.Feed.objects.all().count() == 1

