import pytest
from mixer.backend.django import mixer
from django.conf import settings
from datetime import timedelta, datetime
from django.utils import timezone

from activityfeed.models import Action, Feed
from ratings.models import Rating
from activities.models import ActivityOccurrence


class Fixtures:

    @pytest.fixture
    def user_a(self):
        return self.new_user('user_a')

    @pytest.fixture
    def user_b(self):
        return self.new_user('user_b')

    @pytest.fixture
    def other_user(self):
        return self.new_user('other_user')

    @pytest.fixture
    def data_action(self):
        action = mixer.blend(Action, verb='Pay', description='description')
        return action

    @pytest.fixture
    def activity_ocurrence(self, user, activity):

        ao = mixer.blend(ActivityOccurrence,
                         start=timezone.now(), original_start=timezone.now(),
                         purchased_quantity=mixer.RANDOM(1, 5, 2), activity=activity,
                         end=timezone.now(), original_end=timezone.now())
        return ao

    @pytest.fixture
    def activity_ocurrence_other(self, user):

        activity = mixer.blend("activities.Activity",
                               caption='ACTIVITY_NAME', created_by=user, updated_by=user)
        ao = mixer.blend(ActivityOccurrence,
                         start=timezone.now(), original_start=timezone.now(),
                         purchased_quantity=mixer.RANDOM(1, 5, 2), activity=activity,
                         end=timezone.now(), original_end=timezone.now())
        return ao

    @pytest.fixture
    def activity_ocurrence_users(self, user_a):

        activity = mixer.blend("activities.Activity",
                               caption='ACTIVITY_NAME', created_by=user_a, updated_by=user_a)
        ao = mixer.blend(ActivityOccurrence,
                         start=timezone.now(), original_start=timezone.now(),
                         purchased_quantity=mixer.RANDOM(1, 5, 2), activity=activity,
                         end=timezone.now(), original_end=timezone.now())
        return ao

    @pytest.fixture
    def data_rating(self, user, other_user, activity_ocurrence):
        score = mixer.RANDOM(1, 2, 3, 4, 5)
        rating_object = mixer.blend(Rating,
                                    score=score,
                                    from_user=user, activity=activity_ocurrence,
                                    to_user=other_user, user_provider=user)
        return rating_object

    @pytest.fixture
    def data_rating_other(self,user, other_user, activity_ocurrence_other):
        score = mixer.RANDOM(1, 2, 3, 4, 5)
        rating_object = mixer.blend(Rating,
                                    score=score,
                                    from_user=other_user, activity=activity_ocurrence_other,
                                    to_user=user, user_provider=user)
        return rating_object

    @pytest.fixture
    def data_rating_users(self,user_a, user_b, activity_ocurrence_users):
        score = mixer.RANDOM(1, 2, 3, 4, 5)
        rating_object = mixer.blend(Rating,
                                    score=score,
                                    from_user=user_a, activity=activity_ocurrence_users,
                                    to_user=user_b, user_provider=user_b)
        return rating_object

    @pytest.fixture
    def data_all_feed(self,data_action, data_rating, data_rating_other, data_rating_users):

        action = mixer.blend(Action, verb='Pay', description='description')

        mixer.blend(Feed, content='content', action=action,
                    rating=data_rating_other, url='url_test')

        mixer.blend(Feed, content='content', action=action,
                    rating=data_rating_users, url='url_users')

        return mixer.blend(Feed, content='content', action=action,
                           rating=data_rating, url='url')

    def new_user(self, name):
        return mixer.blend(settings.AUTH_USER_MODEL,
                           username=name, email="{}@test.com".format(name))

    @pytest.fixture
    def data_friends(self):

        jean = self.new_user('jean')
        oliver = self.new_user('oliver')
        ethan = self.new_user('ethan')
        emma = self.new_user('emma')
        wilson = self.new_user('wilson')
        andrew = self.new_user('andrew')
        marie = self.new_user('marie')
        sophia = self.new_user('sophia')
        michael = self.new_user('michael')
        olivia = self.new_user('olivia')
        model = "accounts.Friend"

        mixer.blend(model,
                   to_user=jean, from_user=oliver)

        mixer.blend(model,
                    to_user=michael, from_user=wilson)

        mixer.blend(model,
                    to_user=ethan, from_user=emma)

        mixer.blend(model,
                    to_user=jean, from_user=wilson)

        mixer.blend(model,
                    to_user=marie, from_user=sophia)

        mixer.blend(model,
                    to_user=michael, from_user=olivia)
        mixer.blend(model,
                    to_user=olivia, from_user=oliver)

        mixer.blend(model,
                    to_user=andrew, from_user=jean)

        mixer.blend(model,
                    to_user=andrew, from_user=oliver)

        mixer.blend(model,
                    to_user=emma, from_user=sophia)

        mixer.blend(model,
                    to_user=andrew, from_user=ethan)

        users = [jean,oliver,ethan,emma,wilson,andrew,marie,sophia,michael,olivia]
        return users


    def new_activity_ocurrence(self, activity):
        return mixer.blend(ActivityOccurrence,
                           start=timezone.now(), original_start=timezone.now(),
                           end=timezone.now(), original_end=timezone.now(),
                           activity=activity
                           )

    @pytest.fixture
    def data_rating_friends(self, data_friends,data_all_feed):

        jean = data_friends[0]
        oliver = data_friends[1]
        andrew = data_friends[5]
        score = mixer.RANDOM(1, 2, 3, 4, 5)
        activity = mixer.blend("activities.Activity",
                               caption='Sale of smartphone', created_by=oliver,
                               updated_by=oliver)

        ao1 = self.new_activity_ocurrence(activity)
        ao2 = self.new_activity_ocurrence(activity)
        ao3 = self.new_activity_ocurrence(activity)

        rating = mixer.blend(Rating,
                             score=score,
                             from_user=andrew, activity=ao1,
                             to_user=oliver, user_provider=oliver)

        action = mixer.blend(Action, verb='Pay', description='description')

        return mixer.blend(Feed, content='Andrew compro un telefono a oliver', action=action,
                           rating=rating, url='url_test')

