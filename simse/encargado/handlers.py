from decimal import Decimal
from django.db.models import Sum
from inventario.models import Costo, AsignacionStock, CostoProducto
from .models import *

def _nueva_venta(**kwargs):

    encargado = kwargs.pop('encargado', None)
    if not encargado:
        encargado_id = kwargs.pop('encargado_id')
        encargado = Encargado.objects.get(id=encargado_id)

    cliente = kwargs.pop('cliente', None)
    if not cliente:
        cliente_id = kwargs.pop('cliente_id')
        cliente = Cliente.objects.get(id=cliente_id)

    comentario = kwargs.pop('comentario', None)
    
    venta = Venta()

    total = kwargs.pop('total',None)
    if total:
        try:
            total_venta = Costo.adquirir(total)
        except Costo.MultipleObjectsReturned as e:
            total_venta = Costo.objects.filter(monto=total).first() 
        venta.total = total_venta
    
    venta.encargado = encargado
    venta.cliente = cliente
    if comentario:
        venta.comentario = comentario
    venta.save()
    return venta


def _nueva_venta_producto(**kwargs):

    venta = kwargs.pop('venta', None)
    if not venta:
        venta_id = kwargs.pop('venta_id')
        venta = Venta.objects.get(id=venta_id)

    lista_productos = kwargs.pop('lista_productos')

    for producto in lista_productos:

        qs = AsignacionStock.objects.filter(
                condicion=True,
                encargado=kwargs.get('encargado'),
                stock__producto_id=producto['producto_id'],
                aceptado=True)\
                .order_by()

        pendiente = True

        en_stock = qs.values('cantidad_actual').aggregate(cuenta=Sum('cantidad_actual'))['cuenta']
        cantidad_total = producto['cantidad']
        if cantidad_total <= en_stock:
            for stock_producto in qs:
                if pendiente:
                    venta_cliente = VentaClienteProducto()
                    venta_cliente.venta = venta
                    venta_cliente.stock = stock_producto
                    if cantidad_total <= stock_producto.cantidad_actual:
                        venta_cliente.cantidad = cantidad_total
                        pendiente = False
                    else:
                        venta_cliente.cantidad = stock_producto.cantidad_actual
                        cantidad_total = Decimal(cantidad_total)
                        cantidad_total -= Decimal(stock_producto.cantidad_actual)

                    prod = stock_producto.stock.producto
                    cp = CostoProducto.objects.filter(condicion=True, producto=prod).last()
                    monto = cp.monto.monto * Decimal(venta_cliente.cantidad)

                    try:
                        venta_cliente.monto = Costo.adquirir(monto)
                    except Costo.MultipleObjectsReturned as e:
                        venta_cliente.monto = Costo.objects.filter(monto=monto).first()

                    venta_cliente.save()

    return True


def _nuevo_pago(**kwargs):

    venta = kwargs.pop('venta', None)
    if not venta:
        venta_id = kwargs.pop('venta_id')
        venta = Venta.objects.get(id=venta_id)

    lista_de_pagos = kwargs.pop('lista_pagos')

    for item_pago in lista_de_pagos:
        metodo_id = item_pago['metodo_id']

        try:
            monto = Costo.adquirir(float(item_pago['monto']))
        except Costo.MultipleObjectsReturned as e:
            monto = Costo.objects.filter(monto=float(item_pago['monto'])).first()

        pago = PagoVenta()
        pago.venta = venta
        pago.metodo_id = metodo_id
        pago.monto = monto
        try:
            pago.banco_id = item_pago['banco_id']
            pago.referencia = item_pago['referencia']
            pago.save()
        except:
            pago.banco_id = None
            pago.referencia = None
            pago.save()     
        
        if venta.pagado:
            actual = float(venta.pagado.monto) + float(pago.monto.monto)
            try:
                venta.pagado, creado = Costo.objects.get_or_create(monto=actual)
            except Exception as e:
                venta.pagado = Costo.objects.filter(monto=actual).first()
        else:
            venta.pagado = pago.monto
        venta.save()

    return True