#import pytz
from uuid import UUID
from datetime import datetime, date, timedelta
from rest_framework.generics import ListAPIView, RetrieveAPIView,\
     get_object_or_404, CreateAPIView, RetrieveUpdateAPIView
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework import status, viewsets

from django.contrib.auth import get_user_model
from django.db.models import Sum, Q
from django.utils import timezone
from common.mixins import BaseAPIView, BorrarView, BasePostAPIView,\
    MultipleChoicesAPIView

from .serializers import EncargadoSerializer, \
     GananciaEncargadoSerializer,PagoVentaSerializer,\
     DiaTrabajoSerializer, \
     CuentaEncargadoTemporadaSerializer, EncargadoVentaSerializer,\
     TransaccionListSerializer, TransaccionSerializer, VentaSerializer,\
     CuentaSerializer, ClienteSerializer, EncargadoVentaPagarSerializer,\
     BancoSerializer, MetodoPagoSerializer, EncargadoVentaEliminarSerializer

from ..models import Encargado, DiaTrabajo, GananciaEncargado,\
    Cuenta, TransaccionHistory, Cliente, Venta, CierreEncargado, \
    VentaClienteProducto, Banco, MetodoPago, PagoVenta

from inventario.models import AsignacionStock, Costo, CostoProducto, Producto
from inventario.api.serializers import StockEncargadoSerializer


class EncargadoViewSet(ListAPIView, BasePostAPIView):
    """

    get:
        Lista todos los encargados, se puede filtrar

    post:
        Crear nuevo encargado

    """
    serializer_class = EncargadoSerializer
    permission_classes = (IsAuthenticated,)
    queryset = Encargado.objects.filter(condicion=True)


class EncargadoDetailViewSet(RetrieveUpdateAPIView, BorrarView):
    """
    get:
        regresa un encargado

    delete:
        Activa o desactiva a un encargado por pk de url

    patch:
        actualiza el codigo o el usuario de un encargado
    """

    serializer_class = EncargadoSerializer
    permission_classes = (IsAuthenticated,)
    queryset = Encargado.objects.filter()



class ActualizarGitViewSet(ListAPIView, BaseAPIView):
    """
    get:
        Actualiza via git
    """

    serializer_class = EncargadoSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        import os
        os.system('git pull')
        return Encargado.objects.filter()



class StocksEncargadoViewSet(ListAPIView, BaseAPIView):
    """
    get:
        Lista todo el stock para un encargado
    """

    serializer_class = StockEncargadoSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):

        encargado = Encargado.objects.filter(
            id=self.kwargs['pk'], condicion=True
            )
        
        qs = AsignacionStock.objects.filter(
                encargado=encargado,
                cantidad_actual__gt=0,
                aceptado=True,
                condicion=True).order_by("created",'stock__producto__nombre')
        return qs


class DiaTrabajoViewSet(viewsets.ModelViewSet):
    
    serializer_class = DiaTrabajoSerializer
    queryset = DiaTrabajo.objects.all()
    permission_classes = (IsAuthenticated,)


class EncargadoCuentaTemporadaVS(RetrieveAPIView, BaseAPIView):
    """
    get:
        Lista las cuentas de un encargado para una temporada

    """
    serializer_class = CuentaEncargadoTemporadaSerializer
    permission_classes = (IsAuthenticated,)
    queryset = Encargado.objects.filter(condicion=True)
    lookup_field = 'pk'
    lookup_url_kwarg = 'encargado'


class EncargadoVentaViewSet(BasePostAPIView):
    """
    post:
        Proceso de venta de productos

    """
    serializer_class = EncargadoVentaSerializer
    permission_classes = (IsAuthenticated,)
    queryset = Encargado.objects.filter(condicion=True)
    verify_success = False
    
    def get_exception(self, exc):
        raise exc
        return {
            'error_message':str(exc)
        }

class EncargadoVentaPagarViewSet(BasePostAPIView):
    """
    post:
        Proceso de pago de venta.

    """
    serializer_class = EncargadoVentaPagarSerializer
    permission_classes = (IsAuthenticated,)
    queryset = Encargado.objects.filter(condicion=True)
    verify_success = False

    def get_exception(self, exc):
        raise exc
        return {
            'error_message':str(exc)
        }


class EncargadoVentaEliminarViewSet(BasePostAPIView):
    """
    post:
        Proceso de eliminar o reversar una venta.

    """
    serializer_class = EncargadoVentaEliminarSerializer
    permission_classes = (IsAuthenticated,)
    verify_success = False

    def get_exception(self, exc):
        raise exc
        return {
            'error_message':str(exc)
        }


class TransaccionesEncargadoViewSet(ListAPIView, BaseAPIView):
    """

    get:
        Lista todas las operaciones para un encargado

    """
    serializer_class = TransaccionListSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        encargado = get_object_or_404(Encargado,pk=self.kwargs['pk'])
        cuenta = encargado.cuenta
        return TransaccionHistory.objects.filter(
                Q(origen=cuenta) | Q(destino=cuenta)
                )



class TransaccionViewSet(ListAPIView, CreateAPIView, BaseAPIView):
    """

    get:
        Lista todas las transacciones

    post:
        Crea una nueva transaccion

    """
    serializer_class = TransaccionSerializer
    permission_classes = (IsAuthenticated,)
    queryset = TransaccionHistory.objects.all()



class CuentaViewSet(viewsets.ReadOnlyModelViewSet):
    """

    list:
        Lista todas las cuenta, se puede filtrar

    retrive:
        Detalle de una cuenta

    """
    serializer_class = CuentaSerializer
    permission_classes = (IsAuthenticated,)
    queryset = Cuenta.objects.all()


class BancoViewSet(viewsets.ModelViewSet):
    """

    list:
        Lista todos los bancos

    retrive:
        Detalle de un banco

    """
    serializer_class = BancoSerializer
    permission_classes = (IsAuthenticated,)
    queryset = Banco.objects.all()

class VentaViewSet(viewsets.ModelViewSet):
    """

    list:
        Lista todos las ventas

    retrive:
        Detalle de una venta

    """
    serializer_class = VentaSerializer
    permission_classes = (IsAuthenticated,)
    queryset = Venta.objects.all()



class MetodoPagoViewSet(viewsets.ModelViewSet):
    """

    list:
        Lista todos los metodos de pago

    retrive:
        Detalle de un metodo de pago

    """
    serializer_class = MetodoPagoSerializer
    permission_classes = (IsAuthenticated,)
    queryset = MetodoPago.objects.all()




class GananciaEncargadoViewSet(RetrieveAPIView, CreateAPIView, BaseAPIView):
    """

    post:
        Establece el modo y cantidad de ganancia para un encargado

    get:
        Devuelve el modo y cantidad de ganancia para un encargado

    """

    serializer_class = GananciaEncargadoSerializer
    permission_classes = (IsAuthenticated,)

    def get_object(self):
        enc = Encargado.objects.get(id=self.kwargs['pk'])
        return GananciaEncargado.objects.filter(encargado=enc).latest('modified')



class ClienteViewSet(viewsets.ModelViewSet):
    """

    list:
        Realiza una busqueda de cliente o lista a todos

    retrive:
        Detalle de un cliente

    create:
        Crea un nuevo cliente

    """
    serializer_class = ClienteSerializer
    permission_classes = (IsAuthenticated,)
    queryset = Cliente.objects.all()

    def get_queryset(self):
        q = self.request.query_params.get('q', None)

        if q:
            r = Cliente.objects.filter(condicion=True)\
                .filter(Q(ci_rif__icontains=q) |
                        Q(nombre__icontains=q) |
                        Q(apellido__icontains=q))
            
            return r

        return Cliente.objects.filter(condicion=True)


class CorregirPagosViewSet(BaseAPIView):

    def get(self, request, *args, **kwargs):
        data = {}

        ventas = Venta.objects.filter(condicion=True)

        for ve in ventas:
            ve.pagado = None
            pagos = ve.pagos.all()
            pagado = 0
            for pago in pagos:
                pagado += pago.monto.monto
    
            try:
                nuevo_pago = Costo.adquirir(pagado)
            except Costo.MultipleObjectsReturned as e:
                nuevo_pago = Costo.objects.filter(monto=pagado).first() 
                           
            ve.pagado = nuevo_pago
            ve.save()

        return Response(data, status=status.HTTP_200_OK)


class EncargadoVentaReporteViewSet(BaseAPIView):
    """
    get:
        Obtener reporte
    """

    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        v = kwargs.pop('version')
        if v == 'v1':
            return self.version1(request)

        if v == 'v2':
            return self.version2(request)

    def version1(self, request):

        params = request.GET
        enc = request.user.persona_set.encargado_set
        start = None
        enc_cuenta = request.user.persona_set.cuenta_set.latest()
        end = timezone.now()
        pago_encargados = 0
        
        qs = VentaClienteProducto.objects.filter(
             venta__encargado=enc, condicion=True) 

        #print ("qs VentaClienteProducto :", qs)
        try:                
            pagos_enc = TransaccionHistory.objects.activos(
                        destino=enc_cuenta)
            pagos_enc = pagos_enc.aggregate(t=Sum('monto__monto'))  
            pago_encargados = pagos_enc['t']      
        except Exception as e:
            pago_encargados = 0   


        if 'end' in params and 'start' in params:
            if len(params['start']) > 0 and len(params['end']) > 0:
                date_array = params['start'].split('-')
                start = date(int(date_array[2]), int(date_array[1]), int(date_array[0]))
                start = datetime.combine(start, datetime.min.time())
                #start = start.utcnow().replace(tzinfo=pytz.timezone('America/Caracas'))

                date_array = params['end'].split('-')
                end = date(int(date_array[2]), int(date_array[1]), int(date_array[0]))
                end = end + timedelta(days=1)
                end = datetime.combine(end, datetime.min.time())
                              
                try:
                    #end = end.utcnow().replace(tzinfo=pytz.utc)
                    pass
                except Exception as e:
                    print (e)
                    pass
                
                qs = qs.filter(created__range=[start,end])

                try:

                    pagos_enc = TransaccionHistory.objects.activos(
                                destino=enc_cuenta,created__range=[start,end])
                    pagos_enc = pagos_enc.aggregate(t=Sum('monto__monto'))  
                    pago_encargados = pagos_enc['t']      
                except Exception as e:
                    print ("Pago encargado ")
                    print (e)
                    pago_encargados = 0

        ganancia = 0
        pagos = 0
        ya_venta = []
        ## Metodos de pago
        por_transferencia = 0
        por_efectivo = 0
        por_punto_venta = 0
        por_cheques = 0
        por_otros = 0

        for venta in qs:
            if venta.venta.pagado:
                if venta.venta.id not in ya_venta:
                    metodos_pago = PagoVenta.objects.filter(condicion=True, venta=venta.venta)
                    for m_pago in metodos_pago:
                        if m_pago.metodo.id == 3:
                            por_transferencia += m_pago.monto.monto
                            if m_pago.metodo.id == 4:
                                por_efectivo += m_pago.monto.monto
                        if m_pago.metodo.id == 5:
                            por_punto_venta += m_pago.monto.monto
                        if m_pago.metodo.id == 6:
                            por_cheques += m_pago.monto.monto
                        if m_pago.metodo.id == 7:
                            por_otros += m_pago.monto.monto
                                                
                    pagos += venta.venta.pagado.monto
                    ya_venta.append(venta.venta.id)

            ganancia += venta.monto.monto


        deuda_cliente = ganancia - pagos
        total_neto = (deuda_cliente + pago_encargados) if pago_encargados else deuda_cliente
        total_neto = ganancia - total_neto
        
        data = {
            'ganancia':ganancia,
            'deuda':deuda_cliente,
            'total_neto':total_neto,
            'pago_encargados':pago_encargados if pago_encargados else 0,
            'por_transferencia': por_transferencia,
            'por_efectivo': por_efectivo,
            'por_punto_venta': por_punto_venta,
            'por_cheques': por_cheques,
            'por_otros': por_otros
        }

        return Response(data, status=status.HTTP_200_OK)

    def version2(self, request):
        params = request.GET
        try:
            enc = request.user.persona_set.encargado_set
            qs = Venta.objects.filter(
                 encargado=enc, condicion=True) 
        except Exception as e:
            if 'encargado' in params and params['encargado'] is not None:
                qs = Venta.objects.filter(encargado_id=params['encargado'], condicion=True) 
            else:
                qs = Venta.objects.filter(
                     condicion=True) 
        start = None
        end = timezone.now()
        
        if 'end' in params and 'start' in params:
            if len(params['start']) > 0 and len(params['end']) > 0:
                date_array = params['start'].split('-')
                start = date(int(date_array[2]), int(date_array[1]), int(date_array[0]))
                start = datetime.combine(start, datetime.min.time())
                #start = start.utcnow().replace(tzinfo=pytz.timezone('America/Caracas'))

                date_array = params['end'].split('-')
                end = date(int(date_array[2]), int(date_array[1]), int(date_array[0]))
                end = end + timedelta(days=1)
                end = datetime.combine(end, datetime.min.time())
                qs = qs.filter(created__range=[start,end])

        ya_venta = []
        por_transferencia = 0
        por_efectivo = 0
        por_punto_venta = 0

        for venta in qs:
            if venta.pagado:
                if venta.id not in ya_venta:
                    metodos_pago = PagoVenta.objects.filter(condicion=True,
                                                            venta=venta)
                    for m_pago in metodos_pago:
                        if m_pago.metodo.id == 3:
                            por_transferencia += m_pago.monto.monto
                        if m_pago.metodo.id == 4:
                            por_efectivo += m_pago.monto.monto
                        if m_pago.metodo.id == 5:
                            por_punto_venta += m_pago.monto.monto
                                                
                    ya_venta.append(venta.id)

        data = {
            'por_transferencia': por_transferencia,
            'por_efectivo': por_efectivo,
            'por_punto_venta': por_punto_venta,
        }
        return Response(data, status=status.HTTP_200_OK)


class PagoVentaViewSet(viewsets.ModelViewSet):
    """
    list:
        Trae todos los pagos (futuro, filtrar por venta)

    retrive:
        Detalle de una venta

    create:
        Crea un pago
    """
    serializer_class = PagoVentaSerializer
    permission_classes = (IsAuthenticated,)
    queryset = PagoVenta.objects.all()
        

class EncargadoBuscarPrecios(ListAPIView, BaseAPIView):

    serializer_class = PagoVentaSerializer
    permission_classes = (AllowAny,)    

    def list(self, request, *args, **kwargs):
        from django.http import JsonResponse
        list_products = Producto.objects.filter(condicion=True)
        ids = []
        for pro in list_products:
            ids.append(pro.id)

        costos = []
        for id_pro in ids:
            costo = CostoProducto.objects.filter(
                                    condicion=True,
                                    producto_id=id_pro).last()
            if costo:
                costos.append({
                    'costo':costo.monto.monto,
                    'id':costo.producto.id,
                    'nombre':costo.producto.nombre,
                    'unidad':costo.producto.unidad
                    })

        return JsonResponse(costos, safe=False)


class EncargadoActualizarPrecios(CreateAPIView, BaseAPIView):

    serializer_class = PagoVentaSerializer
    permission_classes = (AllowAny,)    

    def post(self, request, *args, **kwargs):
        import requests
        from django.http import JsonResponse
        from decimal import Decimal
        url = 'https://simse.herokuapp.com/api/v1/encargado/buscar_costos_web/'
        r = requests.get(url)
        if r.status_code == 200:
            data = r.json()
            actualizados = []
            ids_productos_conservar = []

            for obj in data:
                ids_productos_conservar.append(obj['id'])

                try:
                    pro_modificar = Producto.objects.get(id=obj['id'])
                    pro_modificar.nombre = obj['nombre']
                    pro_modificar.unidad = obj['unidad']
                    pro_modificar.save()
                except Exception as e:
                    pass
                
                
                try:    
                    costo = CostoProducto.objects.filter(
                        producto__id=obj['id'],
                        condicion=True).latest('id')
                    if costo:
                        ## si tiene costo pero es diferente al que baja
                        if costo.monto.monto != Decimal(obj['costo']):
                            _costo = Costo.adquirir(Decimal(obj['costo']))

                            nuevo = CostoProducto()
                            nuevo.monto = _costo
                            nuevo.producto = costo.producto
                            nuevo.save()
                            sms = '%s ahora cuesta %s' % (nuevo.producto, obj['costo'])
                            actualizados.append(sms)

                except CostoProducto.DoesNotExist as e:
                    np = Producto.objects.create(
                         id=obj['id'], nombre=obj['nombre'], unidad=obj['unidad']
                         )

                    _costo = Costo.adquirir(Decimal(obj['costo']))
                    nuevo = CostoProducto()
                    nuevo.monto = _costo
                    nuevo.producto = np
                    nuevo.save()
                    sms = '%s creado, con costo %s' % (nuevo.producto, obj['costo'])
                    actualizados.append(sms)


            ### borro productos
            a_borrar = Producto.objects.exclude(pk__in=ids_productos_conservar)
            a_borrar.delete()
            return JsonResponse({'datos':actualizados})
        else:
            return JsonResponse({'error':True})

    def get(self, request, *args, **kwargs):
        return self.post(request,*args, **kwargs)