from django.conf.urls import include, url
from rest_framework import routers
from rest_framework.routers import SimpleRouter
from .viewsets import EncargadoViewSet, EncargadoActualizarPrecios, \
    EncargadoDetailViewSet, StocksEncargadoViewSet,\
    TransaccionesEncargadoViewSet, CuentaViewSet, MetodoPagoViewSet, \
    EncargadoVentaViewSet, TransaccionViewSet, BancoViewSet, \
    GananciaEncargadoViewSet, ClienteViewSet, EncargadoVentaPagarViewSet,\
    EncargadoVentaEliminarViewSet, EncargadoVentaReporteViewSet,\
    VentaViewSet, PagoVentaViewSet, CorregirPagosViewSet, EncargadoBuscarPrecios,\
    ActualizarGitViewSet


router = SimpleRouter()
router.register("cuenta", CuentaViewSet)
router.register("banco", BancoViewSet)
router.register("metodo_pago", MetodoPagoViewSet)
router.register("venta", VentaViewSet)
router.register("cliente", ClienteViewSet)
router.register("pago_venta", PagoVentaViewSet)


urlpatterns = [

    url(r'^encargado/(?P<pk>\d+)/$',
        EncargadoDetailViewSet.as_view(), name="encargado-detail"),

    url(r'^encargado/(?P<pk>\d+)/stocks/$',
        StocksEncargadoViewSet.as_view(), name="encargado-stocks"),

    url(r'^encargado/(?P<pk>\d+)/transacciones/$',
        TransaccionesEncargadoViewSet.as_view(), name="encargado-transacciones"),

    url(r'^encargado/(?P<pk>\d+)/ganancia/$',
        GananciaEncargadoViewSet.as_view(), name="encargado-ganancia"),

    url(r'^encargado/venta/$', EncargadoVentaViewSet.as_view(), name="encargado-venta"),
    url(r'^encargado/venta/pagar/$', EncargadoVentaPagarViewSet.as_view(), name="encargado-venta-pagar"),    
    url(r'^encargado/venta/eliminar/$', EncargadoVentaEliminarViewSet.as_view(), name="encargado-venta-eliminar"),    
    url(r'^encargado/venta/reporte$', EncargadoVentaReporteViewSet.as_view(), name="encargado-venta"),

    ### actualizar costos remotamente.
    url(r'^encargado/buscar_costos_web/$', EncargadoBuscarPrecios.as_view(), name="encargado-buscar_costos_web"),
    url(r'^encargado/actualizar_costos_web/$', EncargadoActualizarPrecios.as_view(), name="encargado-actualizar_costos_web"),
    ### actualizar costos remotamente.

    url(r'^encargado/corregir_pagos$', CorregirPagosViewSet.as_view(), name="corregir-pagos"),

    url(r'^encargado/$', EncargadoViewSet.as_view(), name="encargado"),

    url(r'^transaccion/$', TransaccionViewSet.as_view(), name="transaccion"),

    url(r'^actualizar_git/$',ActualizarGitViewSet.as_view(), name="actualizar_git"),
    
    # url(r'^cliente/$', ClienteViewSet.as_view(), name="cliente"),
    # url(r'^cliente/(?P<pk>\d+)$', ClienteViewSet.as_view(), name="cliente-detail"),

    url(r'^', include(router.urls)),



]
