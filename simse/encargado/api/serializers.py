from django.contrib.auth import get_user_model
from django.urls import resolve
from django.utils.translation import ugettext as _
from django.db.models import Sum

from rest_framework import serializers
from rest_framework.serializers import HyperlinkedModelSerializer,raise_errors_on_nested_writes
from rest_framework.generics import get_object_or_404
from rest_framework.compat import set_many
from rest_framework.utils import model_meta
from rest_framework.fields import SerializerMethodField

from common.serializers import QueryFieldsMixin,PermisoSerializer

from accounts.serializers import PersonaALLSerializer
from accounts.models import Persona

from ..models import Encargado,  DiaTrabajo, GananciaEncargado,\
    Cuenta, TransaccionHistory, Cliente, Venta, PagoVenta, \
    VentaClienteProducto, Banco, MetodoPago
    
from ..handlers import _nueva_venta, _nueva_venta_producto, _nuevo_pago

from ..utils import nueva_transaccion

from inventario.models import Stock, AsignacionStock, CostoProducto

User = get_user_model()


M_hola = _("Hola.")
M_bienvenido = _("Bienvenido.")
M_pago_mayor = _("El monto a pagar no puede ser mayor que la deuda.")
M_sin_fondos = _("La cuenta de origen no tiene fondos suficientes.")
M_misma_cuenta = _("La cuenta de origen no puede ser la misma que la de destino.")
M_stock_superado_error = _("Stock superado ó no aceptado.")
M_stock_superado_venta = _("La cantidad a vender supera al stock.")
M_stock_superado = _("Cantidad en stock superada.")
M_sin_stock = _("No tiene stock para el producto.")
M_sin_costo = _("Este producto (%s) no tiene costo, debe agregar uno.")
M_icono_modificar = _("Icono para modificar.")
M_url_d_admin = _("URL de redirección de dashboard admin.")
M_url_d_encargado = _("URL de redirección de dashboard encargado.")
M_cantidad_inválida = _("Cantidad inválida.")


class EncargadoSerializer(QueryFieldsMixin, HyperlinkedModelSerializer):

    nombre = serializers.CharField(required=True)
    permisos = PermisoSerializer(many=True, read_only=True)
    locacion = serializers.CharField()
    persona = serializers.CharField(write_only=True,source="persona.id")
    datos_personales = PersonaALLSerializer(source="persona", read_only=True)
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())

    def create(self, validated_data):

        try:
            encargado = Encargado.objects.create(
                persona_id=validated_data["persona"]["id"],
                nombre=validated_data["nombre"],
                locacion=validated_data["locacion"],
            )

        except Exception as e:
            raise e
        return encargado

    def update(self, instance, validated_data):
        raise_errors_on_nested_writes('update', self, validated_data)
        info = model_meta.get_field_info(instance)

        for attr, value in validated_data.items():
            if attr in info.relations and info.relations[attr].to_many:
                set_many(instance, attr, value)
            else:
                if attr == 'user':
                    setattr(instance,attr,value['pk'])
                else:
                    setattr(instance, attr, value)

        instance.save()
        return instance

    class Meta:
        model = Encargado
        fields = ('id','nombre','permisos','persona','locacion',
                  'datos_personales','user')


class DiaTrabajoSerializer(serializers.ModelSerializer):

    user = serializers.HiddenField(default=serializers.CurrentUserDefault())
    fecha_registro = serializers.DateField(required=True, format="%d/%m/%Y")

    def validate(self, attrs):
            
        errors = dict()

        if errors:
            raise serializers.ValidationError(errors)

        return super(DiaTrabajoSerializer, self).validate(attrs)

    class Meta:
        model = DiaTrabajo
        fields = ('id','user','fecha_registro')



class GananciaEncargadoSerializer(QueryFieldsMixin,
                                  HyperlinkedModelSerializer):

    ganancia = serializers.IntegerField(write_only=True)
    ganancia_actual = serializers.IntegerField(read_only=True,
                                               source='ganancia')
    tipo = serializers.IntegerField(write_only=True)
    tipo_actual = serializers.IntegerField(read_only=True,
                                           source='tipo')

    def create(self, validated_data):

        encargado = self.context['view'].kwargs['pk']
        instance, creado = GananciaEncargado.objects.get_or_create(
                    ganancia = validated_data.get('ganancia'),
                    encargado_id = encargado,
                    tipo=validated_data.get('tipo'))
        instance.save()

        return instance

    class Meta:
        model = GananciaEncargado
        fields = ('id', 'ganancia', 'tipo_actual',
                  'ganancia_actual','tipo')


class GananciaEncargadoListSerializer(QueryFieldsMixin,
                                      HyperlinkedModelSerializer):

    ganancia = serializers.IntegerField(read_only=True)
    tipo = serializers.IntegerField(read_only=True)

    class Meta:
        model = GananciaEncargado
        fields = ('id', 'ganancia','tipo')


class CuentaEncargadoTemporadaSerializer(QueryFieldsMixin,
                                         HyperlinkedModelSerializer):

    balance = SerializerMethodField()
    abonos = SerializerMethodField()
    retiros = SerializerMethodField()
    moneda = SerializerMethodField()
    ganancias = SerializerMethodField()

    def get_balance(self, obj):
        cuenta = obj.persona.cuenta_set.last()
        return cuenta.monto

    def get_abonos(self, obj):
        cuenta = obj.persona.cuenta_set.last()
        temporada = self.context['view'].kwargs['temporada']
        resp = TransaccionHistory.objects.cantidad_abonos(
               cuenta,temporada)
        return resp

    def get_retiros(self, obj):
        cuenta = obj.persona.cuenta_set.last()
        temporada = self.context['view'].kwargs['temporada']
        resp = TransaccionHistory.objects.cantidad_retiros(
               cuenta,temporada)
        return resp

    def get_ganancias(self,obj):
        ganancias = GananciaEncargado.objects.filter(encargado=obj)
        new_context = {
            "request": self.context["request"]
        }

        data = GananciaEncargadoListSerializer(ganancias,
               context=new_context, many=True).data

        return data

    class Meta:
        model = Encargado
        fields = ('id', 'balance','abonos','retiros',
                  'moneda','ganancias')


class EncargadoSesion(object):
    def set_context(self, serializer_field):
        persona = serializer_field.context['request'].user.persona_set
        self.encargado = Encargado.objects.get(persona=persona)

    def __call__(self):
        return self.encargado

    def __repr__(self):
        return unicode_to_repr('%s()' % self.__class__.__name__)



class VentaClienteProductoJSONSerializer(serializers.Serializer):
    producto_id = serializers.IntegerField(write_only=True)
    cantidad = serializers.IntegerField(write_only=True)


class EncargadoVentaSerializer(QueryFieldsMixin,
                               HyperlinkedModelSerializer):

    cliente_id = serializers.IntegerField(write_only=True)
    lista_productos = VentaClienteProductoJSONSerializer(many=True)
    lista_productos_string = serializers.CharField(required=False)
    encargado = serializers.HiddenField(default=EncargadoSesion())

    def validate(self, attrs):
        import json
        errors = dict()
        
        lista_productos_string = attrs.get('lista_productos_string',None)

        if lista_productos_string:
            lista_productos = json.loads(lista_productos_string)
        else:
            lista_productos = attrs.get('lista_productos')  
            
        attrs['lista_productos'] = lista_productos
        
        for producto in lista_productos:
            if producto is not None:
                qs = AsignacionStock.objects.filter(
                        condicion=True,
                        encargado=attrs.get('encargado'),
                        stock__producto_id=producto['producto_id'],
                        aceptado=True)\
                        .order_by()

                if qs.count() > 0:
                    en_stock = qs.values('cantidad_actual').aggregate(cuenta=Sum('cantidad_actual'))['cuenta']

                    if en_stock < float(producto['cantidad']):
                        errors['stock'] = M_stock_superado_error
                else:
                    errors['stock'] = M_sin_stock
                

                mi_costo = CostoProducto.objects.filter(condicion=True,
                producto_id=producto['producto_id']).last()
                if not mi_costo:
                    print ("Seguramente sin costo")
                    errors['stock'] = M_sin_costo % str(producto['productos'])
                
        if errors:
            raise serializers.ValidationError(errors)

        return super(EncargadoVentaSerializer, self).validate(attrs)

    def create(self, validated_data):

        venta = _nueva_venta(**validated_data)
        validated_data.update({'venta':venta})
        venta_producto = _nueva_venta_producto(**validated_data)

        if not venta_producto:
            venta.reversar()
            return {'success':False,'errors': M_stock_superado_error}
        else:
            venta.terminada = True
            venta.save()
            #_nuevo_pago(**validated_data)

        return venta

    class Meta:
        model = Encargado
        fields = ('id', 'cliente_id', 'lista_productos','lista_productos_string',
                  'encargado',)


class PagoVentaJSONSerializer(serializers.Serializer):
    metodo_id = serializers.IntegerField(write_only=True)
    monto = serializers.DecimalField(max_digits=30,
                                     decimal_places=5,
                                     write_only=True,
                                     required=True)
    banco_id = serializers.IntegerField(write_only=True, required=False)
    referencia = serializers.CharField(required=False)


class EncargadoVentaPagarSerializer(QueryFieldsMixin,
                                    HyperlinkedModelSerializer):

    venta_id = serializers.IntegerField(write_only=True)
    lista_pagos = PagoVentaJSONSerializer(many=True, required=False)
    lista_pagos_string = serializers.CharField(required=False)

    def validate(self, attrs):
        errors = dict()
        import json
        
        lista_pagos_string = attrs.get('lista_pagos_string', None)

        if lista_pagos_string:
            lista_pagos = json.loads(lista_pagos_string)
        else:
            lista_pagos = attrs.get('lista_pagos')  
            
        attrs['lista_pagos'] = lista_pagos        

        total_pago = 0 

        for item_pago in lista_pagos:
            total_pago += float(item_pago['monto'])

        venta = Venta.objects.get(id=attrs.get('venta_id'))
        if venta.pagado:
            pagado = venta.pagado.monto
        else:
            pagado = 0
        restante = venta.total.monto - pagado
        if total_pago > restante:
            errors['lista_pagos'] = M_pago_mayor


        if errors:
            raise serializers.ValidationError(errors)

        return super(EncargadoVentaPagarSerializer, self).validate(attrs)

    def create(self, validated_data):
        _nuevo_pago(**validated_data)
        return validated_data

    class Meta:
        model = Venta
        fields = ('id', 'venta_id','lista_pagos','lista_pagos_string')


class TransaccionListSerializer(QueryFieldsMixin, HyperlinkedModelSerializer):

    origen = serializers.IntegerField(source="origen.id")
    destino = serializers.IntegerField(source="destino.id")
    monto = SerializerMethodField()

    def get_monto(self, obj):
        return obj.monto.monto

    class Meta:
        model = TransaccionHistory
        fields = ('id','origen','destino','monto','descripcion')



class TransaccionSerializer(QueryFieldsMixin, HyperlinkedModelSerializer):

    cuenta_origen = serializers.IntegerField(source="origen.id", 
                                             write_only=True,
                                             required=False)

    cuenta_destino = serializers.IntegerField(source="destino.id", 
                                              write_only=True,
                                              required=True)

    monto = serializers.DecimalField(max_digits=30,
                                     decimal_places=5,
                                     write_only=True,
                                     required=True)

    generado = serializers.BooleanField(default=False)

    def validate(self, attrs):
            
        errors = dict()
        destino = attrs.get('destino',None)
        origen = attrs.get('origen',None)
        monto = attrs.get('monto',None)

        if monto == 0:
            errors['monto'] = 'Monto no puede ser cero'

        if origen:
            cuenta_origen = Cuenta.objects.get(id=origen['id'])
            balance = cuenta_origen.monto
            
            if monto > balance:
                errors['cuenta_origen'] = []
                errors['cuenta_origen'].append(M_sin_fondos)
        
            if origen['id'] == destino['id']:
                if 'cuenta_origen' not in errors:
                    errors['cuenta_origen'] = []
                errors['cuenta_origen'].append(M_misma_cuenta)

        if errors:
            raise serializers.ValidationError(errors)

        return super(TransaccionSerializer, self).validate(attrs)

    def create(self, validated_data):
        origen = validated_data.pop('origen',None)
        destino = validated_data.pop('destino',None)
        monto = validated_data.pop('monto',None)
        generado = validated_data.pop('generado')
        descripcion = validated_data.pop('descripcion',None)

        if monto > 0:
            tipo = 'CR'
        else:
            tipo = 'DB'

        instance = nueva_transaccion(**{
                'tipo': tipo,
                'monto': monto,
                'destino': destino,
                'generado':generado,
                'descripcion':descripcion
        })
        return instance

    def update(self, instance, validated_data):
        print ('in update')
        return validated_data
        

    class Meta:
        model = TransaccionHistory
        fields = ('id','origen','destino','monto','descripcion',
                  'cuenta_origen','cuenta_destino','generado')

        extra_kwargs = {
            'destino': {'required': False,
                        'view_name':'api:cuenta-detail'},
            'origen': {'view_name':'api:cuenta-detail'}
        }



class CuentaSerializer(QueryFieldsMixin, HyperlinkedModelSerializer):
    persona = serializers.HyperlinkedRelatedField(queryset=Persona.objects.all(),
                                                  view_name="api:persona-detail")
    class Meta:
        model = Cuenta
        fields = ('id','monto','persona')  


class BancoSerializer(QueryFieldsMixin, HyperlinkedModelSerializer):
    class Meta:
        model = Banco
        fields = ('id','nombre','orden')  


class ClienteSerializer(QueryFieldsMixin, HyperlinkedModelSerializer):

    encargado_id = serializers.IntegerField(write_only=True,
                                            required=True)

    class Meta:
        model = Cliente
        fields = ('id','nombre','apellido','telefono',
                  'encargado_id','ci_rif')  


class MetodoPagoSerializer(QueryFieldsMixin, HyperlinkedModelSerializer):

    class Meta:
        model = MetodoPago
        fields = ('id','metodo',)  



class EncargadoVentaEliminarSerializer(serializers.Serializer):

    venta_id = serializers.IntegerField(write_only=True)

    def create(self, validated_data):
        venta_id = validated_data.get('venta_id')
        venta = Venta.objects.get(id=venta_id)
        venta.reversar()

        return {"success": True }


class VentaSerializer(QueryFieldsMixin, HyperlinkedModelSerializer):
    encargado = serializers.HyperlinkedRelatedField(queryset=Encargado.objects.all(),
                                                   view_name="api:encargado-detail")
    total = serializers.DecimalField(max_digits=30,
                                     decimal_places=2,
                                     read_only=True,
                                     source='total.monto')
    pagado = serializers.DecimalField(max_digits=30,
                                      decimal_places=2,
                                      read_only=True,
                                      source='pagado.monto')
    cliente = serializers.HyperlinkedRelatedField(queryset=Cliente.objects.all(),
                                                   view_name="api:cliente-detail")

    class Meta:
        model = Venta
        fields = ('id','encargado','total','pagado',
                  'cliente','comentario','terminada','restante')          



class PagoVentaSerializer(QueryFieldsMixin, HyperlinkedModelSerializer):

    class Meta:
        model = PagoVenta
        fields = ('id','referencia','banco',)  

