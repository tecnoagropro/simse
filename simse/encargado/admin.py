from django.contrib import admin, messages
from common.admin import BaseAdmin
from .models import *


class EncargadoAdmin(BaseAdmin):

    list_display = ('apodo','nombre',
    	            'apellido','ganancia','user','persona')

    list_editable = ['nombre',]

    def apodo(self,obj):
        return obj.nombre

    def nombre(self,obj):
        return obj.persona.primer_nombre

    def apellido(self,obj):
        return obj.persona.primer_apellido

    def user(self,obj):
        return obj.persona.user or None

    def ganancia(self,obj):
        return obj.ganancia_actual() or None

admin.site.register(Encargado,EncargadoAdmin)
admin.site.register(Cuenta)
admin.site.register(TransaccionHistory)
admin.site.register(Cliente)
admin.site.register(MetodoPago)
admin.site.register(Banco)



@admin.register(GananciaEncargado)
class GananciaEncargadoAdmin(BaseAdmin):
    pass



@admin.register(VentaClienteProducto)
class VentaClienteProductoAdmin(BaseAdmin):

    list_display = ('venta','stock','cantidad',
                    'monto','condicion')
    list_editable = ['monto','condicion']

    actions = ['cambiar_activacion','reversar_compras']

    def reversar_compras(self, request, queryset):
        n = queryset.count()
        self.message_user(request,"%(count)d ventas reversadas." % {"count": n }, messages.SUCCESS)   
        for item in queryset:
            item.reversar()
        return queryset
    reversar_compras.short_description = 'Devuelve al stock del Encargado'



@admin.register(Venta)
class VentaAdmin(BaseAdmin):

    list_display = ('id','encargado','cliente', 'total',
                    'pagado','condicion')
    list_filter = ('cliente',)
    actions = ['reversar_compras']

    def reversar_compras(self, request, queryset):
        n = queryset.count()
        self.message_user(request,"%(count)d ventas reversadas." % {"count": n }, messages.SUCCESS)   
        for item in queryset:
            item.reversar()
        return queryset
    reversar_compras.short_description = 'Devuelve al stock del Encargado'


@admin.register(CierreEncargado)
class CierreEncargadoAdmin(BaseAdmin):
    list_display = ('encargado','fecha_inicio',
                    'fecha_fin','total','pagado','pago_encargados', 'condicion')



@admin.register(DetalleCierre)
class DetalleCierreAdmin(BaseAdmin):
    list_display = ('cliente','venta','pagado','pendiente', 'condicion')



@admin.register(DetalleCierreCliente)
class DetalleCierreCAdmin(BaseAdmin):
    list_display = ('cliente','venta','pagado','pendiente', 'condicion')

@admin.register(DetalleCierreProducto)
class DetalleCierrePAdmin(BaseAdmin):
    list_display = ('producto','venta','condicion')        


@admin.register(PagoVenta)
class PagoVentaPAdmin(BaseAdmin):
    list_display = ('metodo','monto','venta','venta_pagada','condicion')     
    list_filter = ('metodo',)

    def venta_pagada(self, obj):
        return obj.venta.esta_pagada