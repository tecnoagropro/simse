from django.shortcuts import render
from django.http import JsonResponse

from django.shortcuts import get_object_or_404
from django.views.generic import ListView
from django.contrib.auth.decorators import permission_required
from django.db.models import Q
from .models import Encargado, Cuenta, TransaccionHistory, Venta, CierreEncargado
from common.utils import permission


def listar(request):
    return render(request,'listar_encargado.html',{})


def listar_ventas(request):
    return render(request,'listar_ventas.html',{
    	'params':request.GET,
        'template':"base_ajax.html"
    })


def listar_ventas_encargado(request, pk):
    return render(request,'listar_ventas.html',{
    	'pk_de_encargado':pk,
        'template':"base_ajax.html"
    })


def listar_ventas_cliente(request, pk):
    return render(request,'listar_ventas.html',{
    	'pk_de_cliente':pk,
        'params':request.GET,
        'template':"base_ajax.html"
    })

def listar_ventas_cliente_iframe(request, pk):
    return render(request,'listar_ventas.html',{
        'pk_de_cliente':pk,
        'params':request.GET,
        'template':"base_simple.html"
    })



def registrar(request):
    return render(request,'registrar_encargado.html',{})
registrar.permisos = ["encargado:add_encargado",]


def modificar(request,pk):
	context = {
		'encargado_id' : pk,
		'persona_id': Encargado.objects.get(id=pk).persona.id
	}
	return render(request,'modificar_encargado.html',context)    


def utilidades(request):
    return render(request,'utilidades.html',{})	


def encargados(request):
    return render(request,'encargados.html',{}) 


def ventas(request):
    return render(request,'ventas.html',{}) 


def pagos(request):
    return render(request,'pagos.html',{}) 


def pagos_venta(request, pk):
    venta = Venta.objects.get(pk=pk)
    return render(request,'pagos.html',{
        'pk_de_venta':pk,
        'deuda':venta.restante
    })


def listar_clientes(request):
    return render(request,'listar_clientes.html',{}) 


def listar_clientes_encargado(request, pk):
    return render(request,'listar_clientes.html',{
    	'pk_de_encargado':pk
    })


def cuentas_encargado(request,pk):

	encargado = Encargado.objects.get(id=pk)
	cuenta = Cuenta.objects.get(persona=encargado.persona)
	operaciones = TransaccionHistory.objects.filter(
		Q(origen=cuenta) | Q(destino=cuenta)
		).filter(generado=False)
    
	context = {
		'encargado_id' : pk,
		'nombre': encargado.persona.name(),
		'persona_id': encargado.persona.id,
		'cuenta_id': cuenta.id,
		'saldo': cuenta.monto.monto,
		'simbolo': 'Bss',
		'operaciones':operaciones.count()
	}	
	return render(request,'cuentas_encargado.html',context)	
    

def listar_inventario(request):
    return render(request,'listar_inventario_encargado.html',{})


def encargado_ventas_reporte(request):
    params = request.GET
    try:
        enc = request.user.persona_set.encargado_set
        ultimo_cierre = CierreEncargado.objects.filter(condicion=True,
                        encargado=enc).latest()
    except Exception as e:
        ultimo_cierre = None
    
    return render(request,'encargado_ventas_reporte.html',{
    	'params':params,
        'ultimo_cierre':ultimo_cierre
    }) 
    

def encargado_metodo_reporte(request):
    params = request.GET
    return render(request,'encargado_metodo_pago_reporte.html',{
        'params':params,
    }) 
