from django.http import JsonResponse
from django.utils import timezone
from .models import Encargado, VentaClienteProducto, Venta,\
    GananciaEncargado, TransaccionHistory, Cliente, PagoVenta, CierreEncargado
from common.mixins import DataTable, Botones
from django.db.models import  Q,F
from inventario.models import AsignacionStock, CostoProducto


class ListarEncargado(DataTable):
    model = Encargado
    columns = ['id', 'nombre','persona.primer_nombre','persona.primer_apellido','persona.mobile','ganancia','botones']
    order_columns = ['id', 'nombre','persona.primer_nombre','persona.primer_apellido','persona.mobile','ganancia',]
    max_display_length = 1000

    def render_column(self, row, column):
        if column == 'botones':
            btn = Botones()
            btn.href('#/encargado/modificar/'+str(row.id),'info','edit','Modificar encargado')
            btn.href('#/encargado/modificar/'+str(row.id)+'?asignar=1','warning','glasses')
            btn.href('#/encargado/cuentas/'+str(row.id),'success','money-bill')
            btn.href('#/encargado/ventas/listar/%s' % (row.id),'success','check-double')

            if row.condicion:
                btn.click("eliminar_dato('"+str(row.id)+"')" , 'danger', 'recycle')
            else:
                btn.click("eliminar_dato('"+str(row.id)+"')" , 'success', 'check')
            return str(btn)
        elif column == 'ganancia':
            try:
                g = GananciaEncargado.objects.filter(encargado=row).latest('modified').ganancia
            except Exception as e:
                g = 'No definida'
            
            return str(g)
        else:
           return super(ListarEncargado, self).render_column(row, column)

    def get_initial_queryset(self):
        return self.model.objects.filter().order_by('-condicion')

    def filter_queryset(self, qs):
        search = self.request.POST.get(u'search[value]', None)

        if search:
            qs = qs.filter(nombre__icontains = search)

        if len(qs) == 0:
            qs = qs.filter(persona__primer_nombre__icontains = search)

        if len(qs) == 0:
            qs = qs.filter(persona__primer_apellido__icontains = search)

        if len(qs) == 0:
            qs = qs.filter(persona__numero_documento__icontains = search)

        return qs


class ListarStockEncargado(DataTable):
    model = AsignacionStock
    columns = ['stock.producto.nombre','cantidad_recibida','monto','botones']
    order_columns = ['stock.producto.nombre']
    max_display_length = 1000

    def render_column(self, row, column):

        if column == 'botones':
            cad = '&nbsp'
            if row.condicion:
                cad=cad + '<a type="button" class="btn btn-danger" onclick="eliminar_dato('+str(row.id)+')"><i class="fa fa-recycle"></i></a>'
            else:
                cad=cad + '<a type="button" class="btn btn-success" onclick="eliminar_dato('+str(row.id)+')"><i class="fa fa-check"></i></a>'
                    
            return cad
        else:
           return super(ListarStockEncargado, self).render_column(row, column)

    def get_initial_queryset(self):
        return self.model.objects.filter(condicion=True, aceptado=True)

    def filter_queryset(self, qs):
        search = self.request.POST.get(u'search[value]', None)
        if search:
            qs = qs.filter(stock__producto__nombre__icontains = search)
        return qs


class ListarTransacciones(DataTable):
    model = TransaccionHistory
    columns = ['created','tipo','monto','descripcion']
    order_columns = columns
    max_display_length = 1000

    def prepare_results(self, qs):
        qs = self.get_queryset()
        json_data = []
        for item in qs:
            tipo = 'Abono' if item.tipo == 'CR' else 'Retiro'
            json_data.append([
                str(item.created.strftime("%d-%m-%Y")),
                str(tipo),
                str(item.monto), 
                str(item.descripcion)
            ])
        return json_data  

    def get_queryset(self):
        try:
            encargado = Encargado.objects.get(pk=self.pk)
            cuenta = encargado.cuenta
            query = TransaccionHistory.objects.filter(
                        Q(origen=cuenta) | Q(destino=cuenta)
                    )
            return query.filter(condicion=True)
        except Encargado.DoesNotExist:
            raise e
        except AttributeError as e:
            query = TransaccionHistory.objects.filter(
                condicion=True
            )
            return query

    def filter_queryset(self, qs):          
        return qs


class ListarTransaccionesEncargado(ListarTransacciones):
    pass

class MiProducto(object):
    pass

class ListarVentas(DataTable):
    default_context = False
    model = Venta
    columns = ['encargado','cliente','ci_rif','total','pagado','deuda','productos','created','botones']
    order_columns = ['encargado','cliente','total','pagado','deuda','productos','created']
    max_display_length = 1000

    def prepare_results(self, qs):
        qs = self.get_queryset()
        json_data = []
        for item in qs:
            btn = Botones()
            btn.click("eliminar_dato('"+str(item.id)+"')" , 'danger', 'recycle')
            btn.href('#/encargado/ventas/pagos/'+str(item.id),'info','money-bill','Pagos')

            vp = item.productos_ventas.all()
            vid_productos = []
            productos = []
            for v in vp:
                vid = v.stock.stock.producto.id
                if vid not in vid_productos:
                    vid_productos.append(vid)
                    setattr(self,'pc%s'%str(v.stock.stock.producto),v.cantidad)
                    productos.append(str(v.stock.stock.producto))
                else:
                    oc = getattr(self,'pc%s'%str(v.stock.stock.producto))
                    setattr(self,'pc%s'%str(v.stock.stock.producto),v.cantidad + oc)

            if len(productos) > 0:
                str_productos = ''
                for p in productos:
                    cant_p = getattr(self,'pc%s'%str(p))
                    str_productos = str_productos + p + " " + str(cant_p) + "<br>"
            else:
                str_productos = 'Sin productos'

            json_data.append([
                str(item.encargado),
                str(item.cliente),
                str(item.cliente.ci_rif),
                str(item.total), 
                str(item.pagado) if item.pagado else 'Sin pagos',
                str(item.restante),
                str_productos,
                str(item.created.strftime("%d-%m-%Y")),
                str(btn)
            ])
        return json_data  

    def get_queryset(self):
        try:
            encargado = Encargado.objects.get(pk=self.pk)
            query = Venta.objects.filter(
                    condicion=True, terminada=True, encargado=encargado
                    )
        except Encargado.DoesNotExist:
            raise e


        except AttributeError as e:
            query = Venta.objects.filter(
                    condicion=True, terminada=True
                    )
           
        try:
            query = query.filter(created__range=[self.start, self.end])

        except AttributeError as e:
            pass


        filter_deuda = self._querydict.get('filter_deuda', None)
        if filter_deuda:
            if filter_deuda != 'Deuda':
                nuevos_pks = []
                print ('filter_deuda',filter_deuda)
                if filter_deuda == 'NO':
                    query = query.filter(pagado__monto__gte=F('total__monto'))
                else:
                    query = query.exclude(pagado__monto__gte=F('total__monto'))
                

        from django.core.paginator import Paginator
        start = int(self._querydict.get('start'))
        length = int(self._querydict.get('length'))

        paginator = Paginator(query, length)
        # print (paginator.num_pages)
        # print (str(paginator.num_pages / int(5)))

        try:
            if start == length:
                page = paginator.page(2)
            else:
                page = paginator.page(start/length)
        except Exception as e:
            print (e)
            page = paginator.page(1)

        self.total_records = page.object_list.count()
        self.total_display_records = len(query)
        return page.object_list

    def filter_queryset(self, qs):
        return qs    

    def get_initial_queryset(self):
        return self.model.objects.filter(condicion=True, terminada=True)


class ListarVentasEncargado(ListarVentas):
    pass


class ListarVentasCliente(ListarVentas):
    def get_queryset(self):
        cliente = None

        try:
            cliente = Cliente.objects.get(pk=self.pk)
        except Cliente.DoesNotExist:
            raise e

        try:
            return Venta.objects.filter(
                created__range=[self.start,self.end], cliente=cliente,
                condicion=True, terminada=True
                )
        except AttributeError as e:
            pass

        query = Venta.objects.filter(
                condicion=True, terminada=True, cliente=cliente
                )
        return query


class ListarVentasReporte(DataTable):
    model = VentaClienteProducto
    columns = ['cliente','telefono','producto','cantidad','costo','total',]
    order_columns = columns
    max_display_length = 1000
    default_context = False

    def prepare_results(self, qs):
        qs = self.get_queryset()       
        json_data = []
        for item in qs:
            if item.cantidad:
                costo_en_venta = item.monto.monto / item.cantidad
            else:
                costo_en_venta = 0

            json_data.append([
                str(item.venta.cliente),
                str(item.venta.cliente.telefono),
                str(item.stock.stock.producto),
                str(item.cantidad),
                str(costo_en_venta),
                str(item.monto.monto)
            ])
        return json_data  

    def get_queryset(self):
        try:
            enc = self.usuario.persona_set.encargado_set

            query = VentaClienteProducto.objects.filter(
                     venta__encargado=enc,condicion=True) 

        except Encargado.DoesNotExist:
            raise e

        except AttributeError as e:
            query = VentaClienteProducto.objects.filter(
                condicion=True)

        try:
            miq = query.filter(created__range=[self.start,self.end])
            print ('miq',miq.count())
            return miq
        except AttributeError as e:
            print ("AttributeError ListarVentasReporte")
            print (e)
            
        return query

    def filter_queryset(self, qs):          
        return qs    


class ListarVentasReporteEncargado(ListarVentasReporte):
    pass


class ListarClientes(DataTable):
    model = Cliente
    columns = ['encargado','cliente','total','pagado','created','botones']
    order_columns = columns
    max_display_length = 1000

    def prepare_results(self, qs):
        qs = self.get_queryset()
        json_data = []
        for item in qs:

            deudas = 0
            all_ventas = Venta.objects.filter(condicion=True,
                        cliente=item)

            for clp in all_ventas:
                if not clp.esta_pagada:
                    deudas += clp.restante

            btn = Botones()
            color_ventas = 'warning' if deudas != 0 else 'success'
            btn.href('#/encargado/ventas/listar_cliente/'+str(item.id),color_ventas,'money-bill','Ver ventas')
            if not self.usuario.is_superuser:
                btn.href('#/encargado/ventas?cliente='+str(item.ci_rif),'info','check-double','Vender')

            ws = '<a href="https://api.whatsapp.com/send?phone=58'+ item.telefono +\
                'title="Enviar whatsapp" target="_blank">'+ item.telefono +'</a>'
                
            json_data.append([
                str(item.encargado),
                str(item.nombre),
                str(item.apellido), 
                str(item.telefono),
                str(item.ci_rif),
                str(deudas) if deudas != 0 else 'Sin deudas',
                str(btn),
            ])
        return json_data  

    def get_queryset(self):
        try:
            encargado = Encargado.objects.get(pk=self.pk)
            query = Cliente.objects.filter(condicion=True,encargado=encargado)
        except Encargado.DoesNotExist:
            raise e
        except AttributeError as e:
            query = Cliente.objects.filter(condicion=True)
        try:
            return query.filter(created__range=[self.start,self.end])
        except AttributeError as e:
            pass

        return query


    def filter_queryset(self, qs):
        return qs    

    def get_initial_queryset(self):
        return self.model.objects.filter(condicion=True)


class ListarClientesEncargado(ListarClientes):
    pass


class ListarPagos(DataTable):
    model = PagoVenta
    columns = ['monto','metodo','banco','referencia','botones']
    order_columns = columns
    max_display_length = 1000

    def prepare_results(self, qs):
        qs = self.get_queryset()
        json_data = []
        for item in qs:
            btn = Botones()
            btn.click("eliminar_pago_anterior('"+str(item.id)+"')" , 'danger', 'recycle')
            json_data.append([
                str(item.monto),
                str(item.metodo),
                str(item.banco), 
                str(item.referencia),
                str(btn),
            ])
        return json_data  

    def get_queryset(self):
        try:
            ven = Venta.objects.get(pk=self.pk)
            query = PagoVenta.objects.filter(condicion=True,venta=ven)
        except Venta.DoesNotExist:
            raise e
        except AttributeError as e:
            query = PagoVenta.objects.filter(condicion=True)
        try:
            return query.filter(created__range=[self.start,self.end])
        except AttributeError as e:
            pass
        return query

    def filter_queryset(self, qs):
        return qs    

    def get_initial_queryset(self):
        return self.model.objects.filter(condicion=True)


class ListarPagosVenta(ListarPagos):
    pass




class ListarVentasReporteMetodo(DataTable):
    model = PagoVenta
    columns = ['monto','metodo','banco','referencia']
    order_columns = columns
    max_display_length = 1000

    def prepare_results(self, qs):
        qs = self.get_queryset()

        modo = self.request.GET.get('modo')

        json_data = []
        for item in qs:
            ret = []
            ret.append(str(item.venta.cliente))
            ret.append(str(item.venta.cliente.ci_rif))
            if modo == 'transferencia':
                ret.append(item.referencia + ' (%s)' % item.banco)
            if modo == 'punto':
                ret.append(str(item.banco))
            ret.append(str(item.monto))
            json_data.append(ret)

        return json_data  

    def get_queryset(self):
        from django.utils.datastructures import MultiValueDictKeyError
        try:
            enc = Encargado.objects.get(pk=self.pk)
            query = Venta.objects.filter(
                    condicion=True, encargado=enc) 
        except Encargado.DoesNotExist:
            raise e

        except AttributeError as e:
            query = Venta.objects.filter(
                    condicion=True) 
        params = self.request.GET
        try:
            ini_f = params['start'].split('-')
            fin_f = params['end'].split('-')
            if ini_f[0] == fin_f[0] and ini_f[1] == fin_f[1] and ini_f[2] == fin_f[2]:
                query = query.filter(created__day=ini_f[0],
                                     created__month=ini_f[1],
                                     created__year=ini_f[2])
            else:
                raise MultiValueDictKeyError('error')
        except (MultiValueDictKeyError, IndexError):
            query = query.filter(created__range=[self.start,self.end])
        except AttributeError as e:
            pass

        modo = self.request.GET.get('modo')
        if modo == 'transferencia':
            modo = 3
        if modo == 'efectivo':
            modo = 4
        if modo == 'punto':
            modo = 5

        args = {
            'venta__pk__in':[it.pk for it in query],
            'condicion':True,
            'metodo__pk__exact':modo
        }

        pv = PagoVenta.objects.filter(**args)
        return pv

    def filter_queryset(self, qs):
        return qs    

    def get_initial_queryset(self):
        return self.model.objects.filter(condicion=True)


class ListarVentasReporteMetodoEncargado(ListarVentasReporteMetodo):
    pass
