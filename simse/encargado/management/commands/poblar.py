from django.core.management.base import BaseCommand
from encargado.models import Encargado 
from empleado.models import DiaTrabajoEmpleado
from gastos.api.serializers import TransaccionSerializer

class Command(BaseCommand):
    help = ''

    def handle(self, *args, **options):
        

        dias = DiaTrabajoEmpleado.objects.all()

        for dia in dias:

            enc = dia.empleado.encargado
            ganancia = enc.ganancia.last()

            if ganancia.tipo == 1:
                pet = {
                    'cuenta_destino':enc.persona.cuenta_set.last().id,
                    'monto_transaccion':dia.get_ganancia_encargado(),
                }

                serializer = TransaccionSerializer(data=pet) 
                if serializer.is_valid(raise_exception=True):
                    data = serializer.save()
                    print (data)
                    #pass

