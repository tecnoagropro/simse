from decimal import Decimal
from django.core.management.base import BaseCommand
from encargado.models import VentaClienteProducto, Venta
from inventario.models import Costo, CostoProducto

class Command(BaseCommand):
    help = ''

    def handle(self, *args, **options):
        
        all = VentaClienteProducto.objects.all()
        for i in all:
            cp = CostoProducto.objects.filter(
                 condicion=True, producto=i.stock.stock.producto).last()
            monto = cp.monto.monto * Decimal(i.cantidad)
            i.monto, creado = Costo.objects.get_or_create(monto=monto)
            i.save_super()

        venta = Venta.objects.get(id=3)
        venta.total, creado = Costo.objects.get_or_create(monto=121250)
        venta.save()