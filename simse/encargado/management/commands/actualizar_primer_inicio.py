from decimal import Decimal
from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model
from django.contrib.auth.hashers import (
    check_password, is_password_usable, make_password,
)
from encargado.models import *
from inventario.models import *
from accounts.models import Persona
from django.core.management import call_command


class Command(BaseCommand):
    help = """

    Primero Crea los usuarios y sus personas
    Luego le crea los encargado por cada uno de ellos

    Segundo Carga todos los productos

    """


    def set_user(self, **kwargs):

        username = kwargs.get('username', None)
        User = get_user_model()
        try:
            User.objects.get(username=username)
        except User.DoesNotExist as e:

            primer_nombre = kwargs.get('primer_nombre', None)
            primer_apellido = kwargs.get('primer_apellido', None)
            email = kwargs.get('email', '%s@gmail.com' % username)
            password = kwargs.get('password', None)
            is_superuser = kwargs.get('super', False)
            


            u = User()
            u.username = username
            u.email = email
            u.is_staff = is_superuser
            u.is_superuser = is_superuser
            u.password = make_password(password)
            u.save()

            cedu = kwargs.get('cedula', None)
            celu = kwargs.get('celular', None)

            p = Persona()
            p.user = u
            p.primer_nombre = primer_nombre.title()
            p.primer_apellido = primer_apellido.title()
            p.tipo_documento = 'cedula' if cedu else None
            p.numero_documento = cedu
            if celu:
                p.mobile = celu
            p.save()
            return p


    def productos(self):
        call_command('loaddata','productos.json')


    def costos(self):
        call_command('loaddata','costos.json')


    def banco_metodo(self):
        call_command('loaddata','banco_metodo.json')


    def usuarios(self, **options):

        self.set_user(**{
            'username':'sysadmin',
            'password':'sysadmin',
            'primer_nombre':'administrador',
            'primer_apellido':'sistema',
            'email':'tecnoagropro@gmail.com',
            'super':True
            })

        self.set_user(**{
            'username':'rosmerl',
            'password':'dracula23',
            'primer_nombre':'Rosmerl',
            'primer_apellido':'Barrera',
            'email':'rosmerlbarrera@mail.com',
            'super':True
            })


        p = self.set_user(**{
            'username':'liliana',
            'password':'clave123',
            'primer_nombre':'Liliana',
            'primer_apellido':'mendez',
            'email':'lilianajosue2013@gmail',
            'cedula':'18578047'
            })     

        Encargado.objects.create(persona=p,
            nombre='Liliana',
            locacion='Oficina, Tovar')

        p = self.set_user(**{
            'username':'rosmay',
            'password':'clave123',
            'primer_nombre':'Ezaikel',
            'primer_apellido':'Barrera',
            'cedula':'16432072',
            'celular':'04147027684'
            })     

        Encargado.objects.create(persona=p,
            nombre='Rosmay',
            locacion='Zea')
  

        p = self.set_user(**{
            'username':'marly',
            'password':'clave123',
            'primer_nombre':'Marly',
            'primer_apellido':'Rujano',
            'cedula':'23493297',
            'celular':'04140810068',
            'email':'marlyrujanom@gmail.con',
            })     

        Encargado.objects.create(persona=p,
            nombre='Marly',
            locacion='Los limones, Tovar')


        p = self.set_user(**{
            'username':'veronica',
            'password':'clave123',
            'primer_nombre':'Veronica',
            'primer_apellido':'Morales',
            'cedula':'27581743',
            'celular':'04140810068',
            'email':'marlyrujanom@gmail.con',
            })     

        Encargado.objects.create(persona=p,
            nombre='Veronica',
            locacion='Bailadores')


        p = self.set_user(**{
            'username':'daniela',
            'password':'clave123',
            'primer_nombre':'Daniela',
            'primer_apellido':'Sandoval',
            'cedula':'24583281',
            'celular':'04120755863',
            'email':'danielasandoval796@gmail.com',
            })     

        Encargado.objects.create(persona=p,
            nombre='Daniela',
            locacion='Bailadores')

 
        if options.get('verbosity', 0) >= 1:
          self.stdout.write("Usuarios creados")


    def handle(self, *args, **options):

        try:
            self.usuarios(**options)
        except Exception as e:
            pass
        
        try:
            self.productos()
        except Exception as e:
            pass

        try:
            self.costos()
        except Exception as e:
            print ("error costo ", e)

        try:
            self.banco_metodo()
        except Exception as e:
            pass                                
        
        
        