from django.core.management.base import BaseCommand

from gastos.models import Cuenta 
from accounts.models import Persona

class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def handle(self, *args, **options):
        

        personas_id = []

        for persona in Persona.objects.all():

            if persona.cuenta_set.all().count() > 1:

                cuenta = persona.cuenta_set.last()
                cuenta.delete()


                

