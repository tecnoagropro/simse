from django.db import models
from django.contrib.postgres.fields import ArrayField
from django.conf import settings
from django.dispatch import receiver
from django.db.models.signals import post_save
from django.utils.translation import ugettext_lazy as _

from common.models import (UserOneBaseModel, BaseModel, UserRelationBaseModel,
    Permiso, BaseManager)
from accounts.models import Persona
from inventario.models import Costo


class Encargado(BaseModel):

    permisos = models.ManyToManyField(Permiso,blank=True)
    nombre = models.CharField(max_length=20)
    locacion = models.CharField(max_length=100)
    persona = models.OneToOneField(Persona,related_name="encargado_set")

    def __str__(self):
        return self.nombre

    def ganancia_actual(self):
        return GananciaEncargado.objects.filter(encargado=self).last()

    @property
    def cuenta(self):
        return self.persona.cuenta_set.first()

    class Meta:
        ordering = ("nombre",)
        verbose_name = _('Encargado')
        verbose_name_plural = _('Encargados')  


class GananciaEncargado(BaseModel):

    ganancia = models.PositiveIntegerField(default=0)
    encargado = models.ForeignKey(Encargado, related_name="ganancia")
    tipo = models.PositiveIntegerField(default=1)

    def __str__(self):
        if self.tipo == 1:
            tipo = 'Monto'
        else: #2 
            tipo = 'Porcentaje'
        return str(self.ganancia) + ' ' + tipo

    class Meta:
        ordering = ("ganancia",)
        verbose_name = _('Ganancia por Encargado')
        verbose_name_plural = _('Ganancias para Encargados')   


class DiaTrabajo(UserRelationBaseModel):

    fecha_registro = models.DateField()

    def __str__(self):
        return str(self.fecha_registro)

    class Meta:
        ordering = ("fecha_registro",)
        verbose_name = _('Dia de trabajo')
        verbose_name_plural = _('Dias de trabajo')     


class Cuenta(BaseModel):

    """ 
    User es quien creo la cuenta (si es por api)
    pero pertenese es a una persona
    """
    monto = models.ForeignKey("inventario.costo",related_name="cuenta_monto_set")
    persona = models.ForeignKey(Persona, related_name="cuenta_set")
    user = models.OneToOneField(settings.AUTH_USER_MODEL,
                                on_delete=models.CASCADE,
                                null=True, blank=True)

    class Meta:
        ordering = ("persona",)
        verbose_name = _('Cuenta')
        verbose_name_plural = _('Cuentas')
        get_latest_by = 'id'

    def __str__(self):
        return self.persona.primer_nombre


class TransaccionManager(BaseManager):

    def cantidad_abonos(self, cuenta):
        qs =  self.filter(destino=cuenta,
                          tipo='CR',
                          generado=False,
                          condicion=True).count()
        return qs

    def cantidad_retiros(self,cuenta):
        qs =  self.filter(destino=cuenta,
                          tipo='DB',
                          generado=False,
                          condicion=True).count()
        return qs


class TransaccionHistory(BaseModel):

    origen = models.ForeignKey(Cuenta, related_name="transaccion_origen_set",
                               blank=True, null=True)
    destino = models.ForeignKey(Cuenta, related_name="transaccion_destino_set")
    descripcion = models.CharField(max_length=100, blank=True, null=True)
    monto = models.ForeignKey("inventario.costo")
    generado = models.BooleanField(default=False)
    tipo =  models.CharField(max_length=2, default='CR')
    objects = TransaccionManager()

    class Meta:
        ordering = ("-created",)
        verbose_name = _('Transaccion')
        verbose_name_plural = _('Transacciones')

    def __str__(self):
        return str(self.monto)


class Cliente(BaseModel):
    nombre = models.CharField(max_length=200)
    apellido = models.CharField(max_length=200)
    ci_rif = models.CharField(max_length=30, unique=True)
    encargado = models.ForeignKey(Encargado)
    telefono = models.CharField(_("Telefono"), max_length=20)
    
    def __str__(self):
        return self.nombre + " " + self.apellido

    class Meta:
        ordering = ("-ci_rif",)
        verbose_name = _('Cliente')
        verbose_name_plural = _('Clientes')


class Venta(BaseModel):
    """
    Venta del sistema,
    total es el valor de la compra
    """
    encargado = models.ForeignKey(Encargado, related_name="venta")
    total = models.ForeignKey("inventario.costo", related_name='total_venta_set',
                              null=True, blank=True)
    pagado = models.ForeignKey("inventario.costo", related_name='pagado_venta_set',
                               null=True, blank=True)
    cliente = models.ForeignKey(Cliente)
    comentario = models.TextField(blank=True, null=True)
    terminada = models.BooleanField(default=False)

    @property
    def esta_pagada(self):
        if self.pagado:
            return self.pagado.monto >= self.total.monto
        return False

    @property
    def restante(self):
        if self.pagado:
            return self.total.monto - self.pagado.monto
        if self.total:
            return self.total.monto
        return 0
    
    def delete(self,*args,**kwargs):
        self.pagos.all().delete()
        super(Venta, self).delete(*args, **kwargs)

    def __str__(self):
        try:
            return str(self.total.monto)
        except Exception as e:
            pass
        return str(self.cliente)

    def reversar(self):
        self.pagos.all().delete()
        qs = VentaClienteProducto.objects.filter(venta=self)
        for venta in qs:
            venta.reversar()
        self.delete()
        return True

    class Meta:
        ordering = ("-id",)


class MetodoPago(BaseModel):
    
    metodo = models.CharField(max_length=200)

    def __str__(self):
        return str(self.metodo)

    class Meta:
        ordering = ("-id",)


class Banco(BaseModel):
    nombre = models.CharField(max_length=200)
    orden = models.PositiveIntegerField(default=10)

    def __str__(self):
        return str(self.nombre)

    class Meta:
        ordering = ("orden",)


class PagoVenta(BaseModel):
    
    metodo = models.ForeignKey(MetodoPago)
    monto = models.ForeignKey("inventario.costo")
    venta = models.ForeignKey(Venta, related_name='pagos')
    referencia = models.CharField(max_length=20, null=True, blank=True)
    banco = models.ForeignKey(Banco, null=True, blank=True)

    def __str__(self):
        return str(self.monto)

    def delete(self,*args,**kwargs):
        if self.venta.pagado:
            nuevo_monto = self.venta.pagado.monto - self.monto.monto
            try:
                self.venta.pagado, creado = Costo.objects.get_or_create(monto=nuevo_monto)
            except Costo.MultipleObjectsReturned as e:
                self.venta.pagado = Costo.objects.filter(monto=nuevo_monto).first()
            self.venta.save()

        super(PagoVenta, self).delete(*args, **kwargs)

    class Meta:
        ordering = ("-id",)


class VentaClienteProducto(BaseModel):
    """
    Existe una venta previa y aqui se guardan
    todo lo comprado en ese venta
    """
    venta = models.ForeignKey(Venta, related_name="productos_ventas")
    stock = models.ForeignKey("inventario.asignacionstock")
    cantidad = models.DecimalField(max_digits=16, decimal_places=2, default=0)
    monto = models.ForeignKey("inventario.costo")
    
    def __str__(self):
        return str(self.venta)

    def save_super(self, *args, **kwargs):
        super(VentaClienteProducto, self).save(*args, **kwargs)

    def save(self, *args, **kwargs):
        self.stock.descontar(self.cantidad)
        if self.venta.total:
            nuevo_total = self.monto.monto + self.venta.total.monto
        else:
            nuevo_total = self.monto.monto
        try:
            total = Costo.adquirir(nuevo_total)
        except Costo.MultipleObjectsReturned as e:
            total = Costo.objects.filter(monto=nuevo_total).first()        
        self.venta.total = total
        self.venta.save()
        super(VentaClienteProducto, self).save(*args, **kwargs)

    def reversar(self):
        self.stock.reversar(self.cantidad)
        self.venta.total = None
        self.venta.save()
        self.delete()

    class Meta:
        ordering = ("-venta",)
        get_latest_by = 'id'        


class CierreEncargado(BaseModel):
    
    encargado = models.ForeignKey(Encargado)
    cantidad_productos_vendido = models.PositiveIntegerField(default=0)
    fecha_inicio = models.DateField()
    fecha_fin = models.DateField()
    total = models.ForeignKey("inventario.costo", related_name='cierre_total_set',
                              null=True, blank=True)
    pagado = models.ForeignKey("inventario.costo", related_name='cierre_pagado_set',
                               null=True, blank=True)
    pago_encargados = models.ForeignKey("inventario.costo",
                                        related_name='cierre_pagados_encargados_set',
                                        null=True, blank=True)
    cantidad_productos_por_vender = models.ForeignKey("inventario.costo",
                                    related_name='cantidad_productos_por_vender_set',
                                    null=True, blank=True)
    cantidad_productos_inventario = models.ForeignKey("inventario.costo",
                                    related_name='cantidad_productos_inventario',
                                    null=True, blank=True)

    por_transferencia = models.ForeignKey("inventario.costo", related_name='por_transferencia_set',
                               null=True, blank=True)
    por_efectivo = models.ForeignKey("inventario.costo", related_name='por_efectivo_set',
                               null=True, blank=True)
    por_punto_venta = models.ForeignKey("inventario.costo", related_name='por_punto_venta_set',
                               null=True, blank=True)
    por_cheques = models.ForeignKey("inventario.costo", related_name='por_cheques_set',
                               null=True, blank=True)
    por_otros = models.ForeignKey("inventario.costo", related_name='por_otros_set',
                               null=True, blank=True)

    def __str__(self):
        return str(self.encargado)

    class Meta:
        ordering = ("-id",)
        get_latest_by = 'fecha_fin'


class DetalleCierre(BaseModel):
    cierre = models.ForeignKey(CierreEncargado)
    cliente = models.ForeignKey(Cliente)
    venta = models.ForeignKey("inventario.costo",
                               related_name='cierre_detalle_venta_set')
    pagado = models.ForeignKey("inventario.costo",
                                related_name='cierre_detalle_pagado_set')
    pendiente = models.ForeignKey("inventario.costo",
                                  related_name='cierre_detalle_pendiente_set')

    def __str__(self):
        return str(self.cliente)

    class Meta:
        ordering = ("-id",)
        get_latest_by = 'id'


class DetalleCierreProducto(BaseModel):
    cierre = models.ForeignKey(CierreEncargado)
    producto = models.ForeignKey('inventario.Producto')
    cantidad = models.DecimalField(max_digits=30, decimal_places=2)
    venta = models.ForeignKey("inventario.costo",
                               related_name='cierre_detalle_producto_venta_set')

    def __str__(self):
        return str(self.producto)

    class Meta:
        ordering = ("-id",)
        get_latest_by = 'id'


class DetalleCierreCliente(BaseModel):
    cierre = models.ForeignKey(DetalleCierreProducto)
    cliente = models.ForeignKey(Cliente)
    venta = models.ForeignKey("inventario.costo",
                               related_name='cierre_detalle_cliente_venta_set')
    pagado = models.ForeignKey("inventario.costo",
                                related_name='cierre_detalle_cliente_pagado_set')
    pendiente = models.ForeignKey("inventario.costo",
                                  related_name='cierre_detalle_cliente_pendiente_set')

    def __str__(self):
        return str(self.cliente)

    class Meta:
        ordering = ("-id",)
        get_latest_by = 'id'

@receiver(post_save, sender=Persona)
def save_persona_cuenta(sender, instance, **kwargs):
    try:
        Cuenta.objects.get(persona=instance)
    except:
        cuenta = Cuenta()
        cuenta.persona = instance
        try:
            cuenta.monto, creado = Costo.objects.get_or_create(monto=0)
        except Exception as e:
            c = Costo.objects.create(monto=0)
            cuenta.monto = c
        cuenta.save()