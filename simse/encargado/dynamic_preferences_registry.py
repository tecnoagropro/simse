from django.conf import settings
from dynamic_preferences.registries import global_preferences_registry
from dynamic_preferences.types import StringPreference
from common.mensajes import M

@global_preferences_registry.register
class IconoModificar(StringPreference):
    name = 'ICONO_MODIFICAR'
    default = 'edit'
    verbose_name = M.icono_modificar



@global_preferences_registry.register
class UrlRedireccionarAdmin(StringPreference):
    name = 'URL_REDIRECCIONAR_ADMIN'
    default = '/encargado/listar'
    verbose_name = M.url_d_admin 


@global_preferences_registry.register
class UrlRedireccionarEncargado(StringPreference):
    name = 'URL_REDIRECCIONAR_ENCARGADO'
    default = '/inventario/listar'
    verbose_name = M.url_d_encargado


 

