from django.conf.urls import include, url
from django.views.decorators.csrf import csrf_exempt
from common.decorators import Token
from .views import *
from .ajax import *

urlpatterns = [
    url(r'^encargados/$', Token(encargados), name="encargados"),
    url(r'^utilidades/$', Token(utilidades), name="utilidades"),
	url(r'^encargado/registrar$', Token(registrar), name="registrar_encargado"),
	url(r'^encargado/listar$', Token(listar), name="listar_encargado"),
	url(r'^encargado/modificar/(?P<pk>[0-9\.]+)$', Token(modificar), name="modificar_encargado"),
	url(r'^encargado/cuentas/(?P<pk>[0-9\.]+)$', Token(cuentas_encargado), name="encargados"),
    url(r'^encargado/ventas$', Token(ventas), name="ventas"),
    url(r'^encargado/ventas/reporte$', Token(encargado_ventas_reporte), name="encargado_ventas_reporte"),
    url(r'^encargado/ventas/reporte_metodo$', Token(encargado_metodo_reporte), name="encargado_metodo_reporte"),
    url(r'^encargado/ventas/listar$', Token(listar_ventas), name="listar_ventas"),
    url(r'^encargado/ventas/listar/(?P<pk>[0-9\.]+)$', Token(listar_ventas_encargado), name="listar_ventas_encargado"),
    url(r'^encargado/ventas/listar_cliente/(?P<pk>[0-9\.]+)$', Token(listar_ventas_cliente), name="listar_ventas_cliente"),
    url(r'^encargado/ventas/listar_cliente/iframe/(?P<pk>[0-9\.]+)$',
        Token(listar_ventas_cliente_iframe),
        name="listar_ventas_cliente_iframe"),
    url(r'^encargado/ventas/pagos$', Token(pagos), name="pagos"),
    url(r'^encargado/ventas/pagos/(?P<pk>[0-9\.]+)$', Token(pagos_venta), name="pagos"),
    url(r'^encargado/clientes/listar$', Token(listar_clientes), name="listar_clientes"),
    url(r'^encargado/clientes/listar/(?P<pk>[0-9\.]+)$', Token(listar_clientes_encargado), name="listar_clientes_encargado"),


    url(r'^inventario/encargado/listar$', Token(listar_inventario), name="listar_inventario_encargado"),


	url(r'^ajax/listar_encargado$', csrf_exempt(ListarEncargado.as_view()) , name="listar_encargado_ajax"),
	url(r'^ajax/listar_inventario_encargado$', csrf_exempt(ListarStockEncargado.as_view()), name="listar_inventario_encargado_ajax"),
    url(r'^ajax/listar_transacciones$', csrf_exempt(ListarTransacciones.as_view()), name="listar_transacciones_ajax"),
    url(r'^ajax/listar_transacciones_encargado/(?P<pk>[0-9\.]+)/$', csrf_exempt(ListarTransaccionesEncargado.as_view()), name="listar_transacciones_encargado_ajax"),
    
    url(r'^ajax/listar_ventas_reporte$', csrf_exempt(ListarVentasReporte.as_view()),
        name="listar_ventas_reporte_ajax"),
    url(r'^ajax/listar_ventas_reporte_encargado/(?P<pk>[0-9\.]+)/$', csrf_exempt(ListarVentasReporteEncargado.as_view()),
        name="listar_ventas_reporte_encargado_ajax"),
    
    url(r'^ajax/listar_ventas$', csrf_exempt(ListarVentas.as_view()), name="listar_ventas_ajax"),
    url(r'^ajax/listar_ventas_encargado/(?P<pk>[0-9\.]+)/$', csrf_exempt(ListarVentasEncargado.as_view()),
    	name="listar_ventas_encargado_ajax"),

    url(r'^ajax/listar_clientes$', csrf_exempt(ListarClientes.as_view()), name="listar_clientes_ajax"),
    url(r'^ajax/listar_clientes_encargado/(?P<pk>[0-9\.]+)/$', csrf_exempt(ListarClientesEncargado.as_view()),
        name="listar_clientes_encargado_ajax"),

    url(r'^ajax/listar_ventas_cliente/(?P<pk>[0-9\.]+)/$', csrf_exempt(ListarVentasCliente.as_view()),
        name="listar_ventas_cliente_ajax"),

    url(r'^ajax/listar_pagos_venta$', csrf_exempt(ListarPagos.as_view()), name="listar_pagos_ajax"),
    url(r'^ajax/listar_pagos_venta/(?P<pk>[0-9\.]+)/$', csrf_exempt(ListarPagosVenta.as_view()),
        name="listar_pagos_venta_ajax"),

    url(r'^ajax/listar_ventas_reporte_metodo/$', csrf_exempt(ListarVentasReporteMetodo.as_view()),
        name="listar_ventas_reporte_metodo_ajax"),
    url(r'^ajax/listar_ventas_reporte_encargado_metodo/(?P<pk>[0-9\.]+)/$',
        csrf_exempt(ListarVentasReporteMetodoEncargado.as_view()),
        name="listar_ventas_reporte_metodo_encargado_ajax"),
    

]
