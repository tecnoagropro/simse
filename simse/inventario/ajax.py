from django.http import JsonResponse
from .models import Stock, Producto, AsignacionStock, TrasladoEncargado
from common.mixins import DataTable, Botones
from django.db.models import Sum, Q
from accounts.models import Persona
from encargado.models import Encargado
from inventario.models import CostoProducto

### Datatables
class ListarStock(DataTable):
    model = Stock
    columns = ['producto.nombre','cantidad_recibida','cantidad_actual','monto','botones']
    order_columns = ['producto.nombre', 'cantidad_recibida','cantidad_actual']
    max_display_length = 1000

    def botones(self, row):
        cadena = '&nbsp'
        # hay que cambiar estos permisos
        if self.usuario.has_perm('general.change_cliente'):
            cadena=cadena +'<a href="#/inventario/detalle/producto/'+str(row.id)+'/" type="button" class="btn btn-success" ><i class="fa fa-pencil-alt"></i></a>&nbsp'
        return cadena

    def prepare_results(self, qs):
        search = self.request.POST.get(u'search[value]', None)
        try:
            query = Producto.objects.filter(condicion=True, pk=self.pk)
        except AttributeError as e:
            if search:
                query = Producto.objects.filter(condicion=True, nombre__icontains = search)
            else:
                query = Producto.objects.filter(condicion=True)

        json_data = []

        for item in query:
            qs = Stock.objects.filter(condicion=True, producto=item)
            if qs:
                total_cantidad = qs.aggregate(sum=Sum('cantidad_recibida'))['sum']
                total_cantidad_actual = qs.aggregate(sum=Sum('cantidad_actual'))['sum']
                
                try:
                    enc = self.usuario.persona_set.encargado_set
                    try:
                        total_monto = total_cantidad * CostoProducto.objects.filter(condicion=True,
                        producto_id=producto['producto_id']).last()
                    except CostoProducto.DoesNotExiste:
                        total_monto = 'Debe agregar costo'
                except:
                    total_monto = qs.aggregate(sum=Sum('monto__monto'))['sum']

                list_item = []
                list_item.insert(0, item.nombre_unidad)

                if 'cantidad_recibida' in self.columns:
                    list_item.insert(1, total_cantidad)
                if 'cantidad_actual' in self.columns:
                    list_item.insert(2, total_cantidad_actual)
                if 'monto' in self.columns:
                    list_item.insert(3, str(total_monto).replace('.',',') + ' Bs')                 
                if 'botones' in self.columns:
                    list_item.insert(4, self.botones(item))

                json_data.append(list_item)

        return json_data  



class ListarStockProducto(ListarStock):

    columns = ['producto.nombre','cantidad_recibida','cantidad_actual','botones']

    def botones(self, row):
        from datetime import datetime
        cadena = ''
        if row.condicion:
            hoy = datetime.now()
            con1 = AsignacionStock.objects.filter(stock=row).count() == 0
            con2 = (row.created.day == hoy.day) and \
                   (row.created.month == hoy.month) and (row.created.year == hoy.year)
            if con1 and con2:
                cadena = cadena + '<a type="button" class="btn btn-danger" onclick="eliminar_dato(' + str(row.id) +')"><i class="fa fa-recycle"></i></a>'

        return cadena

    def prepare_results(self, qs):
        search = self.request.POST.get(u'search[value]', None)
        p = Producto.objects.get(condicion=True, pk=self.pk)
        query = Stock.objects.filter(condicion=True, producto=p)

        json_data = []

        for item in query:

            json_data.append([
                p.nombre_unidad,
                item.cantidad_recibida,
                str(item.created.strftime("%d-%m-%Y")),
                str(item.monto).replace('.',',') + ' Bs',
                self.botones(item)
            ])

        return json_data


class ListarStockEncargado(DataTable):
    model = AsignacionStock
    columns = ['stock.producto.nombre','cantidad_recibida','cantidad_actual','stock.monto','botones']
    order_columns = ['stock.producto.nombre', 'cantidad_recibida','cantidad_actual','stock.monto']
    max_display_length = 1000

    def get_nombre_display(self):
        return 'Nomre producto'

    def prepare_results(self, qs):
        json_data = []

        func_btn = getattr(self,'botones',None) 
        botones = ' '
        if func_btn and callable(func_btn):
            botones = self.botones(qs)

        for item in qs:
            json_data.append([
                str(item.stock.producto.nombre_unidad),
                str(item.cantidad_recibida),
                str(item.cantidad_actual),
                str(item.stock.monto).replace('.',',') + ' Bs', 
                botones
            ])
        return json_data

    def filter_queryset(self, qs):
        search = self.request.POST.get(u'search[value]', None)
        if search:
            qs = qs.filter(stock__producto__nombre__icontains = search)
          
        return qs

    def get_initial_queryset(self):
        enc = self.usuario.persona_set.encargado_set
        return self.model.objects.filter(condicion=True,
               cantidad_actual__gt=0, aceptado=True, encargado=enc)


class ListarStockEncargadoAceptarNuevo(DataTable):
    model = AsignacionStock
    columns = ['stock.producto.nombre','cantidad_recibida','created','fecha_aceptado','botones']
    order_columns = ['stock.producto.nombre', 'cantidad_recibida','created','fecha_aceptado']
    max_display_length = 1000

    def get_initial_queryset(self):
        try:
            enc = self.usuario.persona_set.encargado_set
            todas = AsignacionStock.objects.filter(condicion=True,
                   cantidad_actual__gte=0, encargado=enc)
            # pendientes = AsignacionStock.objects.filter(condicion=True,
            #        cantidad_actual__gte=0, encargado=enc, aceptado=False)
            # print (pendientes)
            # nuevo_qs = pendientes.union(todas)
            return todas

        except Exception as e:
            print ("ListarStockEncargadoAceptarNuevo", e)
            return AsignacionStock.objects.filter(condicion=True,
                   cantidad_actual__gte=0)

    def render_column(self, row, column):
        if column == 'botones':
            if row.aceptado:
                botones = '<span title="Ya aceptado" class="btn btn-info"><i class="fa fa-check"></i></span>&nbsp;'
                if row.cantidad_actual > 0:
                    botones += '<a type="button" class="btn btn-success" onclick="traslado_a_sede_action('+str(row.stock.producto.id)+',\''+str(row.cantidad_actual)+'\')"><i class="fa fa-minus"></i></a>&nbsp;'
            else:
                botones = '<a type="button" class="btn btn-success" title="Aceptar stock" onclick="aceptar_stock('+str(row.id)+')"><i class="fa fa-pencil-alt"></i></a>&nbsp;'
            
            if row.cantidad_actual == row.cantidad_recibida:
                try:
                    tras = TrasladoEncargado.objects.get(traslado=row)
                    botones += '<a type="button" class="btn btn-danger"'
                    botones += 'onclick="rechazar_stock_traslado('+str(row.id)+')"><i class="fa fa-recycle"></i></a>'
                except Exception as e:
                    botones += '<a type="button" class="btn btn-danger"'
                    botones += 'onclick="rechazar_stock('+str(row.id)+')"><i class="fa fa-recycle"></i></a>'                    
                

            return botones
        elif column == 'created':
            return row.created.strftime("%d-%m-%Y")
        elif column == 'fecha_aceptado':
            if row.fecha_aceptado:
                return row.fecha_aceptado.strftime("%d-%m-%Y")
            return ''
        else:
            return super(ListarStockEncargadoAceptarNuevo, self).render_column(row, column)


class ListarStockEncargadoAceptar(ListarStockEncargado):
    columns = ['stock.producto.nombre','cantidad_recibida','created','fecha_aceptado','botones']
    order_columns = ['stock.producto.nombre', 'cantidad_recibida','created','fecha_aceptado']

    def prepare_results(self, qs):
        json_data = []
        # print (qs.values('stock__producto__id') \
        #   .annotate(c_a=Sum('cantidad_actual'),c_r=Sum('cantidad_recibida')) \
        #   .order_by('-c_a'))        
        #print(qs)
        productos = []

        for stock in qs:

            p_id = stock.stock.producto.id          
            if p_id not in productos:
                productos.append(p_id)               
            else:
                pass

            o_p = {
            'id':p_id,
            'aceptado':stock.aceptado,
            'c_a':stock.cantidad_actual,
            'c_r':stock.cantidad_recibida,
            'nombre':stock.stock.producto.nombre,
            'fecha_aceptado':stock.fecha_aceptado.strftime("%d-%m-%Y") if stock.fecha_aceptado else '',
            'creado':stock.created.strftime("%d-%m-%Y")
            }

            grup = getattr(self, 'g%s' % str(p_id) , None)
            if not grup:
                setattr(self,'g%s' % str(p_id), [])
                grup = getattr(self, 'g%s' % str(p_id) , [])
            grup.append(o_p)


        for p_id in productos:
            g = getattr(self, "g%s" % str(p_id))
            
            cantidad_recibida = 0
            cantidad_actual = 0
            aceptado = True

            for i in g:
                cantidad_recibida += i['c_r']
                cantidad_actual += i['c_a']

                if not i['aceptado']:
                    aceptado = False

            if aceptado:
                botones = '<span class="btn btn-info"><i class="fa fa-check"></i></span>&nbsp;'
                botones += '<a type="button" class="btn btn-success" onclick="traslado_a_sede_action('+str(g[0]['id'])+',\''+str(cantidad_actual)+'\')"><i class="fa fa-minus"></i></a>&nbsp;'
                botones += '<a type="button" class="btn btn-danger" onclick="rechazar_stock('+str(g[0]['id'])+')"><i class="fa fa-recycle"></i></a>'
            else:
                botones = '<a type="button" class="btn btn-success" onclick="aceptar_stock('+str(g[0]['id'])+')"><i class="fa fa-pencil-alt"></i></a>&nbsp;'
                botones += '<a type="button" class="btn btn-danger" onclick="rechazar_stock('+str(g[0]['id'])+')"><i class="fa fa-recycle"></i></a>'
          
            json_data.append([
                str(g[0]['nombre']),
                str(cantidad_recibida),
                str(g[0]['creado']),
                str(g[0]['fecha_aceptado']), 
                botones
            ])

        return json_data

    def get_initial_queryset(self):
        try:
            enc = self.usuario.persona_set.encargado_set
            return self.model.objects.filter(condicion=True,
                   cantidad_actual__gte=0,encargado=enc).order_by('aceptado')
        except:
            return self.model.objects.filter(condicion=True,
                   cantidad_actual__gte=0).order_by('aceptado')


class ListarTrasladosSucursales(ListarStockEncargado):
    model = TrasladoEncargado
    columns = ['traslado.stock.producto.nombre','traslado.cantidad_recibida',
                'traslado.created','traslado.encargado']
    order_columns = ['traslado.stock.producto.nombre', 'traslado.cantidad_recibida',
                'traslado.created','traslado.encargado']

    def prepare_results(self, qs):
        json_data = []
        productos = []

        for stock in qs:

            p_id = str(stock.traslado.stock.producto.id)       
            if p_id not in productos:
                productos.append(p_id)               
            
            o_p = {
            'id':stock.origen.stock.producto.id,
            'desde':stock.origen.encargado,
            'nombre':stock.origen.stock.producto.nombre,
            'creado':stock.created.strftime("%d-%m-%Y"),
            'c_a':stock.traslado.cantidad_actual,
            'c_r':stock.traslado.cantidad_recibida,
            }

            grup = getattr(self, 'g%s' % str(p_id) , None)
            if not grup:
                setattr(self,'g%s' % str(p_id), [])
                grup = getattr(self, 'g%s' % str(p_id) , [])
            grup.append(o_p)


        for p_id in productos:
            g = getattr(self, "g%s" % str(p_id))
            
            cantidad_recibida = 0
            cantidad_actual = 0

            for i in g:
                cantidad_recibida += i['c_r']
                cantidad_actual += i['c_a']


            json_data.append([
                str(g[0]['nombre']),
                str(cantidad_recibida),
                str(g[0]['creado']),
                str(g[0]['desde'])
            ])
            

        return json_data


    def get_initial_queryset(self):
        try:
            enc = self.usuario.persona_set.encargado_set
            return self.model.objects.filter(condicion=True,
                   traslado__cantidad_actual__gte=0,encargado=enc)
        except:
            return self.model.objects.filter(condicion=True,
                   traslado__cantidad_actual__gte=0)




class ListarProductos(DataTable):
    model = Producto
    columns = ['nombre_unidad', 'costo', 'dolar', 'botones']
    order_columns = ['nombre']
    max_display_length = 1000

    def _get_costo_producto(self, obj):
        try:
            costo = CostoProducto.objects.filter(condicion=True,
                    producto=obj).order_by('id').last()
            return costo
        except Exception as e:
            return 0

    def get_costo_producto(self, obj):
        try:
            costo = self._get_costo_producto(obj)
            return str(costo.monto.monto).replace('.',',') + ' Bs'
        except Exception as e:         
            return 0

    def get_costo_dolar(self, obj):
        costo = self._get_costo_producto(obj)
        try:
            return str(costo.monto_dolar).replace('.',',') + ' $'
        except Exception as e:
            pass

        return 'Sin fijar'

    def render_column(self, row, column):
        if column == 'botones':
            return self.botones(row)
        elif column == 'costo':
            return self.get_costo_producto(row)
        elif column == 'dolar':
            return self.get_costo_dolar(row)
        else:
            return super(ListarProductos, self).render_column(row, column)

    def botones(self, row):
        btn = Botones()
        # hay que cambiar estos permisos
        if self.usuario.has_perm('general.change_cliente'):
            btn.click("fijar_precio_action("+str(row.id)+",'"+row.nombre+"')", 'success', 'money-bill','Fijar precio')
            btn.click("modificar_producto_action("+str(row.id)+",'"+row.nombre+"','"+str(row.unidad)+"')", 'info', 'edit')
        if self.usuario.has_perm('delete.change_cliente'):
            if row.condicion:
                if Stock.objects.filter(producto=row).count() == 0:
                    btn.click("eliminar_dato('producto','"+str(row.id)+"')" , 'danger', 'recycle')
            else:
                btn.click("eliminar_dato('producto','"+str(row.id)+"')" , 'success', 'check')
        return str(btn)

    def get_initial_queryset(self):
        return self.model.objects.filter(condicion=True).order_by('nombre')    

    def filter_queryset(self, qs):
        search = self.request.POST.get(u'search[value]', None)
        if search:
            qs = qs.filter(nombre__icontains = search)
          
        return qs


class ListarTraslados(DataTable):
    model = AsignacionStock
    columns = ['encargado.persona.primer_nombre','stock.producto.nombre_unidad',
                'cantidad_recibida','aceptado','fecha','botones']
    order_columns = columns
    max_display_length = 1000

    def get_initial_queryset(self):
        return self.model.objects.filter(condicion=True)

    def filter_queryset(self, qs):        
        return qs

    def prepare_results(self, qs):

        json_data = []
        # print (qs.values('stock__producto__id') \
        #   .annotate(c_a=Sum('cantidad_actual'),c_r=Sum('cantidad_recibida')) \
        #   .order_by('-c_a'))        
        #print(qs) 

        productos = []

        encs = self.model.objects.filter(condicion=True).values('encargado_id')\
               .distinct('encargado_id').order_by()

        for e in encs:
            nqs = self.model.objects.filter(condicion=True, encargado_id=e['encargado_id'])

            search = self.request.POST.get(u'search[value]', None)
            if search:
                nqs = nqs.filter(stock__producto__nombre__icontains = search)

            filter_traslados_aceptado = self._querydict.get('filter_traslados_aceptado', None)
            if filter_traslados_aceptado:
                if filter_traslados_aceptado != 'Aceptado':
                    cond = True if filter_traslados_aceptado=='SI' else False
                    nqs = nqs.filter(aceptado=cond)


            filter_traslados_encargado = self._querydict.get('filter_traslados_encargado', None)
            if filter_traslados_encargado:
                try:
                    nqs = nqs.filter(encargado__id=filter_traslados_encargado)
                except ValueError:
                    pass


            for stock in nqs:

                p_id = str(stock.stock.producto.id) +' '+ str(e['encargado_id'])       
                if p_id not in productos:
                    productos.append(p_id)               
                
                o_p = {
                'id':stock.stock.producto.id,
                'encargado':stock.encargado,
                'encargado_id':stock.encargado.pk,
                'aceptado':stock.aceptado,
                'c_a':stock.cantidad_actual,
                'c_r':stock.cantidad_recibida,
                'nombre':stock.stock.producto.nombre,
                'creado':stock.created.strftime("%d-%m-%Y"),
                }

                grup = getattr(self, 'g%s' % str(p_id) , None)
                if not grup:
                    setattr(self,'g%s' % str(p_id), [])
                    grup = getattr(self, 'g%s' % str(p_id) , [])
                grup.append(o_p)


        for p_id in productos:
            g = getattr(self, "g%s" % str(p_id))
            
            cantidad_recibida = 0
            cantidad_actual = 0
            aceptado = True

            for i in g:
                cantidad_recibida += i['c_r']
                cantidad_actual += i['c_a']

                if not i['aceptado']:
                    aceptado = False

            if aceptado:
                str_translado = 'Si'
            else:
                str_translado = 'No'

            btn = Botones()
            btn.click("eliminar_translados('"+str(g[0]['id'])+"','"+str(g[0]['encargado_id'])+"')",
                      'danger', 'recycle')            
            
            json_data.append([
                str(g[0]['encargado']),
                str(g[0]['nombre']),
                str(cantidad_recibida),
                str(str_translado),
                str(g[0]['creado']), 
                btn()
            ])

        return json_data
