from django.conf.urls import include, url
from rest_framework import routers
from rest_framework.routers import SimpleRouter
from .viewsets import StockViewSet, StockDetailViewSet,\
StockChangeViewSet, ProductoViewSet, StockEncargadoViewSet, \
StockMontoViewSet, AsignacionStockAceptarViewSet, AsignacionStockAsignarViewSet,\
StockProductoViewSet, StockEncargadoDetailViewSet, EncargadoAEncargadoViewSet,\
AsignacionStockAceptarProductosViewSet, StockProductoEncargadoDetailViewSet,\
EliminarStockProductosEncargados, ProductoListViewSet, PrecioDolarViewSet


router = SimpleRouter()
router.register("list_producto", ProductoListViewSet)
router.register("producto", ProductoViewSet)


urlpatterns = [

	url(r'^stock/(?P<pk>\d+)/change/$', StockChangeViewSet.as_view(), name="stock-change"), 
    url(r'^stock/producto/(?P<pk>\d+)/monto/$', StockMontoViewSet.as_view(), name="stock-monto"),
    url(r'^stock/(?P<pk>\d+)/aceptar/$', AsignacionStockAceptarViewSet.as_view(),
        name="stock-aceptar"),
    url(r'^stock/(?P<pk>\d+)/aceptar_por_productos/$', AsignacionStockAceptarProductosViewSet.as_view(),
        name="stock-aceptar-por-productos"),    
    url(r'^stock/(?P<pk>\d+)/$', StockDetailViewSet.as_view(), name="stock-detail"),


    ### aqui puedo asignar teniendo el id del stock
    url(r'^stock/(?P<pk>\d+)/asignar/$', AsignacionStockAsignarViewSet.as_view(),
        name="stock-asignar"),    

    ### aqui puedo asignar sin tener el id del stock
    url(r'^stock/encargado/$', StockEncargadoViewSet.as_view(), name="stock-encargado"),
    
    url(r'^stock/encargado/(?P<pk>\d+)/$', StockEncargadoDetailViewSet.as_view(),
        name="stock-encargado-detail"),    
    url(r'^stock_para_producto/encargado/(?P<pk>\d+)/$', StockProductoEncargadoDetailViewSet.as_view(),
        name="stock-encargado-detail"), 


    url(r'^stock_para_producto/(?P<pk>\d+)/encargado_a_encargado/$', EncargadoAEncargadoViewSet.as_view(),
        name="encargado-a-encargado"),

    url(r'^stock_para_producto/(?P<p>\d+)/(?P<e>\d+)/$', EliminarStockProductosEncargados.as_view(),
        name="eliminar-stock-producto-encargado"),

    url(r'^stock/producto/(?P<pk>\d+)/$', StockProductoViewSet.as_view(), name="stock-producto"),
    url(r'^stock/precio_dolar/(?P<pk>\d+)/$', PrecioDolarViewSet.as_view(), name="stock-precio_dolar"),
    url(r'^stock/$', StockViewSet.as_view(), name="stock"),
    url(r'^', include(router.urls)), 

]

