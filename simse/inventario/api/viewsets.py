from uuid import UUID
from rest_framework.generics import ListAPIView, RetrieveAPIView,\
    get_object_or_404, CreateAPIView, RetrieveUpdateAPIView
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework import pagination
from rest_framework.response import Response
from rest_framework import status, viewsets
from rest_framework.decorators import list_route
from rest_framework.mixins import ListModelMixin

from django.contrib.auth import get_user_model
from common.mixins import BaseAPIView, BasePostAPIView, BorrarView
from encargado.models import Encargado
from .serializers import StockSerializer, StockChangeSerializer,\
    ProductoSerializer, StockEncargadoSerializer, StockMontoSerializer,\
    AsignacionStockSerializer, EncargadoAEncargadoSerializer, \
    AsignacionStockProductosSerializer, PrecioDolarSerializer

from ..models import Stock, Producto, AsignacionStock, TrasladoEncargado, PrecioDolar


class StockViewSet(ListAPIView, BasePostAPIView):
    """

    get:
        Lista todos los gastos en el stock

    post:
        Registrar un nuevo ingreso en el stock 

    """
    serializer_class = StockSerializer
    permission_classes = (IsAuthenticated,)
    queryset = Stock.objects.all()


class StockDetailViewSet(RetrieveUpdateAPIView, BorrarView, BaseAPIView):
    """

    get:
        Regresa un stock

    delete:
        Activa o desactiva a un stock por pk de url

    patch:
        actualiza un stock
    
    """
    serializer_class = StockSerializer
    permission_classes = (IsAuthenticated,)
    queryset = Stock.objects.all()


class StockChangeViewSet(BaseAPIView):
    """

    patch:
        Descuenta o incrementa una cantidad determinada al stock

    
    """
    serializer_class = StockChangeSerializer
    permission_classes = (IsAuthenticated,)

    def get_object(self):
        stock = get_object_or_404(Stock,pk=self.kwargs['pk'])
        return stock

    def patch(self, request, *args, **kwargs):

        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        
        serializer.save()
        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)


class StockMontoViewSet(BaseAPIView):
    """
    patch:
        Actualiza el precio de un producto.

    """
    serializer_class = StockMontoSerializer
    permission_classes = (IsAuthenticated,)

    def get_object(self):
        stock = get_object_or_404(Producto,pk=self.kwargs['pk'])
        return stock

    def patch(self, request, *args, **kwargs):

        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        if getattr(instance, '_prefetched_objects_cache', None):
            instance._prefetched_objects_cache = {}
        return Response(serializer.data)


class ProductoViewSet(viewsets.ModelViewSet):
    """
        Endpoint de productos
    """
    serializer_class = ProductoSerializer
    queryset = Producto.objects.all()
    permission_classes = (IsAuthenticated,)

    @list_route(['get'])
    def con_stock(self, request, pk=None, **kwargs):
        """
        devuelve solo los productos con stock
        """
        s = Stock.objects.filter(
                cantidad_actual__gt=0,
                condicion=True).order_by("created")\
            .values_list('producto__pk')
        #Stock 
        queryset = Producto.objects.filter(pk__in=s)
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)



class ProductoListViewSet(ListModelMixin, viewsets.GenericViewSet):
    """
        Lista todos los productos
    """
    serializer_class = ProductoSerializer
    queryset = Producto.objects.all()
    permission_classes = (IsAuthenticated,)

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class StockEncargadoViewSet(CreateAPIView, BaseAPIView):
    """
        create: Asigna un stock para un encargado sin id de stock
    """
    serializer_class = StockEncargadoSerializer
    permission_classes = (IsAuthenticated,)
    queryset = AsignacionStock.objects.all()

    def create(self, request, *args, **kwargs):
        import json
        data = json.loads(request.body.decode('utf-8'))
        for obj in data:
            serializer = self.get_serializer(data=obj)
            serializer.is_valid(raise_exception=True)
            serializer.save()
            headers = self.get_success_headers(serializer.data)
        return Response({"success": True}, status=status.HTTP_201_CREATED)


class StockEncargadoDetailViewSet(BorrarView, RetrieveAPIView, BaseAPIView):
    """
    DELETE:
        Borra la asignacion del stock de un encargado
        por el id del stock
        
    GET:
        Detalle del stock de un encargado
    """
    serializer_class = StockEncargadoSerializer
    permission_classes = (IsAuthenticated,)
    queryset = AsignacionStock.objects.all()


class StockProductoEncargadoDetailViewSet(BorrarView, BaseAPIView):
    """
    DELETE:
        Borra las asignacion del stock de un encargado
        para un producto por el id del stock

    """

    permission_classes = (IsAuthenticated,)

    def delete(self, request, *args, **kwargs):

        p = request.query_params
        persona = request.user.persona_set
        encargado = Encargado.objects.get(persona=persona)

        if p.get('borrar', None) and p.get('reenviar_encargado', None ):
            try:
                a_s = AsignacionStock.objects.get(encargado=encargado,
                                                  id=kwargs['pk'])
                tras = TrasladoEncargado.objects.get(traslado=a_s)
                tras.origen.cantidad_actual += a_s.cantidad_recibida
                tras.origen.save()
                a_s.delete()
            except AsignacionStock.DoesNotExist as e:
                pass

        elif p.get('borrar', None):

            try:
                a_s = AsignacionStock.objects.get(encargado=encargado,
                                                  id=kwargs['pk'])         
                a_s.reenvio_admin()
            except AsignacionStock.DoesNotExist as e:
                pass

        return Response(status=status.HTTP_204_NO_CONTENT)


class EliminarStockProductosEncargados(BaseAPIView):

    permission_classes = (IsAuthenticated,)

    def delete(self, request, *args, **kwargs):
        if request.query_params.get('borrar', None):
            encargado = Encargado.objects.get(pk=kwargs['e'])
            a_s = AsignacionStock.objects.filter(encargado=encargado,
                                                 stock__producto__id=kwargs['p'])
            for i in a_s:
                print ("Estoy aqui")
                print (i)
                i.delete()

            return Response(status=status.HTTP_204_NO_CONTENT)


class AsignacionStockAceptarViewSet(BaseAPIView):
    """
    patch:
    Un encargado acepta el stock por el id

    """
    serializer_class = AsignacionStockSerializer
    permission_classes = (IsAuthenticated,)

    def get_object(self):
        stock = get_object_or_404(AsignacionStock,
                                  pk=self.kwargs['pk'])
        return stock

    def patch(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        if getattr(instance, '_prefetched_objects_cache', None):
            instance._prefetched_objects_cache = {}
        return Response(serializer.data)


class AsignacionStockAceptarProductosViewSet(CreateAPIView,
                                             AsignacionStockAceptarViewSet):
    """
    post:
    Un encargado acepta todo los stock para el id de producto 

    """
    serializer_class = AsignacionStockProductosSerializer
    permission_classes = (IsAuthenticated,)


class AsignacionStockAsignarViewSet(AsignacionStockAceptarViewSet):

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            obj = serializer.save()
            return Response({"success": True})


class EncargadoAEncargadoViewSet(BaseAPIView):
    """
    patch:
        Asigna parte de un stock de un encargado a otro encargado

    """
    serializer_class = EncargadoAEncargadoSerializer
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            obj = serializer.save()
            return Response({"success": True})


class StockProductoViewSet(ListAPIView, BaseAPIView):
    """

    get:
        Lista todos los stock para un producto por pk

    """
    serializer_class = StockSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return Stock.objects.filter(producto__pk=self.kwargs['pk'])




class PrecioDolarViewSet(RetrieveAPIView, CreateAPIView, BaseAPIView):
    """
    DELETE:
        Borra el precio actual del dolar
        
    GET:
        Obtener el dolar actual
    """
    serializer_class = PrecioDolarSerializer
    permission_classes = (IsAuthenticated,)
    queryset = PrecioDolar.objects.all()

        