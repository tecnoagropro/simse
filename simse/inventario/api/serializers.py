from decimal import Decimal
from django.contrib.auth import get_user_model
from django.urls import resolve
from django.utils.translation import ugettext as _
from django.utils import timezone
from django.db.models import Sum

from rest_framework import serializers
from rest_framework.serializers import HyperlinkedModelSerializer,raise_errors_on_nested_writes
from rest_framework.generics import get_object_or_404
from rest_framework.compat import set_many
from rest_framework.utils import model_meta

from common.serializers import QueryFieldsMixin
from accounts.models import Persona
from encargado.models import Encargado
from encargado.api.serializers import EncargadoSesion
from ..models import Stock, Producto, AsignacionStock, Costo,\
    CostoProducto, TrasladoEncargado, PrecioDolar

User = get_user_model()

M_cantidad_inválida = _("Cantidad inválida.")


class ProductoSerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        nombre = validated_data.pop('nombre')
        unidad = validated_data.pop('unidad')

        producto, creado = Producto.objects.get_or_create(
                    nombre=nombre,
                    unidad=unidad)

        producto.save()
        return producto

    class Meta:
        model = Producto
        fields = ('id','nombre','unidad')


class StockSerializer(QueryFieldsMixin, HyperlinkedModelSerializer):

    monto = serializers.DecimalField(max_digits=30, decimal_places=2)
    producto = serializers.IntegerField(write_only=True)
    cantidad_recibida = serializers.DecimalField(max_digits=30, decimal_places=2)
    cantidad_actual = serializers.DecimalField(max_digits=30, decimal_places=2, read_only=True)
    datos_producto = ProductoSerializer(source='producto', read_only=True)
    fecha_registro = serializers.DateField(required=False, format="%d/%m/%Y")
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())

    def validate(self, attrs):
        errors = dict()
        cantidad_recibida = attrs.get('cantidad_recibida')
        user = attrs.get('user')
        
        if cantidad_recibida <= 0:
            errors['cantidad_recibida'] = M_cantidad_invalida
        
        if errors:
            raise serializers.ValidationError(errors)

        return super(StockSerializer, self).validate(attrs)

    def validate_producto(self,pk):
        return Producto.objects.get(id=pk)

    def validate_monto(self,monto):
        try:
            return Costo.adquirir(monto=monto)
        except Costo.MultipleObjectsReturned as e:
            return Costo.objects.filter(monto=monto).first()

    def create(self, validated_data):

        cantidad_actual = validated_data.pop('cantidad_recibida',None)
        fecha_registro = validated_data.pop('fecha_registro',
                                            timezone.now())

        stock = Stock(cantidad_actual = cantidad_actual,
                      cantidad_recibida = cantidad_actual,
                      fecha_registro=fecha_registro,
                      **validated_data)
        stock.save()
        return stock


    class Meta:
        model = Stock
        fields = ('id','producto','cantidad_recibida',
                  'cantidad_actual','monto','datos_producto','user',
                  'fecha_registro',)



class StockChangeSerializer(QueryFieldsMixin, HyperlinkedModelSerializer):

    cantidad = serializers.DecimalField(max_digits=30, decimal_places=2,
                                        write_only=True)
    
    def validate(self, attrs):
            
        errors = dict()
        cantidad = attrs.get('cantidad')

        if cantidad == 0:
            errors['cantidad'] = M_cantidad_invalida

        if errors:
            raise serializers.ValidationError(errors)

        return super(StockChangeSerializer, self).validate(attrs)

    def update(self, instance, validated_data):

        cantidad = int(validated_data.get('cantidad'))
        instance.cantidad_actual = instance.cantidad_actual + cantidad
        instance.save()
        return instance

    class Meta:
        model = Stock
        fields = ('cantidad','id','cantidad_actual',)


class StockMontoSerializer(QueryFieldsMixin, HyperlinkedModelSerializer):

    monto = serializers.DecimalField(max_digits=30, decimal_places=2,
                                     write_only=True)

    def validate(self, attrs):
        errors = dict()
        if errors:
            raise serializers.ValidationError(errors)
        return attrs

    def update(self, instance, validated_data):
        costo = CostoProducto()
        monto = validated_data['monto']
        monto_bs = monto * PrecioDolar.objects.filter().first().monto.monto

        try:
            _monto = Costo.adquirir(monto)
        except Costo.MultipleObjectsReturned as e:
            _monto = Costo.objects.filter(monto=monto).first()

        try:
            costo.monto = Costo.adquirir(monto_bs)
        except Costo.MultipleObjectsReturned as e:
            costo.monto = Costo.objects.filter(monto=monto_bs).first()
        
        costo.monto_dolar = _monto
        costo.producto = instance
        costo.save()
        return instance

    class Meta:
        model = Stock
        fields = ('id','monto',)


class AsignacionStockSerializer(HyperlinkedModelSerializer):

    aceptado = serializers.BooleanField(default=False)
    cantidad_recibida = serializers.DecimalField(max_digits=30, decimal_places=2)
    encargado = serializers.IntegerField(write_only=True)
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())

    def update(self, instance, validated_data):
        instance.aceptar()
        return instance

    class Meta:
        model = AsignacionStock
        fields = ('id','aceptado','cantidad_recibida','user','encargado')


class AsignacionStockProductosSerializer(HyperlinkedModelSerializer):

    user = serializers.HiddenField(default=serializers.CurrentUserDefault())
    encargado = serializers.HiddenField(default=EncargadoSesion())

    def create(self, validated_data):
        producto_id = self.context['view'].kwargs['pk']

        stocks =  AsignacionStock.objects.filter(aceptado=False,
                  encargado=validated_data['encargado'],
                  stock__producto__id=producto_id)
        for i in stocks:
            i.aceptar()
        
        return validated_data

    class Meta:
        model = AsignacionStock
        fields = ('id','user','encargado')


class EncargadoAEncargadoSerializer(HyperlinkedModelSerializer):

    cantidad_recibida = serializers.DecimalField(max_digits=30, decimal_places=2)
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())
    encargado = serializers.IntegerField(write_only=True)

    def validate(self, attrs):
        
        cantidad_recibida = attrs.get('cantidad_recibida',None)
        errors = dict()
        producto_id = self.context['view'].kwargs['pk']
        user = attrs.get('user', None)
        enc = user.persona_set
        enc = Encargado.objects.get(persona=enc)    
        qs = AsignacionStock.objects.filter(
                condicion=True,
                encargado=enc,
                stock__producto_id=producto_id,
                aceptado=True)\
                .order_by()
        en_stock = qs.values('cantidad_actual').aggregate(cuenta=Sum('cantidad_actual'))['cuenta']

        if not en_stock:
            errors['stock'] = "El monto a trasladar es mayor que la cantidad actual"
        elif en_stock < float(cantidad_recibida):
            errors['stock'] = "El monto a trasladar es mayor que la cantidad actual"

        if errors:
            raise serializers.ValidationError(errors)
        return attrs

    def create(self, validated_data):

        producto_id = self.context['view'].kwargs['pk']
        cantidad_recibida = validated_data.pop('cantidad_recibida',None)       
        encargado = validated_data.pop('encargado', None)# al que voy a asignar
        user = validated_data.get('user', None)
        enc = Encargado.objects.get(persona=user.persona_set)
        qs = AsignacionStock.objects.filter(
                condicion=True,
                encargado=enc,
                stock__producto_id=producto_id,
                aceptado=True)\
                .order_by()

        pendiente = True

        en_stock = qs.values('cantidad_actual').aggregate(cuenta=Sum('cantidad_actual'))['cuenta']
        cantidad_total = cantidad_recibida
        if cantidad_total <= en_stock:
            for stock_producto in qs:
                if pendiente:
                    astock = AsignacionStock()
                    astock.encargado_id = encargado
                    #consulto cantidad_total
                    if cantidad_total <= stock_producto.cantidad_actual:
                        astock.cantidad_recibida = cantidad_recibida
                        astock.cantidad_actual = cantidad_recibida
                        astock.stock_id = stock_producto.stock.id
            
                        #descuento del stock del encardo que transifere
                        stock_producto.cantidad_actual -= cantidad_recibida
                        stock_producto.save()
                        pendiente = False
                    else:
                        astock.cantidad_recibida = stock_producto.cantidad_actual
                        astock.cantidad_actual = stock_producto.cantidad_actual
                        cantidad_total = Decimal(cantidad_total)
                        cantidad_total -= Decimal(stock_producto.cantidad_actual)

                        #descuento del stock del encardo que transifere
                        #cero porque transfirio todo lo que le quedaba
                        stock_producto.cantidad_actual = 0
                        stock_producto.save()

                    astock.user = user if user else instance.user
                    astock.stock = stock_producto.stock
                    astock.save_no_validate()

                    TrasladoEncargado.objects.create(origen=stock_producto,
                                                     traslado=astock,
                                                     user=user if user else instance.user)
                    

        return astock

    class Meta:
        model = AsignacionStock
        fields = ('cantidad_recibida','user','encargado')


class StockEncargadoSerializer(QueryFieldsMixin, HyperlinkedModelSerializer):

    encargado_id =  serializers.IntegerField(write_only=True)
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())
    cantidad_asignada = serializers.DecimalField(max_digits=30, decimal_places=2,
                                                 write_only=True)
    producto_id = serializers.IntegerField(write_only=True)
    producto_nombre = serializers.CharField(read_only=True, source='stock.producto.nombre')
    producto_stock_id = serializers.IntegerField(read_only=True, source='stock.producto.id')
    cantidad_actual = serializers.SerializerMethodField()
    producto_unidad = serializers.CharField(read_only=True, source='stock.producto.unidad')
    costo_producto = serializers.SerializerMethodField()

    def get_cantidad_actual(self, obj):
        return obj.cantidad_actual

    def get_costo_producto(self, obj):
        try:
            return CostoProducto.objects.filter(condicion=True,
                   producto=obj.stock.producto).order_by('id').last().monto.monto
        except:
            return 0

    def validate(self, attrs):
        errors = dict()
        attrs['encargado'] = Encargado.objects.get(id=attrs['encargado_id'])
        producto_id = attrs.get('producto_id')
        cantidad_total = attrs['cantidad_asignada']
        qs = Stock.objects.filter(condicion=True, producto_id=producto_id)
        en_stock = qs.values('cantidad_actual').aggregate(cuenta=Sum('cantidad_actual'))['cuenta']

        if cantidad_total > en_stock:
             errors['cantidad_asignada'] = 'Cantidad de inventario superada'

        if errors:
            raise serializers.ValidationError(errors)

        return attrs

    def create(self, validated_data):

        producto_id = validated_data.pop('producto_id')
        qs = Stock.objects.filter(condicion=True, producto_id=producto_id,
                                  cantidad_actual__gt=0)
        cantidad_total = validated_data['cantidad_asignada']
        pendiente = True
        # en_stock = qs.values('cantidad_actual').aggregate(cuenta=Sum('cantidad_actual'))['cuenta']
        # if cantidad_total <= en_stock:
        for stock_producto in qs:
            if pendiente:
                a_stock = AsignacionStock()
                a_stock.stock = stock_producto
                a_stock.user = validated_data['user']
                a_stock.encargado = validated_data['encargado']            
                if cantidad_total <= stock_producto.cantidad_actual:
                    a_stock.cantidad_recibida = cantidad_total
                    pendiente = False
                else:
                    a_stock.cantidad_recibida = stock_producto.cantidad_actual
                    cantidad_total -= stock_producto.cantidad_actual
                    
                a_stock.save()
                last_stock = a_stock

        return last_stock  

    class Meta:
        model = AsignacionStock
        fields = ('id','cantidad_asignada','producto_nombre',
                  'producto_stock_id','producto_unidad','costo_producto',
                  'encargado_id', 'user','producto_id','cantidad_actual')



class PrecioDolarSerializer(serializers.ModelSerializer):

    monto = serializers.DecimalField(max_digits=30, decimal_places=2)

    def create(self, validated_data):
        monto_valor = validated_data.pop('monto')

        #Se actualiza el dolar
        _mv = Costo.adquirir(monto_valor)
        PrecioDolar.objects.all().delete()
        obj = PrecioDolar.objects.create(monto=_mv)


        qs = Producto.objects.filter(condicion=True)

        #Actualizacion de precios actuales
        for producto in qs:
            costo_actual = CostoProducto.objects.filter(condicion=True,
                           producto=producto).order_by('id').last()
            print ('costo_actual',costo_actual)
            if costo_actual and costo_actual.monto_dolar:
                v = monto_valor * costo_actual.monto_dolar.monto
                costo_actual.monto = Costo.adquirir(v)
                costo_actual.save()

        return obj

    class Meta:
        model =  PrecioDolar
        fields = ('id','monto')

