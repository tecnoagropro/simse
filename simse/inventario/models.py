from decimal import Decimal
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError
from django.conf import settings
from django.utils import timezone
from common.mensajes import M
from common.models import UserOneBaseModel, BaseModel, UserRelationBaseModel, BaseManager
from accounts.models import Persona
from common.mensajes import M

class Producto(BaseModel):

    nombre = models.CharField(max_length=300)
    unidad = models.CharField(max_length=40, default='lt')

    @property
    def nombre_unidad(self):
        return "%s %s" % (self.nombre, self.unidad)

    @property
    def mi_costo_actual(self):
        try:
            cp = CostoProducto.objects.activos(producto=self).last()
            return cp.monto.monto
        except Exception as e:
            return 0


    def __str__(self):
        return self.nombre

    class Meta:
        ordering = ("nombre",)


class Costo(BaseModel):

    monto = models.DecimalField(max_digits=30, decimal_places=2)

    @staticmethod
    def adquirir(monto):
        try:
            v,c = Costo.objects.get_or_create(monto=monto)
            return v
        except Costo.MultipleObjectsReturned as e:
            return Costo.objects.filter(monto=monto).first()
        

    def __str__(self):
        return str(self.monto)

    class Meta:
        ordering = ("monto",)
        verbose_name = _('Costo')
        verbose_name_plural = _('Costos')


class Stock(UserRelationBaseModel):

    producto = models.ForeignKey(Producto)
    cantidad_recibida = models.DecimalField(max_digits=30, decimal_places=2, default=0)
    cantidad_actual = models.DecimalField(max_digits=30, decimal_places=2, default=0)
    monto = models.ForeignKey(Costo)
    fecha_registro = models.DateField()

    @property
    def costo(self):

        if self.cantidad_recibida != 0:
            return self.monto.monto / self.cantidad_recibida
        return 0

    def get_costo_gastado(self):
        cantida_gastada = self.cantidad_recibida - self.cantidad_actual
        return cantida_gastada * self.costo

    def descontar(self, cant):
        if self.descontable(cant):
            self.cantidad_actual -= cant
            self.save()
            return True
        return False

    def descontable(self, cant):
        if cant <= self.cantidad_actual:
            return True
        return False

    def save(self, *args, **kwargs):
        try:
            monto, creado = Costo.objects.get_or_create(monto=self.monto_tmp)
            self.monto = monto 
        except:
            pass
        super(Stock, self).save(*args, **kwargs)


    class Meta:
        ordering = ("fecha_registro", "producto")
        verbose_name = _('Stock')
        verbose_name_plural = _('Stock')

    def __str__(self):
        return str(self.producto)


class AsignacionStockQuerySet(models.QuerySet):

    def monto_por_vender(self):
        qs = self.filter()
        cant = 0
        for i in qs:
            cant += i.pendiente_por_vender
        return cant


class AsignacionStockManager(BaseManager):

    use_in_migrations = True 

    def get_queryset(self):
        return AsignacionStockQuerySet(self.model, using=self._db)

    def monto_por_vender(self):
        return self.get_queryset().monto_por_vender()


class AsignacionStock(UserRelationBaseModel):

    stock = models.ForeignKey(Stock)
    encargado = models.ForeignKey("encargado.Encargado", related_name='stock')
    cantidad_recibida = models.DecimalField(max_digits=30, decimal_places=2,
                                            default=0)
    cantidad_actual = models.DecimalField(max_digits=30, decimal_places=2,
                                          default=0)
    aceptado = models.BooleanField(default=False)
    fecha_aceptado = models.DateTimeField(null=True, blank=True)
    objects = AsignacionStockManager()

    def __str__(self):
        return str(self.encargado)

    @property
    def pendiente_por_vender(self):
        r = self.cantidad_actual * self.stock.producto.mi_costo_actual
        return r

    def aceptar(self):
        self.aceptado = True
        self.fecha_aceptado = timezone.now()
        self.save()

    def asignar(self):
        self.cantidad_actual = self.cantidad_recibida
        return self.stock.descontar(self.cantidad_recibida)

    def descontar(self, cantidad):
        self.cantidad_actual -= Decimal(cantidad)
        self.save()

    def reversar(self, cantidad):
        self.cantidad_actual += cantidad
        self.save()

    def reenvio_admin(self):
        self.stock.cantidad_actual += self.cantidad_recibida
        self.stock.save()
        self.delete()

    def delete(self, *args, **kwargs):
        super(AsignacionStock, self).delete(*args, **kwargs)

    def save(self, *args, **kwargs):
        if not self.id and not self.asignar():
            raise ValidationError('Cantidad de inventario superada')

        if self.cantidad_actual < 0:
            raise ValidationError(_("Cantidad en stock superada."))
        super(AsignacionStock, self).save(*args, **kwargs)

    def save_no_validate(self, *args, **kwargs):
        super(AsignacionStock, self).save(*args, **kwargs)

    class Meta:
        ordering = ("created",)
        verbose_name = _('Asignacion Stock')
        verbose_name_plural = _('Asignacion Stock')


class TrasladoEncargado(UserRelationBaseModel):

    origen =   models.ForeignKey(AsignacionStock, related_name="origen_set")
    traslado = models.ForeignKey(AsignacionStock, related_name="destino_set")

    class Meta:
        ordering = ("created",)
        verbose_name = _('Traslado Encargado')
        verbose_name_plural = _('Traslados Encargado')


class CostoProducto(BaseModel):

    producto = models.ForeignKey(Producto)
    monto = models.ForeignKey(Costo)
    monto_dolar = models.ForeignKey(Costo, null=True, blank=True, related_name="costoproductomontodolar_set")

    def save(self, *args, **kwargs):
        try:
            monto, creado = Costo.objects.get_or_create(monto=self.monto_tmp)
            self.monto = monto 
        except:
            pass
        super(CostoProducto, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.producto) + ' ' + str(self.monto)

    class Meta:
        ordering = ("id",)
        verbose_name = _('Costo producto')
        verbose_name_plural = _('Costos producto')
        get_latest_by = 'id'



class PrecioDolar(BaseModel):

    monto = models.ForeignKey(Costo)

    def __str__(self):
        return str(self.monto)

    class Meta:
        ordering = ("id",)
        verbose_name = _('Dolar')
        verbose_name_plural = _('Dolares')
        get_latest_by = 'id'

