# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2018-09-13 23:15
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('inventario', '0017_auto_20180821_2002'),
    ]

    operations = [
        migrations.CreateModel(
            name='TrasladoEncargado',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('condicion', models.BooleanField(default=True)),
                ('origen', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='origen_set', to='inventario.AsignacionStock')),
                ('traslado', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='destino_set', to='inventario.AsignacionStock')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Traslado Encargado',
                'verbose_name_plural': 'Traslados Encargado',
                'ordering': ('created',),
            },
        ),
    ]
