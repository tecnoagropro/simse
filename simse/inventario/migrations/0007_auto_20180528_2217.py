# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2018-05-29 02:17
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('inventario', '0006_costoasignacionstock'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='costoasignacionstock',
            options={'ordering': ('monto',), 'verbose_name': 'Costo asignacion', 'verbose_name_plural': 'Costos asignacion'},
        ),
        migrations.RemoveField(
            model_name='costoasignacionstock',
            name='stock',
        ),
        migrations.AddField(
            model_name='costoasignacionstock',
            name='producto',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='inventario.Producto'),
            preserve_default=False,
        ),
    ]
