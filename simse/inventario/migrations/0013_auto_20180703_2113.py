# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2018-07-03 21:13
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('inventario', '0012_auto_20180703_0252'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='asignacionstock',
            options={'ordering': ('created',), 'verbose_name': 'Asignacion Stock', 'verbose_name_plural': 'Asignacion Stock'},
        ),
        migrations.AlterField(
            model_name='asignacionstock',
            name='encargado',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='stock', to='encargado.Encargado'),
        ),
    ]
