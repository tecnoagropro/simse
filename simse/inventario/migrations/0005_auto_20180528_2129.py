# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2018-05-29 01:29
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inventario', '0004_auto_20180528_1901'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='costo',
            options={'ordering': ('monto',), 'verbose_name': 'Costo', 'verbose_name_plural': 'Costos'},
        ),
    ]
