# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2018-08-12 20:08
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inventario', '0015_auto_20180726_1240'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='producto',
            options={'ordering': ('nombre',)},
        ),
        migrations.AlterModelOptions(
            name='stock',
            options={'ordering': ('fecha_registro', 'producto'), 'verbose_name': 'Stock', 'verbose_name_plural': 'Stock'},
        ),
    ]
