# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2018-05-29 01:35
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('inventario', '0005_auto_20180528_2129'),
    ]

    operations = [
        migrations.CreateModel(
            name='CostoAsignacionStock',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('condicion', models.BooleanField(default=True)),
                ('monto', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='inventario.Costo')),
                ('stock', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='inventario.AsignacionStock')),
            ],
            options={
                'verbose_name': 'Costo',
                'verbose_name_plural': 'Costos',
                'ordering': ('monto',),
            },
        ),
    ]
