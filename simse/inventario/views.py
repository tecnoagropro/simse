from django.shortcuts import render
from .models import Producto
from encargado.models import Encargado 

def listar_inventario(request):
    return render(request,'listar_inventario.html',{})

def registrar(request):
    return render(request,'registrar_inventario.html',{})

def detalle(request, pk):
    producto = Producto.objects.get(pk=pk)
    context = {
        'producto':producto.nombre,
        'id':producto.id
    }
    return render(request,'listar_inventario_producto.html',context)