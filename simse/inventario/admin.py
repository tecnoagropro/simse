from django.contrib import admin
from .models import *
from common.admin import BaseAdmin

class StockAdmin(BaseAdmin):
    search_fields = ('producto__nombre',)
    list_display = ('producto', 'cantidad_actual',
                    'cantidad_recibida','monto','fecha_registro')


admin.site.register(Stock,StockAdmin)

@admin.register(AsignacionStock)
class AsignacionStockAdmin(BaseAdmin):
    list_display = ('encargado','stock','aceptado', 'cantidad_actual',
                    'cantidad_recibida','pendiente_por_vender', 'pro_id')
    list_filter = ('aceptado','encargado',)

    def pro_id(self,obj):
        return str(obj.stock.producto.id)


@admin.register(Producto)
class ProductoAdmin(BaseAdmin):
    list_display = ('nombre','unidad','mi_costo_actual')


admin.site.register(Costo)

@admin.register(CostoProducto)
class CostoProductoAdmin(BaseAdmin):
    list_display = ('producto','monto',)
    list_filter = (
        ('producto', admin.RelatedOnlyFieldListFilter),
        ('monto', admin.RelatedOnlyFieldListFilter),
    )    


@admin.register(TrasladoEncargado)
class TrasladoEncargadoAdmin(BaseAdmin):
    list_display = ('origen','traslado','created')
    list_filter = (
        ('origen', admin.RelatedOnlyFieldListFilter),
        ('traslado', admin.RelatedOnlyFieldListFilter),
    )   

    

@admin.register(PrecioDolar)
class PrecioDolarAdmin(BaseAdmin):
    list_display = ('id','monto',)
