from django.conf.urls import include, url
from django.views.decorators.csrf import csrf_exempt
from common.decorators import Token
from .views import *
from .ajax import ListarStock, ListarStockProducto, ListarProductos,\
    ListarStockEncargado, ListarTraslados, ListarStockEncargadoAceptarNuevo,\
    ListarTrasladosSucursales

urlpatterns = [
	url(r'^inventario/registrar$', Token(registrar), name="registrar_inventario"),
	url(r'^inventario/listar$', Token(listar_inventario), name="listar_inventario"),
    url(r'^inventario/detalle/producto/(?P<pk>[0-9\.]+)/$', Token(detalle), name="registrar_inventario"),

	url(r'^ajax/listar_inventario$', csrf_exempt(ListarStock.as_view()), name="listar_inventario_ajax"),
    url(r'^ajax/listar_inventario_encargado$', csrf_exempt(ListarStockEncargado.as_view()),
        name="listar_inventario_encargado_ajax"),
    url(r'^ajax/listar_inventario_encargado_aceptar$', csrf_exempt(ListarStockEncargadoAceptarNuevo.as_view()),
        name="listar_inventario_encargado_aceptar_ajax"),

    url(r'^ajax/listar_traslados_sucursales$', csrf_exempt(ListarTrasladosSucursales.as_view()),
        name="listar_traslados_sucursales_ajax"),

    url(r'^ajax/listar_productos$', csrf_exempt(ListarProductos.as_view()), name="listar_productos_ajax"),
    url(r'^ajax/listar_traslados$', csrf_exempt(ListarTraslados.as_view()), name="listar_traslados_ajax"),    
    url(r'^ajax/listar_inventario/producto/(?P<pk>[0-9\.]+)/$',
        csrf_exempt(ListarStockProducto.as_view()) , name="listar_inventario_producto_ajax"),

]
