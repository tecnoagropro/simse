from django.conf.urls import url, include
from rest_framework import routers
from rest_framework.routers import SimpleRouter
from rest_framework_jwt.views import obtain_jwt_token, verify_jwt_token, refresh_jwt_token
from .views import *
from .vistas import obtain_auth_username, personas_asignar

urlpatterns = [
    url(r'^auth/verify/', verify_jwt_token),
    url(r'^auth/refresh/', refresh_jwt_token),


    url(r'^auth/password_reset_send_email/',
        PasswordResetSendEmail.as_view(), name="password_reset_send_email"),

    url(r'^auth/password_reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        PasswordVerifyToken.as_view(), name="password_reset_verify"),

    url(r'^auth/password_reset/',
        PasswordReset.as_view(), name="password_reset"),

    url(r'^auth/cambio_clave/',
        CambioClave.as_view(), name="cambio_clave"),


    url(r'^auth/logout/',
        UserLogout.as_view(), name="user-logout"),

    url(r'^auth/$', obtain_jwt_token, name="login"),
    #url(r'^auth/$', obtain_auth_username, name="login"),


    # puede actualizar la persona en session u otra persona
    url(r'^users/persona/(?P<pk>\d+)/$', PersonaViewSet.as_view(), name='persona-detail'),

    url(r'^users/update/(?P<pk>\d+)/$',
        UserUpdateViewSet.as_view(), name="user-update"),
    
    url(r'^users/(?P<pk>\d+)/$',
        UserViewSet.as_view(), name="user-detail"),

    url(r'^users/persona/buscar/(?P<tipo>\w+)/(?P<documento>\w+)/$', BuscarPorDocumentoViewSet.as_view()),

    url(r'^users/persona/asociar$', AsociarUsuarioPersonaViewSet.as_view(), name="user_persona"),

    
    url(r'^users/form/$', UserFormChoicesList.as_view(), name="user-form-choices"),

    url(r'^users/validaciones', ValidacionesViewSet.as_view()),

    url(r'^users/avatar/', avatar),

    url(r'^users/$', UserView.as_view(), name="user"),





    ### GET solo trabaja con la persona en session
    url(r'^users/persona/completo', persona_completo),
    url(r'^users/persona/parcial', persona_parcial),

    url(r'^documentos', DocumentChoicesList.as_view()),

    url(r'^users/persona/asignar', personas_asignar),


    url(r'^users/persona/info_roles', InfoRoles.as_view(), name="info-roles"),
    

]
