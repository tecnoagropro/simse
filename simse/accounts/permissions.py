from rest_framework import permissions
from rest_framework.compat import is_authenticated


class IsBusinessPermission(permissions.BasePermission):
    """
    Global permission check for Business User.
    """

    def has_permission(self, request, view):
        if request.method == "GET":
            return True

        return request.user.is_authenticated() and request.user.Persona.hasValidBusiness()


class IsCreatedPermission(permissions.BasePermission):
    """
    Global permission check object created
    """

    def has_object_permission(self, request, view, obj):
        if request.method == "GET":
            return True
        return request.user == obj.created_by


class IsNotAuthenticated(permissions.BasePermission):
    """
    Allows access only to authenticated users.
    """

    def has_permission(self, request, view):
        return not is_authenticated(request.user)