from django.shortcuts import render, redirect
from django.contrib.auth import get_user_model
from .serializers import PasswordVerifyTokenSerializer
import time
from django.http import JsonResponse
from django.contrib.auth import authenticate
User = get_user_model()
from django.views.decorators.csrf import csrf_exempt
from accounts.models import Persona
from encargado.models import Encargado
from dynamic_preferences.registries import global_preferences_registry
global_preferences = global_preferences_registry.manager()

def olvido_de_clave(request,uidb64,token):
    
    context = {
        'uidb64':uidb64,
        'token':token
    }

    serializer = PasswordVerifyTokenSerializer(data=context)
    if not serializer.is_valid():
        context.update({
            'error':True
        })

    return render(request, 'olvido_cambio_clave.html', context)

def bloqueo(request):
    
    context = {}
    return render(request, 'bloqueo.html', context)

def perfil(request):
    
    context = {}
    return render(request, 'perfil.html', context)

def registro(request):
    
    context = {}
    return render(request, 'registro_wizard.html', context)

def olvido(request):
    
    context = {}
    return render(request, 'olvido.html', context)


def login(request):
    
    context = {}
    return render(request, 'login.html', context)

@csrf_exempt
def obtain_auth_username(request,version):

    credenciales = {
        'username':request.POST.get('username'),
        'password':request.POST.get('password'),
    }

    user = authenticate(**credenciales)

    if user:

        res = {
            'token':request.POST.get('username')
        }
        return JsonResponse(res)
    
    return JsonResponse({},status=400)


def asignar_persona(request):
    context = {}
    return render(request, 'asignar_persona.html', context)


def personas_asignar(request,version):

    respuesta_personas = []

    all_personas = Persona.objects.all()

    for per in all_personas:
        try:
            Encargado.objects.get(persona=per)
        except Exception as e:
            respuesta_personas.append({
                'id' : str(per.id),
                'resumen': per.primer_nombre + ' ' + per.primer_apellido
            })

    return JsonResponse(respuesta_personas,safe=False)


def dashboard(request):
    ctx = {}
    return render(request,'base.html', ctx)

def dashboard_ajax(request):
    ctx = {
        'URL_REDIRECCIONAR_ADMIN':'#' + global_preferences['URL_REDIRECCIONAR_ADMIN'],
        'URL_REDIRECCIONAR_ENCARGADO':'#' + global_preferences['URL_REDIRECCIONAR_ENCARGADO']
    }
    return render(request,'dashboard.html', ctx)

    