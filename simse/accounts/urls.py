from django.conf.urls import url, include
from rest_framework import routers
from .vistas import *
from django.contrib.auth.views import LoginView
from django.views.decorators.csrf import csrf_exempt

urlpatterns = [
    url(r'^olvido_de_clave/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        olvido_de_clave, name='olvido_de_clave'),

    url(r'^registro/$', registro, name='registro'),
    url(r'^olvido/$', olvido, name='olvido'),
    url(r'^bloqueo/$', bloqueo, name='bloqueo'),
    url(r'^perfil/$', perfil, name='perfil'),
    url(r'^asignar_persona/$', asignar_persona, name='asignar_persona'),
    url(r'^login/$', login, name='login'),
    url(r'^dashboard/$', dashboard, name='dashboard'),
    url(r'^dashboard_ajax/$', dashboard_ajax, name='dashboard_ajax'),

]
