from datetime import timedelta, date

import pytest
from django.utils import timezone

from ..forms import PasswordResetForm, AuthenticationForm, RegisterUserForm, BirthdayForm, MobileResetPasswordForm, \
    PasswordResetMobileUserForm


# @pytest.fixture
# def user():
#     obj = mixer.blend(settings.AUTH_USER_MODEL, username="test", email="test@test.com")
#     obj.set_password("testing123")
#     obj.save()
#     obj.profile.mobile = "+111111111"
#     obj.profile.generate_mobile_verification_number()
#     obj.profile.save()
#     return obj


@pytest.mark.django_db
class TestPasswordResetForm:
    def test_password_reset_form(self, user, recaptcha):
        form = PasswordResetForm(recaptcha)
        assert form.is_valid() is False
        form = PasswordResetForm(dict({
            "email": user.email,
        }, **recaptcha))
        assert form.is_valid() is True


@pytest.mark.django_db
class TestAuthenticationForm:
    def test_authentication_form_username(self, user):
        form = AuthenticationForm()
        assert form.is_valid() is False
        form = AuthenticationForm(data={"username": user.username, "password": "testing123"})
        assert form.is_valid() is True

    def test_authentication_form_email(self, user):
        form = AuthenticationForm(data={"username": user.email, "password": "testing123"})
        assert form.is_valid() is True

    def test_authentication_form_mobile(self, user):
        form = AuthenticationForm(data={"username": user.profile.mobile, "password": "testing123"})
        assert form.is_valid() is True


@pytest.mark.django_db
class TestRegisterUserForm:
    def test_register_form(self):
        form = RegisterUserForm()
        assert form.is_valid() is False
        form = RegisterUserForm(data={
            "first_name": "Name",
            "last_name": "LastName",
            "email": "test2@test.com",
            "phone": "+111111112",
            "password": "testing123"})

        assert form.is_valid() is True

    def test_register_form_already_registered_email(self, user):
        form = RegisterUserForm(data={
            "first_name": "Name",
            "last_name": "LastName",
            "email": user.email,
            "phone": "+111111112",
            "password": "testing123"})

        assert form.is_valid() is False
        assert "email" in form.errors

    def test_register_form_already_registered_phone(self, user):
        form = RegisterUserForm(data={
            "first_name": "Name",
            "last_name": "LastName",
            "email": "test@test.com",
            "phone": user.profile.mobile,
            "password": "testing123"})

        assert form.is_valid() is False
        assert "phone" in form.errors


@pytest.mark.django_db
class TestMobileResetPasswordForm:
    def test_mobile_reset_form_empty(self):
        form = MobileResetPasswordForm()
        assert form.is_valid() is False

    def test_mobile_reset_form_code_invalid(self, user):
        form = MobileResetPasswordForm({
            "code": "1111111111"
        })
        assert form.is_valid() is False

    def test_mobile_reset_form_valid(self, user):
        user.profile.generate_mobile_verification_number()
        user.profile.mobile_restored_time = timezone.now()
        user.profile.save()
        form = MobileResetPasswordForm({"code": user.profile.mobile_verification_number})

        assert form.is_valid() is True

    def test_mobile_reset_form_invalid_time(self, user):
        user.profile.generate_mobile_verification_number()
        user.profile.mobile_restored_time = timezone.now() - timedelta(days=10)
        user.profile.save()
        form = MobileResetPasswordForm({
            "code": user.profile.mobile_verification_number
        })
        assert form.is_valid() is False
        assert "code" in form.errors


@pytest.mark.django_db
class TestPasswordResetMobileUserForm:
    def test_form_empty(self, recaptcha):
        form = PasswordResetMobileUserForm(recaptcha)
        assert form.is_valid() is False

    def test_form_number_invalid(self, recaptcha):
        form = PasswordResetMobileUserForm(dict({
            "mobile": "+1111"
        }, **recaptcha))
        assert form.is_valid() is False

    def test_form_valid(self, user, recaptcha):
        form = PasswordResetMobileUserForm(dict({
            "mobile": user.profile.mobile
        }, **recaptcha))
        assert form.is_valid() is True


@pytest.mark.django_db
class TestBirthdayForm:
    def test_birthdayform(self):
        form = BirthdayForm()
        assert form.is_valid() is False
        form = BirthdayForm({"birth_date": "07/17/1990"})
        assert form.is_valid()

    def test_birthdayform_invalid_date(self):
        form = BirthdayForm({"birth_date": date.today().strftime("%m/%d/%Y")})
        assert form.is_valid() is False
