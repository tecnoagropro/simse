from datetime import timedelta

import pytest
from django.conf import settings
from django.db.models.base import ModelBase
from django.utils import timezone
from mixer.backend.django import mixer

from accounts import models
from accounts.models import BusinessInformation

from django.contrib.gis.geos import Point

mixer.register(
    'cities.City',
    location=lambda: Point(
        float(mixer.faker.latitude()),
        float(mixer.faker.longitude()),
    ),
)


@pytest.mark.django_db
class TestUser:
    @pytest.fixture
    def data(self):
        user = mixer.blend(settings.AUTH_USER_MODEL, username="test", password="test",
                           first_name="mariano", last_name="Ramirez")
        mixer.blend(models.BusinessInformation, user=user)
        mixer.cycle(5).blend(models.EmergencyContact, user=user, first_name=mixer.RANDOM)
        mixer.cycle(5).blend(models.UserChildren, user=user, first_name=mixer.RANDOM)
        mixer.cycle(5).blend(models.UserPet, user=user, name=mixer.RANDOM)

    def test_models_django(self, data):
        for model in dir(models):
            model_obj = getattr(models, model)
            if isinstance(model_obj, ModelBase):
                if not model_obj._meta.abstract:
                    if len(model_obj.objects.all()) > 0:
                        name = str(model_obj.objects.all()[0])
                        assert type(name) == str
                        # import pdb
                        # pdb.set_trace()

    def test_user_model(self, user):
        # user = mixer.blend(settings.AUTH_USER_MODEL, username="test", password="test")
        assert user.profile is not None, "The User should have profile"
        with pytest.raises(BusinessInformation.DoesNotExist):
            assert user.business, "The User should not have Business Information"

    def test_user_hasValidBusines_model(self, user):
        assert user.profile.hasValidBusiness() is False
        busines = mixer.blend(models.BusinessInformation, user=user)
        assert user.profile.hasValidBusiness() is False
        busines.accepted = True
        busines.save()
        assert user.profile.hasValidBusiness() is True

    def test_profile_str(self, user):
        assert str(user.profile) == user.first_name + " " + user.last_name, "Profile model should " \
                                                                            "return user name and last name"

    def test_profile_verification_number(self, user):
        user.profile.generate_mobile_verification_number()
        assert len(user.profile.mobile_verification_number) == 6

    def test_send_code_again(self, user):
        user.profile.generate_mobile_verification_number()
        user.profile.mobile_verification_time = timezone.now()
        assert user.profile.can_send_code_again() is False
        user.profile.mobile_verification_time = timezone.now() - timedelta(minutes=40)
        assert user.profile.can_send_code_again()

    def test_key_expired(self, user):
        user.profile.generate_mobile_verification_number()
        user.profile.email_confirm_sent = timezone.now()
        assert user.profile.key_expired() is False
        user.profile.email_confirm_sent = timezone.now() - timedelta(days=7, minutes=1)
        assert user.profile.key_expired()
