# import os
# import re
#
# import pytest
# from django.contrib.auth.models import User
# from django.core import mail
# from django.urls import reverse
# from selenium.webdriver.common.by import By
# from selenium.webdriver.support import expected_conditions as EC
# from selenium.webdriver.support.wait import WebDriverWait
#
#
# @pytest.mark.django_db
# class TestUser:
#     # @pytest.fixture
#     # def user(self):
#     #     user = mixer.blend(settings.AUTH_USER_MODEL, username="test", email="yusuf@qawba.com")
#     #     user.set_password("testing123")
#     #     user.profile.mobile = '+584267841766'
#     #     user.save()
#     #     user.profile.save()
#     #     return user
#
#     def test_login(self, selenium, live_server, user):
#         selenium.get(live_server.url + reverse("account:login"))
#
#         username = selenium.find_element_by_id("id_username")
#         password = selenium.find_element_by_id("id_password")
#
#         username.send_keys(user.username)
#         password.send_keys("testing123")
#
#         selenium.find_element(By.XPATH, '//button[text()="Login"]').click()
#         selenium.find_element_by_class_name("page-content")
#         # pytest.set_trace()
#
#     def create(self, selenium, live_server):
#         selenium.get(live_server.url + reverse("account:register"))
#
#         first_name = selenium.find_element_by_id('id_first_name')
#         last_name = selenium.find_element_by_id('id_last_name')
#         email = selenium.find_element_by_id('id_email')
#         phone = selenium.find_element_by_id('id_phone')
#         password = selenium.find_element_by_id('id_password')
#
#         # tnc = selenium.find_element_by_name('tnc').find_element_by_xpath("..")
#
#         # Fill the form with data
#         first_name.send_keys('Yusuf')
#         last_name.send_keys('Unary')
#         email.send_keys('yusuf@qawba.com')
#         phone.send_keys('+584267841766')
#         password.send_keys('testing123')
#
#         # if tnc:
#         #     tnc.click()
#         selenium.execute_script("document.getElementsByName('tnc')[0].checked = true")
#         selenium.execute_script("document.getElementsByName('tnc')[0].setAttribute('checked', 'true')")
#
#         # submitting the form
#         selenium.find_element(By.XPATH, '//button[text()="Submit"]').click()
#         WebDriverWait(selenium, 10).until(EC.presence_of_element_located((By.ID, "id_birth_date")))
#         birth_date = selenium.find_element_by_id("id_birth_date")
#         birth_date.send_keys('07/19/1990')
#         selenium.find_element(By.XPATH, '//button[text()="Submit"]').click()
#         WebDriverWait(selenium, 10).until(EC.presence_of_element_located((By.ID, "id_code")))
#
#     def test_create(self, selenium, live_server):
#         self.create(selenium, live_server)
#
#         user = User.objects.get(email='yusuf@qawba.com')
#         code = selenium.find_element_by_id("id_code")
#
#         code.send_keys(user.profile.mobile_verification_number)
#         selenium.find_element_by_id('validate-submit-btn').click()
#         WebDriverWait(selenium, 10).until(
#             EC.presence_of_element_located((By.CLASS_NAME, "page-content"))
#         )
#         selenium.find_element_by_class_name("page-content")
#
#         # pytest.set_trace()
#
#     def test_create_confirm_email(self, selenium, live_server):
#         self.create(selenium, live_server)
#         assert len(mail.outbox) == 1
#         assert "Welcome" in mail.outbox[2].subject
#
#         urls = re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*(),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+',
#                           mail.outbox[0].body)
#         assert urls
#         selenium.get(urls[0])
#         assert "Email Confirmed" in selenium.page_source
#
#     def test_forgot_password_email(self, selenium, live_server, user):
#         os.environ['RECAPTCHA_TESTING'] = 'True'
#         selenium.get(live_server.url + reverse("account:login"))
#         selenium.find_element(By.XPATH, '//a[text()="Forgot Password?"]').click()
#         WebDriverWait(selenium, 10).until(
#             EC.visibility_of(selenium.find_element(By.XPATH, '//a[text()="via Email"]'))
#         )
#         selenium.find_element(By.XPATH, '//a[text()="via Email"]').click()
#         selenium.find_element_by_id("id_email").send_keys(user.email)
#
#         selenium.execute_script("document.getElementById('g-recaptcha-response').value='PASSED'")
#         # selenium.find_element_by_id("g-recaptcha-response").send_keys("PASSED")
#         selenium.find_element(By.XPATH, '//button[text()="Submit"]').click()
#         WebDriverWait(selenium, 10).until(
#             EC.text_to_be_present_in_element((By.CLASS_NAME, 'content'),
#                                              "We've emailed you instructions for setting")
#         )
#
#         assert len(mail.outbox) == 1
#         assert "Password reset" in mail.outbox[0].subject
#
#         urls = re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*(),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+',
#                           mail.outbox[0].body)
#         assert urls
#         selenium.get(urls[0])
#         selenium.find_element_by_id("id_new_password1").send_keys("Uisdi12309")
#         selenium.find_element_by_id("id_new_password2").send_keys("Uisdi12309")
#         selenium.find_element(By.XPATH, '//button[text()="Submit"]').click()
#
#         WebDriverWait(selenium, 10).until(
#             EC.text_to_be_present_in_element((By.CLASS_NAME, 'content'),
#                                              "Your password has been set")
#         )
#
#     def test_forgot_password_email_invalid(self, selenium, live_server):
#         os.environ['RECAPTCHA_TESTING'] = 'True'
#         selenium.get(live_server.url + reverse("account:login"))
#         selenium.find_element(By.XPATH, '//a[text()="Forgot Password?"]').click()
#         WebDriverWait(selenium, 10).until(
#             EC.visibility_of(selenium.find_element(By.XPATH, '//a[text()="via Email"]'))
#         )
#         selenium.find_element(By.XPATH, '//a[text()="via Email"]').click()
#         selenium.find_element_by_id("id_email").send_keys("no-exists@test.com")
#
#         selenium.execute_script("document.getElementById('g-recaptcha-response').value='PASSED'")
#         # selenium.find_element_by_id("g-recaptcha-response").send_keys("PASSED")
#         selenium.find_element(By.XPATH, '//button[text()="Submit"]').click()
#         WebDriverWait(selenium, 10).until(
#             EC.text_to_be_present_in_element((By.CLASS_NAME, 'content'),
#                                              "We've emailed you instructions for setting")
#         )
#         assert len(mail.outbox) == 1
#         assert "Create a" in mail.outbox[0].subject
#
#     def test_forgot_password_sms(self, selenium, live_server, user):
#         os.environ['RECAPTCHA_TESTING'] = 'True'
#         selenium.get(live_server.url + reverse("account:login"))
#         selenium.find_element(By.XPATH, '//a[text()="Forgot Password?"]').click()
#         WebDriverWait(selenium, 10).until(
#             EC.visibility_of(selenium.find_element(By.XPATH, '//a[text()="via Phone"]'))
#         )
#         selenium.find_element(By.XPATH, '//a[text()="via Phone"]').click()
#         selenium.find_element_by_id("id_mobile").send_keys(user.profile.mobile)
#         selenium.execute_script("document.getElementById('g-recaptcha-response').value='PASSED'")
#         selenium.find_element(By.XPATH, '//button[text()="Submit"]').click()
#
#         WebDriverWait(selenium, 10).until(
#             EC.presence_of_element_located((By.ID, "id_code"))
#         )
#         user.profile.refresh_from_db()
#         # pytest.set_trace()
#         selenium.find_element_by_id("id_code") \
#             .send_keys(user.profile.mobile_verification_number)
#         selenium.find_element(By.XPATH, '//button[text()="Verify Phone"]').click()
#
#         password = "prueba1234"
#         selenium.find_element_by_id("id_new_password1") \
#             .send_keys(password)
#         selenium.find_element_by_id("id_new_password2") \
#             .send_keys(password)
#         selenium.find_element(By.XPATH, '//button[text()="Submit"]').click()
#         WebDriverWait(selenium, 10).until(
#             EC.text_to_be_present_in_element((By.CLASS_NAME, 'content'),
#                                              "Your password has been set")
#         )
#         selenium.find_element(By.XPATH, '//a[text()="Log in"]').click()
#
#         # Log in Test with new password
#         WebDriverWait(selenium, 10).until(
#             EC.presence_of_element_located((By.ID, "id_username"))
#         )
#         selenium.find_element_by_id("id_username"). \
#             send_keys(user.username)
#         selenium.find_element_by_id("id_password"). \
#             send_keys(password)
#
#         selenium.find_element(By.XPATH, '//button[text()="Login"]').click()
#         selenium.find_element_by_class_name("page-content")
#
#         # pytest.set_trace()
#
#         # def test_forgot_password_sms(self, selenium, live_server, user):
#         #     assert True
