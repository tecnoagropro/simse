from django.contrib import admin

from .models import Persona

class PersonaAdmin(admin.ModelAdmin):

    list_display = ('nombre','apellido','tipo',
                    'documento','monto_cuenta','user')

    def nombre(self,obj):
        return obj.primer_nombre

    def apellido(self,obj):
        return obj.primer_apellido

    def tipo(self,obj):
        return obj.tipo_documento

    def documento(self,obj):
        return obj.numero_documento

    def user(self,obj):
        return obj.user or None
           
    def monto_cuenta(self, obj):
        return obj.cuenta_set.get().monto

admin.site.register(Persona,PersonaAdmin)