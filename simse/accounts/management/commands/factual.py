import requests
from django.core.management.base import BaseCommand, CommandError
from ...models import BusinessCategory


class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def handle(self, *args, **options):
        r = requests.get('https://api.factual.com/categories?KEY=QUhPob7PBrBhfc2cU9QkNvebNBgXAazGFvLYsE2q')
        if r.status_code == 200:
            data = r.json()["response"]["data"][0]["children"]
            for obj in data:
                cat, is_created = BusinessCategory.objects.get_or_create(id=obj["id"], name=obj["label"])
                for childobj in obj["children"]:
                    childcat, is_created = BusinessCategory.objects.get_or_create(id=childobj["id"], name=childobj["label"], parent=cat)

        else:
            print("Error")
