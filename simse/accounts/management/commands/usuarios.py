from django.contrib.auth.management.commands import createsuperuser
from django.core.management import CommandError
from django.core.management import call_command
from django.contrib.auth import get_user_model
from accounts.models import Persona


class Command(createsuperuser.Command):
    help = 'Create a superuser with a password non-interactively'

    def set_user(self, **kwargs):

        username = kwargs.get('username', None)
        primer_nombre = kwargs.get('primer_nombre', None)
        primer_apellido = kwargs.get('primer_apellido', None)
        email = kwargs.get('email', None)
        password = kwargs.get('password', None)
        username = kwargs.get('username', None)

        try:
            get_user_model().objects.get(username=username)
        except get_user_model().DoesNotExist as e:
            u = self.UserModel()
            u.username = username
            u.email = email
            u.is_staff = True
            u.is_superuser = True
            u.set_password(password)
            u.save()

            p = Persona()
            p.user = u
            p.primer_nombre = primer_nombre
            p.primer_apellido = primer_apellido
            p.save()

    def handle(self, *args, **options):

        self.set_user(**{
            'username':'sysadmin',
            'password':'sysadmin',
            'primer_nombre':'administrador',
            'primer_apellido':'sistema',
            'email':'tecnoagropro@gmail.com'
            })

        self.set_user(**{
            'username':'rosmerl',
            'password':'dracula23',
            'primer_nombre':'Rosmerl',
            'primer_apellido':'Barrera',
            'email':'rosmerlbarrera@mail.com'
            })



        if options.get('verbosity', 0) >= 1:
          self.stdout.write("Usuarios creados")