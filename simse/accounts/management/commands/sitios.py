"""
Extend createsuperuser command to allow non-interactive creation of a
superuser with a password.
Instructions:
  mkdir -p path-to-your-app/management/commands/
  touch path-to-your-app/management/__init__.py
  touch path-to-your-app/management/commands/__init__.py
and place this file under path-to-your-app/management/commands/
Example usage:
  manage.py create-superuser \
          --username foo     \
          --password foo     \
          --email foo@foo.foo
"""
from django.contrib.auth.management.commands import createsuperuser
from django.core.management import CommandError
from django.core.management import call_command
from sitehub.locations.models import Site

class Command(createsuperuser.Command):
    help = 'Create a superuser with a password non-interactively'


    def handle(self, *args, **options):

        if Site.objects.all().count() == 1:
            lista = [str(x) for x in range(1,20)]
            site = Site.objects.last()

            for n in lista:
                s = site
                s.pk = site.pk + 1
                s.code = 'code-%s' % str(n)
                s.name = 'name-%s' % str(n)
                s.save()

            print ('listo')