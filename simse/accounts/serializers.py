from datetime import date
from random import random

from django.conf import settings
from django.contrib.auth import get_user_model, \
    password_validation, authenticate, logout
from django.contrib.auth.forms import SetPasswordForm
from django.contrib.auth.tokens import default_token_generator
from django.contrib.sites.shortcuts import get_current_site
from django.core import exceptions
from django.db import IntegrityError
from django.db.models import OneToOneField
from django.shortcuts import get_object_or_404
from django.utils import timezone
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode
from django.utils.text import slugify
from django.utils.translation import ugettext as _
from django.core.mail import send_mail
from django.core.exceptions import ValidationError as DjangoValidationError

from rest_framework import serializers
from rest_framework.compat import set_many
from rest_framework.exceptions import NotFound
from rest_framework.fields import SerializerMethodField
from rest_framework.serializers import HyperlinkedModelSerializer, raise_errors_on_nested_writes
from rest_framework.utils import model_meta
from rest_framework.validators import UniqueValidator
from rest_framework_jwt.settings import api_settings

from rest_framework.fields import get_error_detail

# avatar
try:
    from cStringIO import StringIO
except ImportError:
    from io import StringIO
try:
    from PIL import Image
except ImportError:
    import Image
from django.core.files import File
AVATAR_CROP_MAX_SIZE = getattr(settings, 'AVATAR_CROP_MAX_SIZE', 450)
AVATAR_CROP_MIN_SIZE = getattr(settings, 'AVATAR_CROP_MIN_SIZE', 49)
from .models import user_directory_path, Persona
import os
# avatar


from accounts.forms import phone_regex

from common.fields import ParameterisedHyperlinkedIdentityField
from common.serializers import QueryFieldsMixin

from encargado.models import Encargado 
#from empleado.models import Empleado
from reporte.models import Inversionista

User = get_user_model()


class CreateUserSerializer(serializers.Serializer):

    date_joined = serializers.DateTimeField(
        read_only=True, default=timezone.now)
    username = serializers.CharField(required=True)
    email = serializers.EmailField(
        validators=[UniqueValidator(queryset=User.objects.all())])
    password = serializers.CharField(write_only=True)

    first_name = serializers.CharField(max_length=30, required=False)
    last_name = serializers.CharField(max_length=150, required=False)
    id = serializers.IntegerField(read_only=True)
    roles = SerializerMethodField(read_only=True)
    encargado_id = SerializerMethodField(read_only=True)
    user_fields = ["email"]

    def get_encargado_id(self, obj):
        user = self.context['request'].user
        try:
            per = Persona.objects.get(user=user)
            qs = Encargado.objects.filter(persona=per)
            if qs.exists():
                return qs[0].id
        except Exception:
            pass
        return None

    def get_roles(self, obj):
        user = self.context['request'].user
        roles = []

        try:
            per = Persona.objects.get(user=user)

            if user.is_superuser:
                roles.append('administrador')

            if Encargado.objects.filter(persona=per).exists():
                roles.append('encargado')

            if Inversionista.objects.filter(persona=per).exists():
                roles.append('inversionista')

        except (Persona.DoesNotExist, TypeError) as e:
            if user.is_superuser:
                roles.append('administrador')  

        
        return roles

    def validate(self, attrs):
        # here data has all the fields which have validated values
        # so we can create a User instance out of it
        user_data = {your_key: attrs[your_key]
                     for your_key in self.user_fields}

        user = User(**user_data)

        # get the password from the data
        password = attrs.get('password')

        errors = dict()
        try:
            # validate the password and catch the exception
            password_validation.validate_password(password=password, user=user)

        # the exception raised here is different than
        # serializers.ValidationError
        except exceptions.ValidationError as e:
            errors['password'] = list(e.messages)

        if errors:
            raise serializers.ValidationError(errors)

        return super(CreateUserSerializer, self).validate(attrs)

    def create(self, validated_data):

        user_data = {your_key: validated_data[
            your_key] for your_key in self.user_fields}

        user = User(**user_data)
        user.username = validated_data["username"]

        if User.objects.filter(username=user.username).exists():
            user.username = slugify(
                "{}-{}".format(validated_data["username"], int(random() * 1000)))

        user.set_password(validated_data['password'])
        user.save()

        user = authenticate(username=user.username,
                            password=validated_data['password'])

        return user


class UpdateUserSerializer(QueryFieldsMixin, HyperlinkedModelSerializer):

    username = serializers.CharField(required=False)
    email = serializers.EmailField(
        validators=[UniqueValidator(queryset=get_user_model().objects.all())],
        required=False)

    def update(self, instance, validated_data):

        if validated_data.get('username', None):
            instance.username = validated_data.get('username')

        if validated_data.get('email', None):
            instance.email = validated_data.get('email')

        instance.save()
        return instance

    def validate(self, attrs):

        errores = []
        try:
            User.objects.get(username=username)
            errores.append('El nombre de usuario ya existe')
        except Exception as e:
            pass

        try:
            User.objects.get(email=email)
            errores.append('El correo ya existe')
        except Exception as e:
            pass

        if errores:
            raise serializers.ValidationError(errores)

        return super(UpdateUserSerializer, self).validate(attrs)

    class Meta:
        model = User
        fields = ('username', 'email',)


class UserDetailSerializer(QueryFieldsMixin, HyperlinkedModelSerializer):

    persona = serializers.IntegerField(source="persona_set.id")

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'persona', 'email')


class UserLogoutSerializer(serializers.Serializer):

    def create(self, validated_data):
        logout(self.request)


class PasswordResetSendEmailSerializer(serializers.Serializer):
    email = serializers.EmailField()

    def validate(self, attrs):
        email = attrs.get('email')
        errors = dict()

        try:
            user = User.objects.get(
                email__iexact=email,
                is_active=True)

            attrs['user'] = user
        except User.DoesNotExist:
            errors['email'] = _('El correo no existe.')

        if errors:
            raise serializers.ValidationError(errors)

        return super(PasswordResetSendEmailSerializer, self).validate(attrs)

    def get_context_email(self, request, validated_data):
        email = validated_data.get('email')
        user = validated_data.get('user')
        current_site = get_current_site(request)
        site_name = current_site.name
        token = default_token_generator.make_token(user)
        domain = current_site.domain
        uid = urlsafe_base64_encode(force_bytes(user.pk)).decode(),
        use_https = request.is_secure()
        reset_url = settings.PASSWORD_RESET_VERIFY_URL % (str(uid[0]), token)
        context = {
            'email': email,
            'domain': domain,
            'site_name': site_name,
            'reset_url': reset_url,
            'user': user,
            'protocol': 'https' if use_https else 'http',
        }
        return context

    def create(self, validated_data):
        subject_template_name = 'accounts/password_reset_subject.txt',
        template_name_html = 'accounts/password_reset_email.html',
        template_name_txt = 'accounts/password_reset_email.txt',
        email = validated_data.get('email', None)
        from django.template import loader
        context = self.get_context_email(
            self.context["request"], validated_data)
        subject = loader.render_to_string(subject_template_name, context)
        # # Email subject *must not* contain newlines
        #subject = ''.join(subject[0].splitlines())
        body = loader.render_to_string(template_name_txt, context)
        body_html = loader.render_to_string(template_name_html, context)

        send_mail(subject, body, 'jovan@mail.com', [email],
                  fail_silently=False, auth_user=None, auth_password=None,
                  connection=None, html_message=body_html)

        return validated_data


class PasswordVerifyTokenSerializer(serializers.Serializer):
    token = serializers.CharField()
    uidb64 = serializers.CharField()

    def validate(self, attrs):

        token = attrs.get('token')
        uidb64 = attrs.get('uidb64')
        errors = dict()

        user = self.get_user(uidb64)
        if user is not None:
            if not self.verify_token(user, token):
                errors['token'] = _('token not verify.')
        else:
            errors['uidb64'] = _('Not user get.')

        if errors:
            raise serializers.ValidationError(errors)

        return super(PasswordVerifyTokenSerializer, self).validate(attrs)

    def get_user(self, uidb64):
        try:
            uid = urlsafe_base64_decode(uidb64).decode()
            user = User._default_manager.get(pk=uid)
        except (TypeError, ValueError, OverflowError, User.DoesNotExist):
            user = None
        return user

    def verify_token(self, user, token):
        if default_token_generator.check_token(user, token):
            return True
        else:
            return False

    def create(self, validated_data):
        return validated_data


class PasswordResetSerializer(serializers.Serializer):
    new_password1 = serializers.CharField()
    new_password2 = serializers.CharField()
    uidb64 = serializers.CharField()

    def get_user(self, uidb64):
        try:
            # urlsafe_base64_decode() decodes to bytestring
            uid = urlsafe_base64_decode(uidb64).decode()
            user = User._default_manager.get(pk=uid)
        except (TypeError, ValueError, OverflowError, User.DoesNotExist):
            user = None
        return user

    def validate(self, attrs):
        uidb64 = attrs.get('uidb64')
        new_password1 = attrs.get('new_password1')
        new_password2 = attrs.get('new_password2')
        errors = dict()

        if new_password1 is None:
            errors['new_password1'] = _('new_password1 is required.')

        if new_password2 is None:
            errors['new_password2'] = _('new_password2 is required.')

        user = None
        try:
            user = self.get_user(uidb64)
        except IndexError:
            errors['uidb64'] = _('token invalid format.')

        form_data = {
            'new_password1': new_password1,
            'new_password2': new_password2,
        }
        if user:
            form = SetPasswordForm(user, data=form_data)

            if form.errors:
                errors.update(form.errors)
            attrs['form'] = form

        if errors:
            raise serializers.ValidationError(errors)

        return super(PasswordResetSerializer, self).validate(attrs)

    def create(self, validated_data):
        form = validated_data.get('form')
        form.save()
        return validated_data


class CambioClaveSerializer(serializers.Serializer):
    new_password1 = serializers.CharField()
    new_password2 = serializers.CharField()
    password = serializers.CharField()

    def validate(self, attrs):
        new_password1 = attrs.get('new_password1')
        new_password2 = attrs.get('new_password2')
        password = attrs.get('password')
        errors = dict()

        if password is None:
            errors['password'] = _('new_password1 is required.')

        if new_password1 is None:
            errors['new_password1'] = _('new_password1 is required.')

        if new_password2 is None:
            errors['new_password2'] = _('new_password2 is required.')

        user = None
        try:
            user = self.context['request'].user
        except IndexError:
            errors['uidb64'] = _('token invalid format.')

        form_data = {
            'new_password1': new_password1,
            'new_password2': new_password2,
        }
        if user:
            form = SetPasswordForm(user, data=form_data)

            if form.errors:
                errors.update(form.errors)
            attrs['form'] = form

            clave_actual_session = authenticate(
                username=user.username,
                password=attrs['password']
            )

            if not isinstance(clave_actual_session, User):
                errors['password'] = _('password incorrepto.')

        if errors:
            raise serializers.ValidationError(errors)

        return super(CambioClaveSerializer, self).validate(attrs)

    def create(self, validated_data):
        form = validated_data.get('form')
        form.save()
        return validated_data


class BasePersonaSerializer(QueryFieldsMixin, HyperlinkedModelSerializer):

    mobile = serializers.CharField(max_length=20,
                                   validators=[phone_regex,
                                               UniqueValidator(queryset=Persona.objects.all())],
                                   required=False)
    avatar = serializers.CharField(required=False)
    birth_date = serializers.DateField(required=False, format="%d/%m/%Y")
    tipo_documento = serializers.CharField(max_length=30, required=False)
    numero_documento = serializers.CharField(max_length=30, required=False)
    email = serializers.EmailField(source="user.email",
                                   validators=[UniqueValidator(queryset=get_user_model().objects.all())],
                                   required=False)
    primer_nombre = serializers.CharField(max_length=30, required=True)
    primer_apellido = serializers.CharField(max_length=150, required=True)
    segundo_nombre = serializers.CharField(max_length=150, required=False)
    segundo_apellido = serializers.CharField(max_length=150, required=False)
    direccion = serializers.CharField(max_length=200, required=False)

    crear_usuario = False


    def validate(self, attrs):
        tipo = attrs.get('tipo_documento')
        nro = attrs.get('numero_documento')
        errors = dict()

        try:
            qs = Persona.objects.get(tipo_documento=tipo,
                                     numero_documento=nro)
            errors['numero_documento'] = _('El documento ya existe')
        except Exception as e:
            pass

        if errors:
            raise serializers.ValidationError(errors)

        return super(BasePersonaSerializer, self).validate(attrs)

    def create_user(self, validated_data):

        username = validated_data["user"].get("username", None)
        email = validated_data["user"]["email"]
        password = validated_data["user"].get("password", None)

        user_data = {
            "email": email,
            "username": username
        }

        # if not username:
        #     username = "{}-{}".format(first_name, last_name)
        user = get_user_model()(**user_data)
        # user.username = slugify(
        #     "{}".format(username))

        if get_user_model().objects.filter(username=user.username).exists():
            user.username = slugify(
                "{}-{}".format(username, int(random() * 1000)))

        if password:
            user.set_password(password)

        user.save()
        return user

    def create_persona(self, validated_data, user=None):
        mobile = validated_data.get("mobile", None)
        birth_date = validated_data.get("birth_date", None)
        tipo_documento = validated_data.get("tipo_documento", None)
        numero_documento = validated_data.get("numero_documento", None)
        primer_nombre = validated_data.get("primer_nombre", None)
        primer_apellido = validated_data.get("primer_apellido", None)
        gender = validated_data.get("gender", None)

        persona = Persona()
        if mobile:
            persona.mobile = mobile
        if birth_date:
            persona.birth_date = birth_date

        persona.tipo_documento = tipo_documento
        persona.numero_documento = numero_documento

        if user:
            persona.user = user
        persona.gender = gender
        persona.primer_nombre = primer_nombre
        persona.primer_apellido = primer_apellido

        if validated_data.get("segundo_nombre", None):
            persona.segundo_nombre = validated_data.get("segundo_nombre")

        if validated_data.get("segundo_apellido", None):
            persona.segundo_apellido = validated_data.get("segundo_apellido")
        # user.persona.send_email(get_current_site(self.context["request"]).domain,
        #                         'https' if self.context["request"].is_secure() else "http")

        persona.save()
        return persona

    def create(self, validated_data):

        usuario = None
        if self.crear_usuario:
            try:
                usuario = self.create_user(validated_data)
            except Exception as e:
                raise e

        retorno = self.create_persona(validated_data, usuario)
        return retorno


class PersonaALLSerializer(BasePersonaSerializer):

    username = serializers.CharField(source="user.username", required=True)
    password = serializers.CharField(source="user.password", write_only=True)

    def validate(self, attrs):
        password = attrs.get('user')['password']
        user_data = {
            "email": attrs['user']["email"]
        }
        user = get_user_model()(**user_data)
        try:
            password_validation.validate_password(password=password, user=user)
        except exceptions.ValidationError as e:
            errors = dict()
            errors['password'] = list(e.messages)
            raise serializers.ValidationError(errors)

        self.crear_usuario = True
        return super(PersonaALLSerializer, self).validate(attrs)

    class Meta:
        model = Persona
        fields = (
            'mobile', 'birth_date', 'gender', 'primer_nombre', 'segundo_nombre',
            'primer_apellido', 'segundo_apellido', 'username', 'avatar', 'email', 'password',
            'tipo_documento', 'numero_documento', 'id')


class PersonaParcialSerializer(BasePersonaSerializer):

    class Meta:
        model = Persona
        fields = (
            'mobile', 'birth_date', 'gender', 'primer_nombre', 'primer_apellido',
            'tipo_documento', 'numero_documento', 'email', 'direccion', 'id')




class AvatarCreateSerializer(QueryFieldsMixin, HyperlinkedModelSerializer):

    x1 = serializers.IntegerField(required=True, write_only=True)
    x2 = serializers.IntegerField(required=True, write_only=True)
    y1 = serializers.IntegerField(required=True, write_only=True)
    y2 = serializers.IntegerField(required=True, write_only=True)

    def create(self, validated_data):

        persona = self.context["request"].user.persona_set
        persona.avatar = validated_data.get('avatar')
        persona.save()
        image = Image.open(persona.avatar)

        left = validated_data.get('x1')
        top = validated_data.get('y1')
        bottom = validated_data.get('y2')
        right = validated_data.get('x2')

        # print (validated_data)
        # box = [ left, top, right, bottom ]
        # print (box)
        # result = 'width'
        # (w, h) = image.size
        # if result=="width":
        #     box = map(lambda x: x*h/AVATAR_CROP_MAX_SIZE, box)
        # else:
        #     box = map(lambda x: x*w/AVATAR_CROP_MAX_SIZE, box)

        box = [left, top, right, bottom]
        image = image.crop(box)
        if image.mode != 'RGB':
            image = image.convert('RGB')

        new_location = "media/" + \
            user_directory_path(persona, 'avatar_tmp.jpg')
        image.save(new_location)
        image.close()
        # si se descomenta borra el archivo original
        # persona.avatar.delete()
        persona.avatar.save('avatar.jpg', open(new_location, 'rb'))
        os.remove(new_location)
        return validated_data

    class Meta:
        model = Persona
        fields = ('avatar', 'x1', 'x2', 'y1', 'y2')


class ValidacionesSerializer(serializers.Serializer):

    password = serializers.CharField(required=False)
    username = serializers.CharField(required=False)
    email = serializers.EmailField(required=False)

    def validate(self, attrs):

        self.errores = []
        password = attrs.get('password', None)
        username = attrs.get('username', None)
        email = attrs.get('email', None)

        if not password:
            self.errores.append('La clave es obligatoria')

        if not username:
            self.errores.append('El nombre de usuario es obligatorio')

        if not email:
            self.errores.append('El email es obligatorio')

        try:
            User.objects.get(username=username)
            self.errores.append('El nombre de usuario ya existe')
        except Exception as e:
            pass

        try:
            User.objects.get(email=email)
            self.errores.append('El correo ya existe')
        except Exception as e:
            pass

        data_user = {
            'username': username,
            'email': email,
            'first_name': username,
            'last_name': username
        }
        u = User(**data_user)

        try:
            password_validation.validate_password(password=password, user=u)
        except exceptions.ValidationError as e:
            for err in e.messages:
                self.errores.append(err)

        if self.errores:
            raise serializers.ValidationError(self.errores)
        return super(ValidacionesSerializer, self).validate(attrs)

    def create(self, validated_data):
        return validated_data


class UpdatePersona(QueryFieldsMixin, HyperlinkedModelSerializer):

    mobile = serializers.CharField(max_length=20, required=False)
    birth_date = serializers.DateField(required=False, format="%d/%m/%Y")
    primer_nombre = serializers.CharField(max_length=30, required=False)
    primer_apellido = serializers.CharField(max_length=150, required=False)
    segundo_nombre = serializers.CharField(max_length=150, required=False)
    segundo_apellido = serializers.CharField(max_length=150, required=False)
    direccion = serializers.CharField(max_length=200, required=False)
    tipo_documento = serializers.CharField(max_length=30, required=False)
    numero_documento = serializers.CharField(max_length=30, required=False)
    gender = serializers.IntegerField(required=False, write_only=True)
    user = serializers.IntegerField(required=False, write_only=True)

    def validate_user(self, id):
        user= get_object_or_404(User, pk=id)
        return user

    def validate_gender(self, id):
        if id == 0:
            return False
        return True

    class Meta:
        model = Persona
        fields = (
            'id', 'mobile', "primer_nombre", "segundo_nombre", 'gender',
            'primer_apellido', 'segundo_apellido', 'birth_date','user',
             'direccion', 'tipo_documento', 'numero_documento')


class BuscarPorDocumentoSerializer(serializers.Serializer):


    tipo = serializers.CharField(max_length=30)
    documento = serializers.CharField(max_length=30)

    def create(self, validated_data):
        tipo = validated_data.get('tipo', None)
        num = validated_data.get('documento', None)
        persona = get_object_or_404(tipo_documento=tipo, numero_documento=num)
        
        return persona

class AsociarUsuarioPersonaSerializer(serializers.Serializer):

    user = serializers.HiddenField(default=serializers.CurrentUserDefault())
    persona = serializers.IntegerField(write_only=True)

    def validate(self, attrs):

        errores = {}
        persona = attrs.get('persona', None)

        try:
            Persona.objects.get(id=persona)
        except Exception as e:
            print (e)
            errores['persona'] = 'No existe la persona'

        if errores:
            raise serializers.ValidationError(errores)
        return super(AsociarUsuarioPersonaSerializer, self).validate(attrs)   

    def create(self, validated_data):
        
        user = validated_data.get('user')
        id = validated_data.pop('persona')

        persona = Persona.objects.get(pk=id)
        persona.user = user
        persona.save()

        return validated_data


     


class InfoRolesSerializer(serializers.Serializer):

    roles = SerializerMethodField(read_only=True)

    def get_roles(self, obj):
        user = self.context['request'].user
        roles = {}

        try:
            per = Persona.objects.get(user=user)

            if user.is_superuser:
                roles.append('administrador')

            if Encargado.objects.filter(persona=per).exists():
                roles.append('encargado')

            if Inversionista.objects.filter(persona=per).exists():
                roles.append('inversionista')

        except (Persona.DoesNotExist, TypeError) as e:
            if user.is_superuser:
                roles.append('administrador')  

        
        return roles        