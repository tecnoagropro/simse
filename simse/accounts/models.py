import random
from datetime import timedelta

from django.conf import settings
from django.contrib.auth import get_user_model

from django.contrib.auth.validators import UnicodeUsernameValidator
from django.core.exceptions import ValidationError
from django.core.validators import MinLengthValidator
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from common.fields import PaymentIdField
from common.models import UserOneBaseModel, BaseModel, Pais
from common.utils import send_mail, generate_random_token

GENDER_CHOICES = ((True, _('Male')), (False, _('Female')))

BLOOD_TYPE_CHOICES = (
    ("A+", "A+"),
    ("A-", "A-"),
    ("B+", "B+"),
    ("B-", "B-"),
    ("AB+", "AB+"),
    ("AB-", "AB-"),
    ("O+", "0+"),
    ("O-", "O-"),
)
TIP_TYPE_CHOICES = (
    ("percentage", "Percentage"),
    ("amount", "Amount"),
)


DOCUMENT_CHOICES = (
    ("cedula", _('Cedula')),
)

DOCUMENT_CHOICES_PRIVATES = (
    ("dni", _('Documento nacional de identificacion')),
    ("cedula", _('Cedula')),
    ("ric", _('Registro de identidad civil')),
    ("old_dni", _('OLD Documento nacional de identificacion')),
    ("old_cedula", _('OLD Cedula')),
    ("old_ric", _('OLD Registro de identidad civil')),    
)


def user_directory_path(instance, filename):
    return 'user_{0}/{1}'.format(instance.user.id, filename)


class Persona(UserOneBaseModel):
    
    birth_date = models.DateField(null=True, blank=True)
    mobile = models.CharField(_("Mobile"), max_length=20)
    mobile_validated = models.BooleanField(default=False)
    gender = models.NullBooleanField(_("Gender"), choices=GENDER_CHOICES,
                                     default=None)
    mailing_address = models.TextField(_("Mailing address"), blank=True)
    avatar = models.ImageField(_("Avatar"), blank=True, null=True,
                               upload_to=user_directory_path)

    user = models.OneToOneField(settings.AUTH_USER_MODEL, blank=True,
                                null=True,  on_delete=models.CASCADE,
                                related_name="persona_set")

    tipo_documento = models.CharField(_("Tipo de documento"), null=True, blank=True,
                                      max_length=10, choices=DOCUMENT_CHOICES)
    numero_documento = models.CharField(_("Numero de documento"), null=True, blank=True,
                                        max_length=30)
    primer_nombre = models.CharField(_("Nombre"), max_length=30, null=False)
    segundo_nombre = models.CharField(_("segundo Nombre"), max_length=30, null=True, blank=True)
    primer_apellido = models.CharField(_("Apellido"), max_length=100, null=False)
    segundo_apellido = models.CharField(_("segundo Apellido"), max_length=100, null=True, blank=True)

    direccion = models.CharField(_("Direccion"), max_length=200, null=True, blank=True)

    def name(self):
        return self.primer_nombre + ' ' + self.primer_apellido
        
    def __str__(self):
        return "{} {}".format(self.user, self.numero_documento)

    def send_email(self, domain, protocol):
        self.email_confirm_key = generate_random_token(self.user.email)
        self.email_confirm_sent = timezone.now()

        context = {
            'email': self.user.email,
            'domain': domain,
            'site_name': settings.SITE_NAME,
            'user': self.user,
            'protocol': protocol,
            'key': self.email_confirm_key,
            'confirm_url': settings.CONFIRM_EMAIL_URL % self.email_confirm_key
        }
        from_email = None
        send_mail('accounts/register_user_subject.txt', "accounts/register_user_email.txt", context, from_email,
                  self.user.email, html_email_template_name="accounts/register_user_email.html")

    class Meta:
        verbose_name = _('Persona')
        verbose_name_plural = _('Personas')
        ordering = ('id', )
