import jwt
from django.contrib.auth import get_user_model
from django.db.models import Q
from django.utils.translation import ugettext as _
from rest_framework import generics, status
from rest_framework import viewsets
from rest_framework.decorators import detail_route
from rest_framework.exceptions import NotFound
from rest_framework.generics import RetrieveAPIView, CreateAPIView, \
    UpdateAPIView, ListAPIView, get_object_or_404
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework.parsers import FileUploadParser,MultiPartParser

from common.mixins import BaseAPIView, BasePostAPIView, PutSerializerProcessViewMixin, MultipleChoicesAPIView, MultipleChoicesViewSet
from .permissions import IsNotAuthenticated
from common.pagination import LargeResultsSetPagination

from common.serializers import PermisoSerializer
from .serializers import *
from .models import GENDER_CHOICES, TIP_TYPE_CHOICES, BLOOD_TYPE_CHOICES, Persona, DOCUMENT_CHOICES
from rest_framework_jwt.utils import jwt_decode_handler

User = get_user_model()

class UserView(RetrieveAPIView, CreateAPIView, BaseAPIView):
    """
    get:
        regresa el usuario de session

    post:
        crea un nuevo usuario en el sistema

    """
    serializer_class = CreateUserSerializer
    permission_classes = (AllowAny,)

    # def get_permissions(self):
    #     if self.request.method.lower() == "post":
    #         return [IsNotAuthenticated()]
    #     return super(UserView, self).get_permissions()

    def get_object(self):
        return self.request.user


class UserViewSet(RetrieveAPIView, BaseAPIView):
    """
    get:
    return the user

    """
    serializer_class = UserDetailSerializer
    queryset = User.objects.all()

    def get_permissions(self):
        if self.request.method.lower() == "post":
            return [AllowAny()]
        return super(UserViewSet, self).get_permissions()




class UserFormChoicesList(MultipleChoicesAPIView):
    """
    List all choices for users form fields. gender, blood_type, tip_type
    """
    choices_response = {"gender": GENDER_CHOICES, "blood_type": BLOOD_TYPE_CHOICES, "tip_type": TIP_TYPE_CHOICES}




class UserLogout(BaseAPIView):
    """
    post:
    Closes a user session

    """
    serializer_class = UserLogoutSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response({"success": True})


class PasswordResetSendEmail(BaseAPIView):
    """
    post:
    Receive the email of a user and send a url to reset the password.

    """
    serializer_class = PasswordResetSendEmailSerializer

    def get_permissions(self):
        return [AllowAny()]

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response({"success": True})


class PasswordVerifyToken(BaseAPIView):
    """
    get:
    Check the token and uidb64. Returns success in case of success,
    otherwise the list of errors.

    """
    serializer_class = PasswordVerifyTokenSerializer

    def get_permissions(self):
        return [AllowAny()]

    def get(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=kwargs)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response({"success": True})


class PasswordReset(BaseAPIView):
    """
    post:
    Change the password of a user on the system, receive the uidb64,
    the new password and the confirmation of the new password. 
    Return success in case of change of key, otherwise a list of errors.

    """
    serializer_class = PasswordResetSerializer

    def get_permissions(self):
        return [AllowAny()]

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response({"success": True})


class CambioClave(BaseAPIView):
    """
    post:
    Cambia la clave de un usuario logeado, recibe la clave antual
    la nueva clave y la verificacion de la clave.

    """
    serializer_class = CambioClaveSerializer
    permission_classes = (IsAuthenticated,)
    
    def get_permissions(self):
        return [AllowAny()]

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response({"success": True})


class BasePersonaViewSet(CreateAPIView,RetrieveAPIView,UpdateAPIView,BaseAPIView):
    """
    get:
        Api para recibir y actualizar datos del modelo persona.

    """
    serializer_class = PersonaALLSerializer
    permission_classes = (AllowAny,)

    def get_object(self):
        return Persona.objects.get(user=self.request.user)
        

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            data = serializer.save()
            return Response({"success": True,'persona':data.id}, status=status.HTTP_201_CREATED)

class PersonaCompletoViewSet(BasePersonaViewSet):
    """
    get:
    Api para recibir y actualizar datos del modelo persona mismo persona.

    """
    pass


class PersonaParcialViewSet(BasePersonaViewSet):
    """
    get:
    Api para recibir y actualizar datos del modelo persona otro usuario.

    """
    serializer_class = PersonaParcialSerializer



class AvatarViewSet(BasePersonaViewSet):
    """
    get:
    Actualiza la foto del perfil persona de session

    """
    parser_classes = (MultiPartParser,FileUploadParser,)
    serializer_class = AvatarCreateSerializer
    permission_classes = (IsAuthenticated,)

    def put(self, request, version, format=None):
        
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response(status=204)


class ValidacionesViewSet(BaseAPIView):
    """
    get:
        Valida el usuario, clave y correo

    """
    permission_classes = (AllowAny,)
    serializer_class = ValidacionesSerializer

    def get(self, request, *args, **kwargs):

        serializer = self.get_serializer(data=request.GET)
        if serializer.is_valid(raise_exception=False):
            return Response({"success": True})
        return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)


class DocumentChoicesList(MultipleChoicesAPIView):
    """
    get:
        Lista de los tipos de documentos
    """
    choices_response = {"tipo": DOCUMENT_CHOICES}
    permission_classes = (AllowAny,)

persona_completo = PersonaCompletoViewSet.as_view()       
persona_parcial = PersonaParcialViewSet.as_view()



class ValidacionesViewSet(BaseAPIView):
    """
    get:
        Valida el usuario, clave y correo

    """
    permission_classes = (AllowAny,)
    serializer_class = ValidacionesSerializer

    def get(self, request, *args, **kwargs):

        serializer = self.get_serializer(data=request.GET)
        if serializer.is_valid(raise_exception=False):
            return Response({"success": True})
        return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)


avatar = AvatarViewSet.as_view()


class PersonaViewSet(RetrieveAPIView, UpdateAPIView, BaseAPIView):
    """
    patch:
        Actualiza datos de una persona

    """
    permission_classes = (IsAuthenticated,)
    serializer_class = UpdatePersona
    queryset = Persona
    lookup_field = 'pk'

    def get_serializer_class(self):
        if self.request.method.lower() == "get":
            return PersonaParcialSerializer
        return self.serializer_class

    def patch(self, request, *args, **kwargs):

        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data)

        if serializer.is_valid(raise_exception=True):
            data = serializer.save()
            return Response({"success": True})
        return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)





class UserUpdateViewSet(UpdateAPIView, BaseAPIView):
    """
    patch:
        update parcial

    """
    serializer_class = UpdateUserSerializer
    permission_classes = (IsAuthenticated,)    
    queryset = User


    def patch(self, request, *args, **kwargs):
        response = super(UserUpdateViewSet, self).patch(request,*args,**kwargs)
        return Response({"success": True})

class BuscarPorDocumentoViewSet(BaseAPIView):

    permission_classes = (AllowAny,)
    serializer_class = PersonaALLSerializer

    def get(self, request, *args, **kwargs):

        instance = get_object_or_404(Persona,
                   tipo_documento=kwargs.get('tipo'),
                   numero_documento=kwargs.get('documento'))
        serializer = self.get_serializer(instance)
        return Response(serializer.data)

    


class AsociarUsuarioPersonaViewSet(BasePostAPIView, BaseAPIView):
    """
    post:
        El usuario se asocia una persona, debe estar logueado

    """
    serializer_class = AsociarUsuarioPersonaSerializer
    queryset = User.objects.all()
    permission_classes = (IsAuthenticated,)



class InfoRoles(RetrieveAPIView, BaseAPIView):
    """
    get:
        regresa info de roles del usuario en session
    """
    serializer_class = InfoRolesSerializer
    permission_classes = (AllowAny,)

    def get_object(self):
        return self.request.user
