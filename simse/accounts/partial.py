from django.shortcuts import redirect
from django.urls import reverse
from social_core.pipeline.partial import partial


@partial
def require_email(strategy, details, response, user=None,
                  is_new=False, *args, **kwargs):
    if user and user.email:
        return
    elif is_new and not details.get('email'):
        email = strategy.request_data().get('email')
        if email:
            details['email'] = email
        else:
            return redirect(reverse("account:register"))
