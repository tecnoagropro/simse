from django.contrib.auth import get_user_model
from django.contrib.auth.backends import ModelBackend
from rest_framework.authentication import BaseAuthentication, CSRFCheck
from rest_framework import exceptions
from rest_framework_jwt.authentication import JSONWebTokenAuthentication


class EmailAuthBackend(ModelBackend):
    """Allow users to log in with their email address"""

    def authenticate(self, request, username=None, password=None, **kwargs):
        # Some authenticators expect to authenticate by 'username'
        email = username
        if email is None:
            email = kwargs.get('username')

        try:
            user = get_user_model().objects.get(email=email)
            if user.check_password(password):
                user.backend = "%s.%s" % (self.__module__, self.__class__.__name__)
                return user
        except get_user_model().DoesNotExist:
            return None

    def get_user(self, user_id):
        try:
            return get_user_model().objects.get(pk=user_id)
        except get_user_model().DoesNotExist:
            return None


class MobileAuthBackend(ModelBackend):
    """Allow users to log in with their mobile phone"""

    def authenticate(self, request, username=None, password=None, **kwargs):
        # Some authenticators expect to authenticate by 'username'
        mobile = username
        if mobile is None:
            mobile = kwargs.get('username')

        try:
            user = get_user_model().objects.get(persona__mobile=mobile)
            if user.check_password(password):
                user.backend = "%s.%s" % (self.__module__, self.__class__.__name__)
                return user
        except get_user_model().DoesNotExist:
            return None

    def get_user(self, user_id):
        try:
            return get_user_model().objects.get(pk=user_id)
        except get_user_model().DoesNotExist:
            return None


class DevelopmentSessionAuthentication(BaseAuthentication):
    """
    Use Django's session framework for authentication.
    """

    def authenticate(self, request):
        """
        Returns a `User` if the request session currently has a logged in user.
        Otherwise returns `None`.
        """

        # Get the session-based user from the underlying HttpRequest object
        user = getattr(request._request, 'user', None)

        # Unauthenticated, CSRF validation not required
        if not user or not user.is_active:
            return None

        jwttoken = JSONWebTokenAuthentication().get_jwt_value(request)

        if not jwttoken:
            self.enforce_csrf(request)

        # CSRF passed with authenticated user
        return (user, None)

    def enforce_csrf(self, request):
        """
        Enforce CSRF validation for session based authentication.
        """
        reason = CSRFCheck().process_view(request, None, (), {})
        if reason:
            # CSRF failed, bail with explicit error message
            raise exceptions.PermissionDenied('CSRF or JWT Failed: %s' % reason)
