{% load i18n %}
{% autoescape off %}
{% blocktrans %}Someone has used your email address to request a password reset. Unfortunately, there is no {{ site_name }} account set up with this email address.{% endblocktrans %}
{% blocktrans %}Click here to sign up for a {{ site_name }} account now!{% endblocktrans %}
{{ protocol }}://{{ domain }}{% url 'try-asante' %}</a>
{% blocktrans %}The {{ site_name }} team{% endblocktrans %}
{% endautoescape %}