import jwt

from django.contrib.auth import get_user_model
from django.utils.encoding import smart_text
from django.utils.translation import ugettext as _
from rest_framework import exceptions
from rest_framework.authentication import (
    BaseAuthentication, get_authorization_header
)

from rest_framework_jwt.settings import api_settings


jwt_decode_handler = api_settings.JWT_DECODE_HANDLER
jwt_get_username_from_payload = api_settings.JWT_PAYLOAD_GET_USERNAME_HANDLER

User = get_user_model()

class UsernameAuthentication(BaseAuthentication):

    def authenticate(self, request):

        username = request.META.get('HTTP_AUTHORIZATION',None)
        if username is None:
            return

        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            msg = _('No se encontro usuario')
            raise exceptions.AuthenticationFailed(msg)

        return (user, None)

