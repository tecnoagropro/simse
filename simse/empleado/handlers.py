from decimal import Decimal
from .models import Empleado, CostoTattoActual,\
GananciaEmpleado, Ganancia 


def al_asociar_empleado_a_encargado(*args, **kwargs):

	empleado = kwargs.pop('empleado', None)
	pais = empleado.persona.pais
	moneda = Moneda.objects.get(pais=pais)
	costo = CostoTattoActual.objects.filter(
		    encargado=empleado.encargado).last()
	chico = costo.chico
	grande = costo.grande
	relacion = Decimal(0.25)
	costo_chico = chico * relacion
	costo_grande = grande * relacion

	ganancia, creado = Ganancia.objects.get_or_create(
				moneda=moneda, chico=round(costo_chico),
				grande=round(costo_grande))

	GananciaEmpleado.objects.get_or_create(ganancia=ganancia,
										   empleado=empleado)

	return True

def al_actualizar_costo_tattoo_encargado(*args, **kwargs):

	return True
	
	encargado_id = kwargs.pop('encargado_id', None)
	condicion = kwargs.pop('condicion', True)
	chico = kwargs.pop('chico', None)
	grande = kwargs.pop('grande', None)
	moneda_id = kwargs.pop('moneda_id', 1)
	relacion = Decimal(0.3)

	if not encargado_id:
		encargado_id = kwargs.pop('encargado').id

	

	empleados = Empleado.objects.filter(encargado_id=encargado_id,
		                                condicion=condicion)

	if chico and grande:
		costo_chico = chico * relacion
		costo_grande = grande * relacion

		ganancia, creado = Ganancia.objects.get_or_create(
				   chico=costo_chico, grande=costo_grande,
				   moneda_id=moneda_id)

		for empleado in empleados:
			GananciaEmpleado.objects.get_or_create(empleado=empleado,
											       ganancia=ganancia)

