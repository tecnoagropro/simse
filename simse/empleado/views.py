from django.shortcuts import render
from django.http import JsonResponse

from django.shortcuts import get_object_or_404
from django.contrib.auth.decorators import permission_required
from .models import Empleado


def listar(request):
    return render(request,'listar_empleado.html',{})

def registrar(request):
    return render(request,'registrar_empleado.html',{})

def modificar(request,pk):
	context = {
		'empleado_id' : pk,
		'persona_id': Empleado.objects.get(id=pk).persona.id
	}
	return render(request,'modificar_empleado.html',context)    