from django.contrib import admin, messages
from .models import *
from encargado.models import Encargado

class EmpleadoAdmin(admin.ModelAdmin):

    list_display = ('nombre','apellido','ganancia',
                    'user','encargado','condicion')

    list_editable = ['encargado']

    actions = ['empleados_pablo','cambiar_activacion']

    def empleados_pablo(self, request, queryset):
        #pablo = Encargado.objects.get(nombre='Pol') 
        n = queryset.count()

        self.message_user(request,"%(count)d empleados cambiados." % {"count": n }, messages.SUCCESS)   

        for instance in queryset:
            instance.condicion = True
            instance.persona.tipo_documento = instance.persona.tipo_documento.replace("old_","")
            instance.persona.mobile = instance.persona.mobile.replace("old_","")
            instance.persona.save()
            instance.save()

        return queryset

    empleados_pablo.short_description = 'Empleados a pablo'


    def cambiar_activacion(self, request, queryset):
        n = queryset.count()
        self.message_user(request,"%(count)d empleados cambiados." % {"count": n }, messages.SUCCESS)   
        for item in queryset:
            if item.condicion:
                item.condicion = False
            else:
                item.condicion = True
            item.save()
        return queryset
    cambiar_activacion.short_description = 'Cambia el estado de la activacion'

    def nombre(self,obj):
        return obj.persona.primer_nombre

    def apellido(self,obj):
        return obj.persona.primer_apellido

    def user(self,obj):
        return obj.persona.user or None

    def ganancia(self,obj):
        return GananciaEmpleado.objects.filter(empleado=obj).latest('id') or None

admin.site.register(Empleado,EmpleadoAdmin)



@admin.register(CostoTatto)
class CostoTattoAdmin(admin.ModelAdmin):
    pass


class TattosChicosAdmin(admin.ModelAdmin):

    list_display = ('encargado','cantidad',
                    'monto_cobrado','created')

admin.site.register(TattosChicos,TattosChicosAdmin)



@admin.register(RazonesNoTrabajo)
class RazonesNoTrabajoAdmin(admin.ModelAdmin):
    pass



@admin.register(Contador)
class ContadorAdmin(admin.ModelAdmin):
    pass


@admin.register(CostoTattoActual)
class CostoTattoActualAdmin(admin.ModelAdmin):
    pass


class GananciaAdmin(admin.ModelAdmin):

    list_display = ('chico', 'grande')

admin.site.register(Ganancia,GananciaAdmin)


class GananciaEmpleadoAdmin(admin.ModelAdmin):

    list_display = ('ganancia', 'empleado','encargado')

    def encargado(self,obj):
        return obj.empleado.encargado

admin.site.register(GananciaEmpleado,GananciaEmpleadoAdmin)



class DiaTrabajoEmpleadoAdmin(admin.ModelAdmin):

    list_display = ('empleado', 'get_ganancia_empleado','get_ganancia_encargado',
                   'get_monto_recaudado','trabajo')

    def trabajo(self,obj):
        if obj.no_trabajo:
            return False
        else:
            return True