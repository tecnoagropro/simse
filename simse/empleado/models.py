from decimal import Decimal
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.db.models.signals import post_save
from django.dispatch import receiver

from common.models import UserOneBaseModel, BaseModel, UserRelationBaseModel, Permiso
from encargado.models import DiaTrabajo, Encargado, GananciaEncargado
from accounts.models import Persona

class Empleado(BaseModel):

    permisos = models.ManyToManyField(Permiso, blank=True)
    persona = models.OneToOneField(Persona)
    encargado = models.ForeignKey(Encargado, null=True, blank=True)

    def __str__(self):
        return str(self.persona.primer_nombre)

    class Meta:
        ordering = ("persona",)
        verbose_name = _('Empleado')
        verbose_name_plural = _('Empleados')


class Ganancia(BaseModel):

    chico = models.PositiveIntegerField(default=0)
    grande = models.PositiveIntegerField(default=0)

    def __str__(self):
        return str(self.chico)

    class Meta:
        ordering = ("chico",)
        verbose_name = _('Ganancia')
        verbose_name_plural = _('Ganancias')  


class GananciaEmpleado(BaseModel):

    ganancia = models.ForeignKey(Ganancia)
    empleado = models.ForeignKey(Empleado)

    def __str__(self):
        return str(self.empleado)

    class Meta:
        ordering = ("empleado",)
        verbose_name = _('Ganancia por Empleado')
        verbose_name_plural = _('Ganancias para Empleados')  


class CostoTattoActual(BaseModel):

    chico = models.DecimalField(max_digits=30, decimal_places=2)
    grande = models.DecimalField(max_digits=30, decimal_places=2)
    encargado = models.ForeignKey(Encargado)

    def __str__(self):
        return str(self.chico) + ' ' + str(self.grande) 

    class Meta:
        ordering = ("chico",)
        verbose_name = _('Costo de tatto actual')
        verbose_name_plural = _('Costos de tattos actual')


class CostoTatto(BaseModel):

    monto = models.DecimalField(max_digits=30, decimal_places=2)

    def __str__(self):
        return str(self.monto)

    class Meta:
        ordering = ("monto",)
        verbose_name = _('Costo de tatto')
        verbose_name_plural = _('Costo de tattos')

class RazonesNoTrabajo(UserRelationBaseModel):

    razon = models.CharField(max_length=100)

    def __str__(self):
        return self.razon

    class Meta:
        ordering = ("razon",)
        verbose_name = _('Razon no trabajo')
        verbose_name_plural = _('Razones no trabajo')


class TattosChicos(UserRelationBaseModel):

    cantidad = models.PositiveIntegerField()
    monto_cobrado = models.ForeignKey(CostoTatto)
    encargado = models.ForeignKey(Encargado)

    def save(self, *args, **kwargs):
        monto = CostoTattoActual.objects.filter(encargado=self.encargado).latest('id')
        costo, creado = CostoTatto.objects.get_or_create(monto=monto.chico)
        self.monto_cobrado = costo
        super(TattosChicos, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.cantidad)

    @property
    def get_recaudado(self):
        return self.cantidad * self.monto_cobrado.monto

    class Meta:
        ordering = ("cantidad",)
        verbose_name = _('Tatto chico')
        verbose_name_plural = _('Tattos chicos')



class Contador(UserRelationBaseModel):

    inicio = models.PositiveIntegerField()
    fin = models.PositiveIntegerField()
    diferencia = models.PositiveIntegerField()

    def __str__(self):
        return str(self.inicio)

    class Meta:
        ordering = ("inicio",)
        verbose_name = _('Contador')
        verbose_name_plural = _('Contadores')


