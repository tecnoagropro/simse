from django.conf.urls import include, url
from django.views.decorators.csrf import csrf_exempt

from .views import *
from .ajax import *

urlpatterns = [
	url(r'^empleado/registrar$', registrar, name="registrar_empleado"),
	url(r'^empleado/listar$', listar, name="listar_empleado"),
	url(r'^empleado/modificar/(?P<pk>[0-9\.]+)$', modificar, name="modificar_empleado"),
	# url(r'^empleado/detalle$', detalle_empleado, name="detalle_empleado"),

	url(r'^ajax/listar_empleado$', csrf_exempt(ListarEmpleado.as_view()) , name="listar_empleado_ajax"),
	# url(r'^ajax/modificar_empleado$', modificar_empleado_ajax, name="modificar_empleado_ajax"),
	# url(r'^ajax/desactivar_empleado$', csrf_exempt(desactivar_empleado_ajax), name="desactivar_empleado_ajax"),
	# url(r'^ajax/buscar_empleado$', buscar_empleado, name="buscar_empleado_ajax"),
]
