from django.http import JsonResponse
from .models import *
from common.mixins import DataTable
from django.db.models import F
from accounts.models import Persona

### Datatables
class ListarEmpleado(DataTable):
    model = Empleado
    columns = ['persona.primer_nombre','persona.primer_apellido','persona.mobile','encargado.nombre','botones']
    order_columns = ['persona.primer_nombre','persona.primer_apellido','persona.mobile','encargado.nombre',]
    max_display_length = 1000

    def render_column(self, row, column):

        if column == 'botones':
            cadena = '&nbsp'
            # hay que cambiar estos permisos
            #if self.usuario.has_perm('general.change_cliente'):
            cadena=cadena +'<a href="/empleado/modificar/'+str(row.id)+'" type="button" class="btn btn-success" ><i class="fa fa-edit"></i></a>&nbsp'
            cadena=cadena +'<a href="/empleado/modificar/'+str(row.id)+'?ganancia=1" type="button" class="btn btn-warning" ><i class="fa fa-glass"></i></a>&nbsp'
            if row.condicion:
                    cadena=cadena + '<a type="button" class="btn btn-danger" onclick="eliminar_dato('+str(row.id)+')"><i class="fa fa-recycle"></i></a>'
                    #cadena=cadena + '<a type="button" class="btn btn-danger" onclick="eliminar_dato('+str(row.id)+',true)"><i class="fa fa-recycle"></i></a>'
            else:
                cadena=cadena + '<a type="button" class="btn btn-success" onclick="eliminar_dato('+str(row.id)+')"><i class="fa fa-check"></i></a>'
                    
            return cadena
        elif column == 'encargado.nombre':
            try:
                if row.encargado.nombre:
                    return row.encargado.nombre
                return row.encargado.persona.primer_nombre
            except AttributeError as e:
                return 'No tiene encargado'
            
        else:
           return super(ListarEmpleado, self).render_column(row, column)

    def get_initial_queryset(self):
        if self.usuario.is_superuser:
            return self.model.objects.filter(condicion=True)
        else:
            per = Persona.objects.get(user=self.usuario)
            e = Encargado.objects.filter(persona=per)

            if e.exists():
                return self.model.objects.filter(encargado=e[0],condicion=True)
            else:
                return self.model.objects.none()

    def filter_queryset(self, qs):
        search = self.request.POST.get(u'search[value]', None)

        if search:
            qs = qs.filter(persona__primer_nombre__icontains = search)

        if len(qs) == 0:
            qs = qs.filter(persona__primer_apellido__icontains = search)

        if len(qs) == 0:
            qs = qs.filter(persona__numero_documento__icontains = search)

        return qs
