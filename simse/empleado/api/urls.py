from django.conf.urls import include, url
from rest_framework import routers
from rest_framework.routers import SimpleRouter
from .viewsets import EmpleadoViewSet,\
	EmpleadoDetailViewSet, EmpleadoEncargadoViewSet,\
	CostoTattoActualViewSet, RazonesNoTrabajoViewSet,\
	TattosChicosViewSet, GananciasEmpleadosViewSet, \
	UltimoCostoTattoViewSet, GananciaEmpleadoViewSet,\
	CostoTattoEmpleadoSessionViewSet, CostoTattoEmpleadoPkViewSet

router = SimpleRouter()
router.register("costo_tatto_actual", CostoTattoActualViewSet)
router.register("tattos_chicos", TattosChicosViewSet)
router.register("razones_no_trabajo", RazonesNoTrabajoViewSet)
router.register("ganancia_empleados", GananciasEmpleadosViewSet)


urlpatterns = [

    url(r'^empleado/(?P<pk>\d+)/ganancia/$',GananciaEmpleadoViewSet.as_view(), name="empleado-ganancia"),	
	url(r'^empleado/(?P<pk>\d+)/encargado/$', EmpleadoEncargadoViewSet.as_view(), name="empleado-encargado"), 
    url(r'^empleado/(?P<pk>\d+)/$', EmpleadoDetailViewSet.as_view(), name="empleado-detail"), 
    
    url(r'^ultimo_costo_tatto/(?P<pk>\d+)/$', UltimoCostoTattoViewSet.as_view(), name="ultimo-costo-tatto"),
    
    url(r'^empleado/costo/tatuajes/$', CostoTattoEmpleadoSessionViewSet.as_view(), name="empleadosession-costo-tatuajes"),
    url(r'^empleado/(?P<pk>\d+)/costo/tatuajes/$', CostoTattoEmpleadoPkViewSet.as_view(),
    	name="empleado-costo-tatuajes"),
    
    url(r'^empleado/$', EmpleadoViewSet.as_view(), name="empleado"),
    url(r'^', include(router.urls)),

]
