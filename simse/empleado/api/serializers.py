from django.contrib.auth import get_user_model
from django.urls import resolve
from django.utils.translation import ugettext as _

from rest_framework import serializers
from rest_framework.serializers import HyperlinkedModelSerializer,raise_errors_on_nested_writes
from rest_framework.generics import get_object_or_404
from rest_framework.compat import set_many
from rest_framework.utils import model_meta
from rest_framework.fields import SerializerMethodField
from rest_framework.validators import UniqueTogetherValidator

from accounts.models import Persona
from accounts.serializers import PersonaParcialSerializer,PersonaALLSerializer
from common.serializers import QueryFieldsMixin,PermisoSerializer
from encargado.models import Encargado
from ..handlers import al_actualizar_costo_tattoo_encargado,\
al_asociar_empleado_a_encargado

from ..models import *

User = get_user_model()


class EmpleadoEncargadoSerializer(QueryFieldsMixin, HyperlinkedModelSerializer):

    encargado = serializers.IntegerField(source="encargado.id", required=False)
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())

    def validate(self, attrs):
            
        errors = dict()
        encargado = attrs.get('encargado',None)
        user = attrs.get('user')

        if not encargado:
            try:
                Encargado.objects.get(persona=user.persona_set) 
            except Encargado.DoesNotExist:
                errors['encargado'] = 'Encargado invalido'
        
        if errors:
            raise serializers.ValidationError(errors)

        return super(EmpleadoEncargadoSerializer, self).validate(attrs)

    def validate_encargado(self,pk):
        return Encargado.objects.get(id=pk)

    def create(self,validated_data):

        user = validated_data.pop('user')

        try:
            per = Persona.objects.get(user=user)
            user_encargado = Encargado.objects.get(persona=per) 
        except Exception as e:
            print (e)
            user_encargado = None
        
        if validated_data.get('encargado',None):
            encargado = validated_data.pop('encargado')['id']
        else:
            encargado = user_encargado
            
        empleado_id = self.context['view'].kwargs['pk']

        empleado = Empleado.objects.get(id=empleado_id)
        empleado.encargado = encargado
        empleado.save()

        al_asociar_empleado_a_encargado(
            empleado=empleado
        )
        return validated_data

    class Meta:
        model = Empleado
        fields = ('id','encargado','user')


class GananciaSerializer(QueryFieldsMixin, serializers.ModelSerializer):

    user = serializers.HiddenField(default=serializers.CurrentUserDefault())
    
    class Meta:
        model = Ganancia
        fields = ('id','chico','grande','moneda','user')

    

class EmpleadoSerializer(QueryFieldsMixin, HyperlinkedModelSerializer):

    permisos = PermisoSerializer(many=True, read_only=True)
    persona = serializers.IntegerField(source="persona.id",write_only=True)
    datos_personales = PersonaALLSerializer(source="persona",read_only=True)
    ganancia = SerializerMethodField(read_only=True)

    def get_ganancia(self, empleado):
        try:
            ganancia = GananciaEmpleado.objects.filter(empleado=empleado).latest('id')
            
            new_context = {
                "request": self.context["request"]
            }
            response = GananciaSerializer(ganancia.ganancia, context=new_context)
            return response.data
        except GananciaEmpleado.DoesNotExist:
            return None


    def create(self, validated_data):
        persona = validated_data.get('persona')

        try:
            empleado = Empleado()
            empleado.persona_id=persona.get('id')
            empleado.save()

        except Exception as e:
            raise e
        return empleado

    def validate_persona(self, id):
        persona = get_object_or_404(Persona, pk=id)
        return id

    def update(self, instance, validated_data):
        raise_errors_on_nested_writes('update', self, validated_data)
        info = model_meta.get_field_info(instance)

        for attr, value in validated_data.items():
            if attr in info.relations and info.relations[attr].to_many:
                set_many(instance, attr, value)
            else:
                if attr == 'user':
                    setattr(instance,attr,value['pk'])
                else:
                    setattr(instance, attr, value)

        instance.save()
        return instance

    class Meta:
        model = Empleado
        fields = ('id','permisos','persona','datos_personales','ganancia')


class CostoTattoActualSerializer(serializers.ModelSerializer):

    encargado = serializers.IntegerField(source="encargado.id")
    
    def create(self, validated_data):
        encargado = validated_data.pop('encargado')['id']
        chico = validated_data.pop('chico')
        grande = validated_data.pop('grande')
        instance = CostoTattoActual(encargado_id=encargado,
                                    grande=grande,
                                    chico=chico,
                                    **validated_data)
        instance.save()

        al_actualizar_costo_tattoo_encargado(
            encargado_id=encargado,
            grande=grande,
            chico=chico,
        )

        return instance

    class Meta:
        model = CostoTattoActual
        fields = ('id','chico','grande','encargado',)


class TattosChicosSerializer(serializers.ModelSerializer):

    user = serializers.HiddenField(default=serializers.CurrentUserDefault())
    encargado = serializers.IntegerField(source="encargado.id")

    def create(self, validated_data):
        encargado = validated_data.pop('encargado')['id']
        instance, creado = TattosChicos.objects.get_or_create(
                           encargado_id=encargado,**validated_data)
        instance.save()
        return instance
        
    class Meta:
        model = TattosChicos
        fields = ('id','user',
                  'cantidad','encargado')


class RazonesNoTrabajoSerializer(serializers.ModelSerializer):

    user = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = RazonesNoTrabajo
        fields = ('id','razon','user')


class GananciasEmpleadosSerializer(serializers.ModelSerializer):

    user = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = GananciaEmpleado
        fields = ('id','ganancia','empleado','user')


class UltimoCostoTattoSerializer(HyperlinkedModelSerializer):

    class Meta:
        model = CostoTattoActual
        fields = ('chico','grande')



class GananciaPorTemporadaSerializer(HyperlinkedModelSerializer):

    ganancia = SerializerMethodField()
    generado = SerializerMethodField()
    moneda = SerializerMethodField()

    def get_ganancia(self, empleado):
        
        dias_trabajo = DiaTrabajoEmpleado.objects.filter(
                       empleado=empleado,condicion=True,
                       temporada_id=1,no_trabajo__isnull=True)

        ganancia = 0
        for dia in dias_trabajo:
            ganancia += dia.get_ganancia_empleado

        return ganancia

    def get_generado(self, empleado):
        
        dias_trabajo = DiaTrabajoEmpleado.objects.filter(
                       empleado=empleado, condicion=True,
                       temporada_id=1, no_trabajo__isnull=True)

        generado = 0
        for dia in dias_trabajo:
            generado += dia.get_monto_recaudado

        return generado

    def get_moneda(self, empleado):
        try:
            ganancia_empleado = GananciaEmpleado.objects.filter(empleado=empleado).latest('id')
            return ganancia_empleado.ganancia.moneda.simbolo
        except Exception as e:
            pais = empleado.persona.pais
            return Moneda.objects.get(pais=pais).simbolo     
        

    class Meta:
        model = Empleado
        fields = ('ganancia','moneda','generado')



class GananciaEmpleadoSerializer(QueryFieldsMixin,
                                  HyperlinkedModelSerializer):

    ganancia_chico = serializers.IntegerField(read_only=True,
                                     source='ganancia.chico')
    ganancia_grande = serializers.IntegerField(read_only=True,
                                     source='ganancia.grande')
    empleado = serializers.IntegerField(write_only=True,source='empleado.id')
    grande = serializers.IntegerField(write_only=True)
    chico = serializers.IntegerField(write_only=True)
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())

    def create(self, validated_data):

        grande = validated_data.pop('grande')
        chico = validated_data.pop('chico')
        empleado = validated_data.pop('empleado')
        user = validated_data.pop('user')

        moneda = Moneda.objects.get(pais=Persona.objects.get(user=user).pais)
        ganancia, creado = Ganancia.objects.get_or_create(chico=chico,
                           grande=grande, moneda=moneda)

        
        instance = GananciaEmpleado.objects.create(
                    ganancia=ganancia,
                    empleado_id=empleado['id'])

        instance.save()

        return instance


    class Meta:
        model = GananciaEmpleado
        fields = ('id', 'chico','grande','empleado','user',
                  'ganancia_chico','ganancia_grande')
