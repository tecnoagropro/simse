from uuid import UUID
from rest_framework.generics import ListAPIView, RetrieveAPIView,\
    get_object_or_404, RetrieveUpdateAPIView,\
    DestroyAPIView, CreateAPIView
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework import status, viewsets

from accounts.models import Persona
from django.contrib.auth import get_user_model
from common.mixins import BaseAPIView, BorrarView, BasePostAPIView, MultipleChoicesAPIView
from .serializers import *
from ..models import Empleado, GananciaEmpleado

class EmpleadoViewSet(ListAPIView, BasePostAPIView):
    """

    get:
        Lista todos los empleados, se puede filtrar

    post:
        Crear nuevo empleado

    """
    serializer_class = EmpleadoSerializer
    permission_classes = (IsAuthenticated,)
    queryset = Empleado.objects.filter(condicion=True)


class EmpleadoDetailViewSet(RetrieveUpdateAPIView, BaseAPIView):
    """

    get:
    regresa un empleado

    delete:
    Activa o desactiva a un empleado por pk de url

    patch:
    actualiza el codigo o el usuario de un empleado
    
    """
    serializer_class = EmpleadoSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return Empleado.objects.filter(condicion=True)

    def delete_old(self, request, *args, **kwargs):
        
        instance = self.get_object()
        instance.condicion = False       
        instance.persona.tipo_documento = 'old_' + instance.persona.tipo_documento
        instance.persona.mobile = 'old_' + instance.persona.mobile
        if instance.persona.user:
            instance.persona.user.delete()
            instance.persona.user = None
        instance.persona.save()
        instance.save()

        return Response(status=status.HTTP_204_NO_CONTENT)

    def delete(self, request, *args, **kwargs):

        instance.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class EmpleadoEncargadoViewSet(DestroyAPIView,
                               RetrieveAPIView,
                               BasePostAPIView):
    """

    post:
        Asocia un empleado a un encargado

    delete:
        Elimina la asociacion de un empleado con un encargado

    """
    serializer_class = EmpleadoEncargadoSerializer
    permission_classes = (IsAuthenticated,)
    queryset = Empleado

    def perform_destroy(self, instance):
        instance.encargado = None
        instance.save()



class CostoTattoActualViewSet(viewsets.ModelViewSet):
    """

    post:
        Actualiza el costo de los tatuajes para un encargado
    """

    serializer_class = CostoTattoActualSerializer
    queryset = CostoTattoActual.objects.all()
    permission_classes = (IsAuthenticated,)


class TattosChicosViewSet(viewsets.ModelViewSet):
    
    serializer_class = TattosChicosSerializer
    queryset = TattosChicos.objects.all()
    permission_classes = (IsAuthenticated,)


class RazonesNoTrabajoViewSet(viewsets.ModelViewSet):
    
    serializer_class = RazonesNoTrabajoSerializer
    queryset = RazonesNoTrabajo.objects.all()
    permission_classes = (IsAuthenticated,)


class GananciasEmpleadosViewSet(viewsets.ModelViewSet):
    
    serializer_class = GananciasEmpleadosSerializer
    queryset = GananciaEmpleado.objects.all()
    permission_classes = (IsAuthenticated,)


class UltimoCostoTattoViewSet(RetrieveAPIView):
    """
    get:

        Regresa el ultimo costo de tatuajes para un encargado
        por pk de url
    """
    serializer_class = UltimoCostoTattoSerializer
    queryset = CostoTattoActual.objects.all()
    permission_classes = (IsAuthenticated,)

    def get_object(self):
        encargado_id = self.kwargs.get('pk')
        return CostoTattoActual.objects.filter(encargado_id=encargado_id).latest('id')



class GananciaEmpleadoViewSet(RetrieveAPIView, CreateAPIView, BaseAPIView):
    """


    get:
        Devuelve la ganancia para un empleado en particular

    """

    serializer_class = GananciaEmpleadoSerializer
    permission_classes = (IsAuthenticated,)

    def get_object(self):
        emp = Empleado.objects.get(id=self.kwargs['pk'])
        return GananciaEmpleado.objects.filter(empleado=emp).latest('id')



class CostoTattoEmpleadoSessionViewSet(RetrieveAPIView):
    """
    get:

        Regresa el costo de tatuajes para un empleado en la session
        
    """
    serializer_class = UltimoCostoTattoSerializer
    queryset = CostoTattoActual.objects.all()
    permission_classes = (IsAuthenticated,)

    def get_object(self):
        persona = Persona.objects.get(user=self.request.user)
        empleado = Empleado.objects.get(persona=persona)
        return CostoTattoActual.objects.filter(encargado=empleado.encargado).latest('id')


class CostoTattoEmpleadoPkViewSet(RetrieveAPIView):
    """
    get:

        Regresa el costo de tatuajes para un empleado por pk
        
    """
    serializer_class = UltimoCostoTattoSerializer
    queryset = CostoTattoActual.objects.all()
    permission_classes = (IsAuthenticated,)

    def get_object(self):
        emp = Empleado.objects.get(id=self.kwargs['pk'])
        return CostoTattoActual.objects.filter(encargado=emp.encargado).latest('id')        