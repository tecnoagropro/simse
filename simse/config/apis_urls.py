from django.conf.urls import include, url
from common.views import SwaggerSchemaView

urlpatterns = [

    url(r'^(?P<version>[v1.]+)/', include('accounts.apis_urls'), name="accounts"),
    url(r'^(?P<version>[v1.v2.]+)/', include('encargado.api.urls'), name="encargado"),
    url(r'^(?P<version>[v1.]+)/', include('inventario.api.urls'), name="inventario"),
    url(r'^(?P<version>[v1.]+)/', include('reporte.api.urls'), name="reporte"),
    url(r'^(?P<version>[v1.]+)/', include('common.apis_urls'), name="common"),
    url(r'^docs$', SwaggerSchemaView.as_view())
]
