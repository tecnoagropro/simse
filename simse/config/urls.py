from django.conf import settings
from django.conf.urls import url, include, handler404, handler500
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.conf.urls.static import static
from django.views.static import serve
from common.views import HomeView, View404, View500, ConfigDjangoJs

urlpatterns = [
    url(r'^$', HomeView.as_view(), name="inicio"),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^api/', include('config.apis_urls', namespace='api')),
    url(r'^', include('inventario.urls', namespace='inventario_app')),
    url(r'^', include('encargado.urls', namespace='encargados_app')),
    url(r'^', include('reporte.urls', namespace='reporte_app')),
    url(r'^', include('accounts.urls', namespace='accounts')),
    url(r'^', include('notificaciones.urls', namespace='notificaciones_app')),    
    url(r'^admin/', include(admin.site.urls)),
    #url(r'^(?!api|admin|\.).*$', HomeView.as_view(), name='home'),
    url(r'^config_django.js$', ConfigDjangoJs.as_view(content_type="application/javascript"), name="config_django"),

    
    #url(r'^template/skin-config.html/$', views.skin_config, name='skin_config'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

handler404 = View404.as_view()
handler500 = View500.as_view()

if settings.DEBUG:

    urlpatterns += [
        url(r'^(?P<path>(favicon.ico))$', serve, {'document_root': settings.STATICFILES_DIRS[0]})
    ]

    import sys
    if "debug_toolbar" in sys.modules:
        import debug_toolbar

        urlpatterns = [
                          url(r'^__debug__/', include(debug_toolbar.urls)),
                      ] + urlpatterns