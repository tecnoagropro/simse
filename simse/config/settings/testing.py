# -*- coding: utf-8 -*-
from .base import *
import dj_database_url

from config.settings import get_env_variable_required

SECRET_KEY = get_env_variable_required('SECRET_KEY')

DATABASES = {'default': dj_database_url.config(conn_max_age=600)}


LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'file': {
            'level': 'ERROR',
            'class': 'logging.FileHandler',
            'filename': os.path.join(BASE_DIR, 'debug.log'),  # '/path/to/django/debug.log'
        },
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler',
        }
    },
    'loggers': {
        'django': {
            'handlers': ['file'],
            'level': 'ERROR',
            'propagate': True,
        },
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': False,
        },
    },
}

EMAIL_BACKEND = 'django.core.mail.backends.locmem.EmailBackend'

TWILIO_ACCOUNT_SID = "ACffc39d5c0564a35f6cadc7d32eccce2c"
TWILIO_ACCOUNT_TOKEN = "e82be4ab737a4b36f67fcc1efa414386"
TWILIO_PHONE_NUMBER = "+15005550006"
TWILIO_DEFAULT_CALLERID = TWILIO_PHONE_NUMBER

RECAPTCHA_PUBLIC_KEY = '6Lf0_hgUAAAAAHeYimJLCsSmra-0Cwvve6Hh-OnG'
RECAPTCHA_PRIVATE_KEY = '6Lf0_hgUAAAAAAOJhASZbTZwz4cpOUtYRqG3eKa4'

SOCIAL_AUTH_FACEBOOK_KEY = '1535766036465971'
SOCIAL_AUTH_FACEBOOK_SECRET = 'a9c433d8aeb4f83f403dee857c6e2604'

EMAIL_HOST_USER = 'ahsante@ahsante.co'

DEBUG = True

# JENKINS CONFIGS

INSTALLED_APPS += ['django_jenkins']

# JENKINS_TEST_RUNNER = "config.runner.CIPytestTestRunner"

JENKINS_TASKS = (
    'django_jenkins.tasks.run_pep8',
    'django_jenkins.tasks.run_pylint',
)