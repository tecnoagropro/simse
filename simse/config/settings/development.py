# -*- coding: utf-8 -*-
import raven

from .base import *
from . import get_env_variable_required
import os
import dj_database_url

DATABASES = {'default': dj_database_url.config(conn_max_age=600)}

ALLOWED_HOSTS = ['dev.ahsante.co', "198.74.48.173", "127.0.0.1"]

DEBUG = True

SECRET_KEY = get_env_variable_required('SECRET_KEY')

TWILIO_ACCOUNT_SID = get_env_variable_required("TWILIO_ACCOUNT_SID")
TWILIO_ACCOUNT_TOKEN = get_env_variable_required("TWILIO_ACCOUNT_TOKEN")
TWILIO_PHONE_NUMBER = get_env_variable_required("TWILIO_PHONE_NUMBER")
TWILIO_DEFAULT_CALLERID = TWILIO_PHONE_NUMBER

SESSION_COOKIE_SECURE = False
CSRF_COOKIE_SECURE = False
SECURE_SSL_REDIRECT = False

# SECURE_HSTS_SECONDS = 3600 * 24

EMAIL_BACKEND = 'django.core.mail.backends.locmem.EmailBackend'

INSTALLED_APPS += ["corsheaders"]

MIDDLEWARE.insert(1, "corsheaders.middleware.CorsMiddleware")

MIDDLEWARE.remove('django.middleware.clickjacking.XFrameOptionsMiddleware')

CORS_ORIGIN_WHITELIST = (
    'localhost:3000',
    '127.0.0.1:3000'
)

INSTALLED_APPS.append('raven.contrib.django.raven_compat')
RAVEN_CONFIG = {
    'dsn': 'https://b67a1eb6dca7478f8339ac4ec0e50dbf:e5e91e0f078d4811ae83eaae0d6b55f9@sentry.io/197238',
    # If you are using git, you can also automatically configure the
    # release based on the git info.
    'release': raven.fetch_git_sha(os.path.dirname(BASE_DIR)),

}