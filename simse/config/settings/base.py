import os
from datetime import timedelta
from django.urls import reverse_lazy

from . import get_env_variable
from django.utils.translation import ugettext_lazy as _

BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

SECRET_KEY = get_env_variable('SECRET_KEY')

DEBUG = True

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.staticfiles',
    'django.contrib.messages',
    'dynamic_preferences',
    'rest_framework',
    'accounts',
    'rest_framework_swagger',
    'inventario',
    'reporte',
    'encargado',
    #'empleado',
    'notificaciones',
    'common',
    #'channels',
]

CHANNEL_LAYERS = {
 "default": {
 "BACKEND": "asgi_redis.RedisChannelLayer",
 "CONFIG": {
   "hosts": [os.environ.get("REDIS_URL", "redis://redis:6379")],
 },
 "ROUTING": "config.routing.channel_routing",
 },
}

BROKER_URL = os.environ.get("REDIS_URL", "redis://redis:6379/0")


# Jenkins application definition
PROJECT_APPS = ['accounts', 'activities', 'tracking', 'ratings', 'purchases',
                'banks',
                'payments',
                'refunds',
                'messaging']

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    #'tracking.middleware.VisitorTrackingMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    #'django.middleware.common.BrokenLinkEmailsMiddleware',
    #'django.middleware.clickjacking.XFrameOptionsMiddleware',
    #'social_django.middleware.SocialAuthExceptionMiddleware',
    #'simple_history.middleware.HistoryRequestMiddleware',
    'common.middleware.MethodOverrideMiddleware',
]
# security
X_FRAME_OPTIONS = 'DENY'

ROOT_URLCONF = 'config.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.i18n',
                'common.context_processors.baseurl',
            ],
        },
    },
]

ADMINS = [('Jovan', 'jovanjpacheco@gmail.com'), ('Jeremy', 'jeremyquijada@gmail.com ')]
MANAGERS = ADMINS  # MANAGERS are sent email notifications of broken links (i.e. HTTP 404 page requests)

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s  \n %(asctime)s %(module)s %(process)d \n %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'file': {
            'level': 'ERROR',
            'class': 'logging.FileHandler',
            'formatter': 'verbose',
            'filename': os.path.join(BASE_DIR, 'debug.log'),
        },
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler',
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['file'],
            'level': 'ERROR',
            'propagate': False,
        },
    },
}

WSGI_APPLICATION = 'config.wsgi.application'

# Password validation
# https://docs.djangoproject.com/en/1.10/ref/settings/#auth-password-validators

PASSWORD_HASHERS = [
    #'django.contrib.auth.hashers.Argon2PasswordHasher',
    'django.contrib.auth.hashers.PBKDF2PasswordHasher',
    'django.contrib.auth.hashers.PBKDF2SHA1PasswordHasher',
    'django.contrib.auth.hashers.BCryptSHA256PasswordHasher',
    'django.contrib.auth.hashers.BCryptPasswordHasher',
]

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

AUTHENTICATION_BACKENDS = [
    "accounts.backends.EmailAuthBackend",
    #"veenote.accounts.backends.MobileAuthBackend",
    "django.contrib.auth.backends.ModelBackend"
]


FACEBOOK_AUTH_EXTRA_ARGUMENTS = {'display': 'touch'}

SOCIAL_AUTH_URL_NAMESPACE = 'social'
SOCIAL_AUTH_FACEBOOK_KEY = get_env_variable("SOCIAL_AUTH_FACEBOOK_KEY")
SOCIAL_AUTH_FACEBOOK_SECRET = get_env_variable("SOCIAL_AUTH_FACEBOOK_SECRET")
SOCIAL_AUTH_FACEBOOK_SCOPE = ['email', 'user_birthday']
SOCIAL_AUTH_FACEBOOK_API_VERSION = '2.8'
SOCIAL_AUTH_FACEBOOK_PROFILE_EXTRA_PARAMS = {
    'locale': 'en_EN',
    'fields': 'id, name, email, age_range'
}

TWILIO_ACCOUNT_SID = get_env_variable("TWILIO_ACCOUNT_SID")
TWILIO_ACCOUNT_TOKEN = get_env_variable("TWILIO_ACCOUNT_TOKEN")
TWILIO_PHONE_NUMBER = get_env_variable("TWILIO_PHONE_NUMBER")
TWILIO_DEFAULT_CALLERID = TWILIO_PHONE_NUMBER

# DIGITS_CONSUMER_KEY = "JdiphPW80DH97lu293hoISw3e" Not necessary

LOGIN_URL = '/login/' #reverse_lazy("rest_framework:login")
LOGOUT_URL = reverse_lazy("rest_framework:logout")
LOGOUT_REDIRECT_URL = reverse_lazy("rest_framework:login")
# LOGIN_REDIRECT_URL = reverse_lazy("profile")

CONFIRM_EMAIL_URL = "/confirm_email/%s"
PASSWORD_RESET_VERIFY_URL = "/password_reset/%s/%s"

#SOCIAL_AUTH_NEW_USER_REDIRECT_URL = reverse_lazy("account:register")

# FACEBOOK_EXTENDED_PERMISSIONS = ['email']

# SOCIAL_AUTH_LOGIN_REDIRECT_URL = '/complete/facebook/'

SOCIAL_AUTH_RAISE_EXCEPTIONS = DEBUG
# SOCIAL_AUTH_BACKEND_ERROR_URL
# LOGIN_ERROR_URL
# LOGIN_URL

SOCIAL_AUTH_DISCONNECT_PIPELINE = (
    # Verifies that the social association can be disconnected from the current
    # user (ensure that the user login mechanism is not compromised by this
    # disconnection).
    'social_core.pipeline.disconnect.allowed_to_disconnect',

    # Collects the social associations to disconnect.
    'social_core.pipeline.disconnect.get_entries',

    # Revoke any access_token when possible.
    'social_core.pipeline.disconnect.revoke_tokens',

    # Removes the social associations.
    'social_core.pipeline.disconnect.disconnect',
)

SOCIAL_AUTH_PIPELINE = (
    # Send a validation email to the user to verify its email address.
    # Disabled by default.
    # 'social_core.pipeline.mail.mail_validation',

    # recibe via backend y uid las instancias de social_user y user
    'social_core.pipeline.social_auth.social_details',

    'social_core.pipeline.social_auth.social_uid',
    'social_core.pipeline.social_auth.auth_allowed',

    # Recibe segun user.email la instancia del usuario y lo reemplaza con uno que recibio anteriormente
    'social_core.pipeline.social_auth.social_user',

    # Trata de crear un username valido segun los datos que recibe
    'social_core.pipeline.user.get_username',

    # Crea un usuario nuevo si uno todavia no existe
    'social_core.pipeline.user.create_user',

    # Trata de conectar las cuentas
    'social_core.pipeline.social_auth.associate_user',

    # Recibe y actualiza social_user.extra_data
    'social_core.pipeline.social_auth.load_extra_data',

    # Actualiza los campos de la instancia user con la informacion que obtiene via backend
    'social_core.pipeline.user.user_details',
    'accounts.partial.require_email'
)

# Internationalization
# https://docs.djangoproject.com/en/1.11/topics/i18n/

LANGUAGE_CODE = 'en'

TIME_ZONE = 'America/Caracas'

USE_I18N = True
# USE_L10N = True

USE_TZ = True

LANGUAGES = (
    ('en', _('English')),
    ('es', _('Spanish')),
)

LOCALE_PATHS = (
    os.path.join(BASE_DIR, 'locale'),
)
# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.10/howto/static-files/

STATIC_URL = '/static/'
MEDIA_URL = '/media/'

FRONTEND_DIR = os.path.join(os.path.dirname(BASE_DIR), "frontend", "build")
MEDIA_ROOT = os.path.join(BASE_DIR, "media")
#STATICFILES_DIRS = (os.path.join(BASE_DIR, 'assets'), os.path.join(FRONTEND_DIR, "static"))
#STATIC_ROOT = os.path.join(BASE_DIR, 'static')

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'staticfiles')

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
)


REST_FRAMEWORK = {
    'DEFAULT_RENDERER_CLASSES': (
        'common.renderers.PrettyJSONRenderer',
    ),
    'NON_FIELD_ERRORS_KEY':'errores',
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated',
    ),
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework_jwt.authentication.JSONWebTokenAuthentication',
    ),
    'DEFAULT_VERSIONING_CLASS': 'common.versioning.AppVersioning',

    'DEFAULT_PAGINATION_CLASS': 'common.pagination.LinkHeaderPagination',
    'PAGE_SIZE': 300,

    'EXCEPTION_HANDLER': 'common.views.custom_exception_handler',
    'JWT_EXPIRATION_DELTA': timedelta(hours=12),
    "JWT_ALLOW_REFRESH": True,
    'DEFAULT_THROTTLE_CLASSES': (
        'rest_framework.throttling.AnonRateThrottle',
        'rest_framework.throttling.UserRateThrottle'
    ),
    'NUM_PROXIES': 2,
    'DEFAULT_THROTTLE_RATES': {
        'anon': '100/hour',
        'user': '500/hour'
    },

}

SWAGGER_SETTINGS = {
    'SECURITY_DEFINITIONS': {
        'api_key': {
            'type': 'apiKey',
            'in': 'header',
            'name': 'Authorization'
        }
    },
    'USE_SESSION_AUTH': False
}

CITIES_TRANSLATION_LANGUAGES = ['en']
CITIES_POSTAL_CODES = ['US']

STRIPE_API_KEY = get_env_variable("STRIPE_API_KEY")

NOCAPTCHA = True
RECAPTCHA_PUBLIC_KEY = get_env_variable("RECAPTCHA_PUBLIC_KEY")
RECAPTCHA_PRIVATE_KEY = get_env_variable("RECAPTCHA_PRIVATE_KEY")



TEST_RUNNER = 'config.runner.PytestTestRunner'

CELERY_BROKER_URL = get_env_variable('BROKER_URL', 'amqp://guest:guest@localhost//')
CELERY_RESULT_BACKEND = get_env_variable('RESULT_BACKEND', 'redis://localhost:6379/0')
# configure queues, currently we have only one
# CELERY_DEFAULT_QUEUE = 'default'
# CELERY_QUEUES = (
#     Queue('default', Exchange('default'), routing_key='default'),
# )

# posible redis for result if necessary
# CELERY_RESULT_BACKEND = 'redis://%s:%d/%d' % (REDIS_HOST, REDIS_PORT, REDIS_DB)
# CELERY_REDIS_MAX_CONNECTIONS = 1

EMAIL_BACKEND = 'djcelery_email.backends.CeleryEmailBackend'
# EMAIL_BACKEND = 'django_ses.SESBackend'

AWS_ACCESS_KEY_ID = get_env_variable("AWS_ACCESS_KEY_ID")
AWS_SECRET_ACCESS_KEY = get_env_variable("AWS_SECRET_ACCESS_KEY")

AWS_SES_REGION_NAME = 'us-west-2'
AWS_SES_REGION_ENDPOINT = 'email.us-west-2.amazonaws.com'

CELERY_EMAIL_BACKEND = "django_ses.SESBackend"

MONEY_DECIMAL_FIELDS = {
    'max_digits': 30,
    'decimal_places': 2,
}

SITE_NAME = "simse"
CSRF_COOKIE_NAME = 'csrftoken_' + SITE_NAME
SESSION_COOKIE_NAME = 'scn_' + SITE_NAME
NAME_JWT = 'tok_' + SITE_NAME
SITE_ID = 1

ONESIGNAL_APP_ID = "065c624a-d014-4403-a43d-e39f9eb91231"
ONESIGNAL_API_KEY = 'MmU1MzUyNDYtMzEzOC00NzZmLWI2YTQtNjExZmUyZTI5Y2Mz'