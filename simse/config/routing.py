from channels import route
from reporte import consumers

channel_routing = [
 # Las funciones se definen en consumers.py
 route("websocket.connect", consumers.ws_connect),
 route("websocket.receive", consumers.ws_receive),
]