jQuery.validator.addMethod("notEqual", function(value, element, param) {
  return this.optional(element) || value != param;
}, "Please specify a different (non-default) value");

$.validator.addMethod(
    "formatofecha",
    function(value, element) {
        // put your own logic here, this is just a (crappy) example
        return value.match(/^\d\d?\-\d\d?\-\d\d\d\d$/);
    },
    "Please enter a date in the format dd/mm/yyyy."
);
/*
jQuery.validator.addMethod("correosociovalidar", function(value, element, param) {
   

    if ($('input:radio[name=tipo_titular]:checked').val() == 'T')
    {
        socio = respuesta.socio
    }else
    {
        socio = respuesta.cotitulares[parseInt($('input:radio[name=socio_cotitular]:checked').val())]
    }
    if ($("#correo").val().toUpperCase() == socio.correo.toUpperCase())
    {
        if(socio.nombre)
        {
            $("#nombre").val(socio.nombre)
            $("#apellido").val(socio.apellido)
        }
        if(socio.segundo_nombre)
        {
            $("#segundo_nombre").val(socio.segundo_nombre)
            $("#segundo_apellido").val(socio.segundo_apellido)
        }
        else
        {
            $("#segundo_nombre").val("");
            $("#segundo_apellido").val("");
        }

        $("#pid").val(socio.pid)    
            return true;
    }else
    {
        return false;
    }

}, "Disculpe, el correo no coincide");*/



var FormWizard = function () {
	"use strict";
    var wizardContent = $('#wizard');
    var wizardForm = $('#form');
    var numberOfSteps = $('.swMain > ul > li').length;
    var initWizard = function () {
        // function to initiate Wizard Form
        wizardContent.smartWizard({
            selected: 0,
            keyNavigation: false,
            onLeaveStep: leaveAStepCallback,
            onShowStep: onShowStep,
        });
        var numberOfSteps = 0;
        initValidator();
        
    };

    var initValidator = function () {
        
        $.validator.setDefaults({
            errorElement: "span", // contain the error msg in a span tag
            errorClass: 'help-block',
            ignore: ':hidden',
            rules: {
                fecha_nacimiento: {
                    formatofecha: true,
                    required: true
                },
                ita: {
                    minlength: 4,
                    maxlength: 5,
                    number: true,
                    required: true
                },                
                tipo_titular: {
                    required: true
                },
                correo: {
                    email: true,
                    required: true
                },
                nombre: {
                    minlength: 3,
                    maxlength: 25,
                    required: true
                },
                apellido: {
                    minlength: 3,
                    maxlength: 25,
                    required: true
                },
                segundo_nombre: {
                    minlength: 3,
                    maxlength: 25
                },
                segundo_apellido: {
                    minlength: 3,
                    maxlength: 25
                },
                sexo: {
                    required: true
                },
                username: {
                    minlength: 6,
                    maxlength: 12,
                    required: true
                },
                clave1: {
                    minlength: 6,
                    maxlength: 12,
                    required: true
                },
                clave2: {
                    minlength: 6,
                    maxlength: 12,
                    equalTo: "#clave1",
                    required: true
                },
                contacto1: {
                    required: true
                },
                estado: {
                    notEqual: "Seleccione",
                    required: true
                },
                municipios: {
                    notEqual: "Seleccione",
                    required: true
                },
                parroquias: {
                    notEqual: "Seleccione",
                    required: true
                },
                lider: {
                    required: false
                },
                socio_directo: {
                    required: false
                }
            },
            messages: {
                fecha_nacimiento:{
                    formatofecha: "El formato de fecha debe ser dd-mm-yyyy",
                    required: "Debes seleccionar una fecha"
                },
                ita: {
                    minlength: "Debe contener un minimo de 4 digitos",
                    maxlength: "Debe contener un maximo de 5 digitos",
                    number: "El campo debe ser numerico",
                    required: "El campo es obligatorio"
                },                
                tipo_titular: "El campo es obligatorio",
                correo: {
                    email: "El correo no es valido",
                    required: "El campo es obligatorio",
                    correosociovalidar: "El correo no esta asociado con el ITA",
                },
                nombre: {
                    minlength: "Debe contener un minimo de 3 digitos",
                    maxlength: "Debe contener un maximo de 25 digitos",
                    required: "El campo es obligatorio"
                },
                apellido: {
                    minlength: "Debe contener un minimo de 3 digitos",
                    maxlength: "Debe contener un maximo de 25 digitos",
                    required: "El campo es obligatorio"
                },
                segundo_nombre: {
                    minlength: "Debe contener un minimo de 3 digitos",
                    maxlength: "Debe contener un maximo de 25 digitos"
                },
                segundo_apellido: {
                    minlength: "Debe contener un minimo de 3 digitos",
                    maxlength: "Debe contener un maximo de 25 digitos"
                },
                sexo: "El campo es obligatorio",
                username: {
                    minlength: "Debe contener un minimo de 6 digitos",
                    maxlength: "Debe contener un maximo de 12 digitos",
                    required: "El campo es obligatorio"
                },
                clave1: {
                    minlength: "Debe contener un minimo de 6 digitos",
                    maxlength: "Debe contener un maximo de 12 digitos",
                    required: "El campo es obligatorio"
                },
                clave2: {
                    minlength: "Debe contener un minimo de 6 digitos",
                    maxlength: "Debe contener un maximo de 12 digitos",
                    equalTo: "La clave no coinciden",
                    required: "El campo es obligatorio"
                },
                contacto1: "El campo es obligatorio",
                estado: {
                    notEqual: "Debe seleccionar un estado",
                    required: "El campo es obligatorio"
                },
                municipios: {
                    notEqual: "Debe seleccionar un municipio",
                    required: "El campo es obligatorio"
                },
                parroquias: {
                    notEqual: "Debe seleccionar una parroquia",
                    required: "El campo es obligatorio"
                },
                lider: "El campo es obligatorio",
                socio_directo: "El campo es obligatorio"
                
            },
            highlight: function (element) {
                $(element).closest('.help-block').removeClass('valid');
                // display OK icon
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                // add the Bootstrap error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error');
                // set error class to the control group
            },
            success: function (label, element) {
                label.addClass('help-block valid');
                // mark the current input as valid and display OK icon
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
            }
        });
    };// fin de validaciones*/

    var displayConfirm = function () {
        $('.display-value', form).each(function () {
            var input = $('[name="' + $(this).attr("data-display") + '"]', form);
            if (input.attr("type") == "text" || input.attr("type") == "email" || input.is("textarea")) {
                $(this).html(input.val());
            } else if (input.is("select")) {
                $(this).html(input.find('option:selected').text());
            } else if (input.is(":radio") || input.is(":checkbox")) {

                $(this).html(input.filter(":checked").closest('label').text());
            } else if ($(this).attr("data-display") == 'card_expiry') {
                $(this).html($('[name="card_expiry_mm"]', form).val() + '/' + $('[name="card_expiry_yyyy"]', form).val());
            }
        });
    };
    var onShowStep = function (obj, context) {
    	if(context.toStep == numberOfSteps){
    		$('.anchor').children("li:nth-child(" + context.toStep + ")").children("a").removeClass('wait');
            displayConfirm();
    	}
        $(".next-step").unbind("click").click(function (e) {
            e.preventDefault();
            wizardContent.smartWizard("goForward");
        });
        $(".back-step").unbind("click").click(function (e) {
            e.preventDefault();
            wizardContent.smartWizard("goBackward");
        });
        $(".go-first").unbind("click").click(function (e) {
            e.preventDefault();
            wizardContent.smartWizard("goToStep", 1);
        });
        $(".finish-step").unbind("click").click(function (e) {
            e.preventDefault();
            wizardContent.smartWizard("goForward");
            onFinish(obj, context);

        });
    };
    var leaveAStepCallback = function (obj, context) {
        return validateSteps(context.fromStep, context.toStep);
        // return false to stay on step and true to continue navigation
    };
    var onFinish = function (obj, context) {

        if (validateAllSteps()) {

            registrar_socio();

            $('.anchor').children("li").last().children("a").removeClass('wait').removeClass('selected').addClass('done').children('.stepNumber').addClass('animated tada');
            //wizardForm.submit();
        }
    };
    var validateSteps = function (stepnumber, nextstep) {
        var isStepValid = false;
        
        
        if (numberOfSteps >= nextstep && nextstep > stepnumber) {
        	
            // cache the form element selector
            if (wizardForm.valid()) { // validate the form
                wizardForm.validate().focusInvalid();
                for (var i=stepnumber; i<=nextstep; i++){
        		$('.anchor').children("li:nth-child(" + i + ")").not("li:nth-child(" + nextstep + ")").children("a").removeClass('wait').addClass('done').children('.stepNumber').addClass('animated tada');
        		}
                //focus the invalid fields
                isStepValid = true;
               
                return true;
            };
        } else if (nextstep < stepnumber) {
        	for (i=nextstep; i<=stepnumber; i++){
        		$('.anchor').children("li:nth-child(" + i + ")").children("a").addClass('wait').children('.stepNumber').removeClass('animated tada');
        	}
            
            return true;

        } 

    };
    var validateAllSteps = function () {
        var isStepValid = true;
        // all step validation logic
        return isStepValid;
    };
    return {
        init: function () {
            initWizard();
        }
    };
}();

