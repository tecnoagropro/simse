function permite(elEvento, permitidos) {
    // Variables que definen los caracteres permitidos
    var numeros = "0123456789";
    var numeros_cifras = "0123456789,";
    var numeros_cifras_p = "0123456789,.";
    var numeros_tlf = " 0123456789-/";
    var caracteres = " abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZáéíóúÁÉÍÓÚ_-.,:@";
    var caracteres_s = "abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZáéíóúÁÉÍÓÚ";
    var numeros_caracteres = numeros + caracteres;
    var teclas_especiales = [8, 9, 13, 16, 18, 39];
    // 8 = BackSpace, 46 = punto, 39 = flecha derecha
    //var teclas_especiales = [8, 9, 13, 16, 18, 37, 39, 46, 64];
   
    // Seleccionar los caracteres a partir del parámetro de la función
    switch(permitidos) {
        case 'num':
            permitidos = numeros;
            break;
        case 'num_cifras':
            permitidos = numeros_cifras;
            break;
        case 'num_cifras_p':
            permitidos = numeros_cifras_p;
            break;
        case 'num_tlf':
            permitidos = numeros_tlf;
            break;
        case 'car':
            permitidos = caracteres;
            break;
        case 'car_s':
            permitidos = caracteres_s;
            break;
        case 'num_car':
            permitidos = numeros_caracteres;
            break;
    }
   
    // Obtener la tecla pulsada 
    var evento = elEvento || window.event;
    var codigoCaracter = evento.charCode || evento.keyCode;
    var caracter = String.fromCharCode(codigoCaracter);
   
    // Comprobar si la tecla pulsada es alguna de las teclas especiales
    // (teclas de borrado y flechas horizontales)
    var tecla_especial = false;
    for(var i in teclas_especiales) {
        if(codigoCaracter == teclas_especiales[i]) {
            tecla_especial = true;
            break;
        }
    }
   
    // Comprobar si la tecla pulsada se encuentra en los caracteres permitidos o si es una tecla especial
    
    return permitidos.indexOf(caracter) != -1 || tecla_especial;
}

function validar_ci_rif(elEvento) {
        // Obtener la tecla pulsada 
    var evento = elEvento || window.event;
    var codigoCaracter = evento.charCode || evento.keyCode;
    var caracter = String.fromCharCode(codigoCaracter);
    //alert(caracter)
    var numeros = "0123456789";

    letras = {
        v:'',
        V:'',
        e:'',
        E:'',
        j:'',
        J:'',
        g:'',
        G:'',
        p:'',
        P:''
    }

    if($("#ci_rif").val().length <= 11)
    {
        if ($("#ci_rif").val().substr(0, 1) in letras)
        {
            if(isNaN(caracter))
                return false;
            return true
        }
        else
        {
            if (caracter in letras)
            {

                return true;
            }
            else
            {
                return false;
            }

        }        
    }
    return false


}

$('#ci_rif').on('input',function(e){

    letras = {
        v:'',
        e:'',
        j:'',
        g:'',
        p:''
    }

      if ($("#ci_rif").val() in letras)
      {
        $("#ci_rif").val($("#ci_rif").val().toUpperCase() + '-'); 
      }
});