function templater ( strings, ...keys ) {
  return function( data ) {
      let temp = strings.slice();
      keys.forEach( ( key, i ) => {
          if(typeof(data[ key ]) !== 'undefined')
          {
            temp[ i ] = temp[ i ] + data[ key ];
          }
      });
      return temp.join( '' );
  }
};


function save_file(filename, data, type='text/html') {

/*	if (type=='text/plain')
	{
		if (typeof(data) == "object")
		{
			data = JSON.stringify(data);
		}
	}*/

    var blob = new Blob([data], {type: type});
    if(window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveBlob(blob, filename);
    }
    else{
        var elem = window.document.createElement('a');
        elem.href = window.URL.createObjectURL(blob);
        elem.download = filename;        
        document.body.appendChild(elem);
        elem.click();        
        document.body.removeChild(elem);
    }
}


var HDD = {
    get: function(key) {
        return localStorage.getItem(key);
    },
    set: function(key, val) {
        return localStorage.setItem(key, val);
    },
    unset: function(key) {
        return localStorage.removeItem(key);
    },
    setJ:function(key,val)
    {
        return localStorage.setItem(key, JSON.stringify(val));
    },
    getJ:function(key)
    {
        return JSON.parse(localStorage.getItem(key));
    },
    item:function(key)
    {
    	return;
    },
    push:function(hdd, val){
    	var lista = JSON.parse(localStorage.getItem(hdd));
    	lista.push(val);
    	var new_lista = JSON.stringify(lista);
    	localStorage.setItem(hdd, new_lista);
    	return new_lista;
    },
   	clear: function(){
	    for (var i = 0; i < localStorage.length; i++) {
	        HDD.unset(localStorage.key(i)); 
	    }

	    for(var x = 0; x <= localStorage.length; x++) {
	        localStorage.removeItem(localStorage.key(x))
	    }   		
   	}
};


var HSD = {
	init:function()
	{
		if ( typeof(window.DEBUG_SERTIVEN) === 'undefined' )
		{
			this.clear();
			window.DEBUG_SERTIVEN = true;
		}

	},
    get: function(key) {
        return sessionStorage.getItem(key);
    },
    set: function(key, val) {
        return sessionStorage.setItem(key, val);
    },
    unset: function(key) {
        return sessionStorage.removeItem(key);
    },
    setJ:function(key,val)
    {
        return sessionStorage.setItem(key, JSON.stringify(val));
    },
    getJ:function(key)
    {
        return JSON.parse(sessionStorage.getItem(key));
    },
    push:function(hdd, val){
    	this.init();
   	    var lista = JSON.parse(sessionStorage.getItem(hdd));
   	    if (lista == null)
   	    {
   	    	var lista = [];
   	    }
   	    var md = new Date();
   	    md.setTime( md.getTime() - md.getTimezoneOffset()*60*1000 );
    	lista.push('\n ' + md.toJSON() + ' ' + val);
    	var new_lista = JSON.stringify(lista);
    	sessionStorage.setItem(hdd,new_lista);
    	return new_lista;
    },
   	clear: function(){
		sessionStorage.clear();	    		
   	} 
};


var utiles=function() {

	function log(cajon, objeto)
	{

		if (typeof(objeto) === "object")
		{
			objeto = JSON.stringify(objeto);
		}

		HSD.push('LOG_'+cajon,objeto);
	};

	function salvar_consola()
	{

		var template_log = 
		templater`
		<html>
		<link href="style.css" rel="stylesheet" type="text/css">
		${'pre_variables'}
		<pre>${'variables'}</pre>
		</div>${'pre_ajax'}
		    <pre>${'ajax'}</pre>
		</div>${'pre_registros'}
		    <pre>${'registros'}</pre>
		</div><p>
		</html>`;

		var dump_data = {}
		if (HSD.getJ('LOG_variables')){
			mergeObject(dump_data, {
            'variables':HSD.getJ('LOG_variables'),
            'pre_variables':"<div>Variables:<br>"
        	})
		}
		if (HSD.getJ('LOG_ajax')){
			mergeObject(dump_data, {
            'ajax':HSD.getJ('LOG_ajax'),
            'pre_ajax':"<p><div>Ajax:<br>"
        	})
		}
		if (HSD.getJ('LOG_registros')){
			mergeObject(dump_data, {
            'registros':HSD.getJ('LOG_registros'),
            'pre_registros':"<p><div>Registros:<br>"
        	})
		}
						
		var html = template_log(dump_data);
        save_file('log.html', html);
	}

	function comparar_objeto(obj1, obj2) {
		return JSON.stringify(obj1) === JSON.stringify(obj2);
	};

	function si_esta(a,b) {
		// si a esta en b
		for (var i = 0; i < b.length; i++) {
			if (a == b[i]){
				return true;
			};
		};

		return false;
		
	}

	function validar_ci_rif(valor) {
		//formato=/^([VEG]{1})-([0-9]{8})-([0-9]{1})$/
		formato = /^[JGVEP][-][0-9]{8}[-][0-9]{1}$/
		return formato.test(valor)
	}

	function formato_rif(valor) {
		formato = /^[0-9]{8}$/
		letras = {
			v:'',
			V:'',
			e:'',
			E:'',
			j:'',
			J:'',
			g:'',
			G:'',
			p:'',
			P:''
		}

		if (valor in letras)
		{
			return valor.toUpperCase() + '-';
		}
		else
		{
			if(valor.length < 2)
				return ""

			if(isNaN(valor.substr(0, valor.length)))
			{
				return valor.substr(0, valor.length);
			}
			else
			{
				return valor
				
			}
			
		}
		
	}
	return {
		comparar_objeto:comparar_objeto,
		log:log,
		ci_rif:formato_rif,
		si_esta:si_esta,
		salvar_consola:salvar_consola
	}
}();
// fin
    
function eliminar_sms_div()
{
	$('#mensaje_ajax').addClass('hide');
}


function get_jwt() {

	if (HDD.get('jwt_date'))
	{
		s = segundos_pasados();
		if (s & s<43000){//tiempo viene de conf django
			if (HDD.get(settings.jwt_name)){
				return HDD.get(settings.jwt_name);
			}		
		}else{
			//que hacer cuando se vence el token
			
		}
	}

	if (HDD.get(settings.jwt_name)){
		return HDD.get(settings.jwt_name);
	}	

	$.ajax({
		type: "POST", //GET, POST, PUT
		url: '/api/v1/auth/',  //the url to call
		data: {
			username: $("#username").val(),
			password: $("#password").val()
		},
	}).done(function (response) {
		HDD.set(settings.jwt_name,response.token);
		HDD.set('jwt_date',new Date());
		HDD.unset('bloqueo_pantalla')
		return response.token;
	}).fail(function (request, status, error)  {
		console.log("Error: " + request.responseText);
	});  
}


function segundos_pasados() {
    var a = new Date();
    if (HDD.get('jwt_date')){
	    var b = new Date(HDD.get('jwt_date'));
	    //La diferencia se da en milisegundos así que debes dividir entre 1000
	    var c = ((a-b)/1000);
	    return c;    	
    }
    return false;
}

function LOG(registro) {
	return true;
/*	if (HDD.get('LOG')){
		json_log = JSON.parse(HDD.get('LOG'))
		json_log[0].push(JSON.stringify([[new Date(),registro]]))
		HDD.set('LOG',json_log);
	}
	else{
		HDD.set('LOG',JSON.stringify([]));
		json_log = JSON.parse(HDD.get('LOG'))
		json_log.push(JSON.stringify([[new Date(),registro]]))
		HDD.set('LOG',json_log);		
	}*/
}

function get_foto_perfil() {

	if (HDD.get('url_perfil'))
	{
		$("#img_perfil").attr('src', HDD.get('url_perfil'));
		$("#img_perfil_small").attr('src', HDD.get('url_perfil'));
	}
	else
	{
		$.ajax({
		      type: "GET", //GET, POST, PUT
		      url: '/api/v1/users/persona/completo',  //the url to call
		      beforeSend: function (xhr) {   //Include the bearer token in header
		          xhr.setRequestHeader("Authorization", 'JWT '+ HDD.get(settings.jwt_name));
		      }
		  }).done(function (response) {
		  	$("#img_perfil").attr('src','/media/'+response.avatar);
		  	$("#img_perfil_small").attr('src', '/media/'+response.avatar);
		  	HDD.set('nombre_apellido',response.primer_nombre+ ' ' + response.primer_apellido);

		  	if (response.avatar != '')
		  	{
		  		HDD.set('url_perfil','/media/'+response.avatar)
		  	}
		  	else
		  	{
		  		$("#text_perfil > img").remove();
				iniciales_array = HDD.get('nombre_apellido').split(' ');
		  		$("#text_perfil").text(iniciales_array[0].substr(0,1) + iniciales_array[1].substr(0,1));

		  		$("#text_perfil_small > img").remove();
				iniciales_array = HDD.get('nombre_apellido').split(' ');
		  		$("#text_perfil_small").text(iniciales_array[0].substr(0,1) + iniciales_array[1].substr(0,1));
		  	}
		  	//

		  }).fail(function (err)  {
		      //Error during request
		}); 		
	}

}

function refrescar_token() {
	$.ajax({
      type: "POST", //GET, POST, PUT
      url: '/api/v1/auth/refresh/',  //the url to call
      beforeSend: function (xhr) {   //Include the bearer token in header
          xhr.setRequestHeader("Authorization", 'jwt '+ HDD.get(settings.jwt_name));
      },
      data:{
      	token: HDD.get(settings.jwt_name)
      }
	}).done(function (response) {
		HDD.set(settings.jwt_name,response.token);
		HDD.set('jwt_date',new Date());
		HDD.unset('bloqueo_pantalla')
		console.log('Refrecado token')
		utiles.log('registros','Refrecado token'+ JSON.stringify(response.token));
		return response.token;
	}).fail(function (err)  {
	  //Error during request
	});
}

function verifyJwtToken(done_function, fail_function) {
	$.ajax({
      type: "POST", //GET, POST, PUT
      url: '/api/v1/auth/verify/',  //the url to call
      beforeSend: function (xhr) {   //Include the bearer token in header
          xhr.setRequestHeader("Authorization", 'jwt '+ HDD.get(settings.jwt_name));
      },
      data:{
      	token: HDD.get(settings.jwt_name)
      }
	}).done(function (response) {
		done_function();
	}).fail(function (err)  {
	  	fail_function();
	}); 

}


function get_error_api(respuesta_api,campos) {
	
	if (respuesta_api.errors){
		mensajes = ''
		try {
			errs = respuesta_api.errors.errores;

			for (var j = 0; j < errs.length; j++) {
				mensajes = errs[j] + '\n';
			}
		}
		catch(err) {
			for (var i = 0; i < campos.length; i++) {
				campo = 'respuesta_api.errors.' + campos[i];

				try {
					lista = eval(campo);
					for (var j = 0; j < lista.length; j++) {
						mensajes = mensajes + ' ' + campos[i].toUpperCase() + ': ' +lista[j] + '\n';
					}
				}
				catch(err) {
				    
				}
			}
		}

		return mensajes
	}

	if (respuesta_api.message){
		return respuesta_api.message;
	}

}

function get_usuario_conectado() {

	if (HDD.get('bloqueo_pantalla')){
		if (window.location.pathname != '/bloqueo/')
		{
			location.href = '/bloqueo/'
		}
	}

	recorrerMenu()
	
    if (HDD.get(settings.jwt_name)){

    	get_foto_perfil();
    
    	if (HDD.get('username')){
    		$("#nombre_usuario").text(HDD.get('username'))
    		$("#nombre_apellido").text(HDD.get('nombre_apellido'))
    		$("#rol_navbar").text(HDD.get('rol_navbar'));
    		return true;
    	}

	    $.ajax({
	          type: "GET", //GET, POST, PUT
	          url: '/api/v1/users/',  //the url to call
	          beforeSend: function (xhr) {   //Include the bearer token in header
	              xhr.setRequestHeader("Authorization", 'JWT '+ HDD.get(settings.jwt_name));
	          }
	      }).done(function (response) {
				$("#nombre_usuario").text(response.username)
				HDD.set('roles', JSON.stringify(response.roles))
				HDD.set('username',response.username);
				HDD.set('idpersona',response.id);
			  	
			  	if (response.roles.includes('encargado'))
			  	{
			  		HDD.set('rol_navbar','Encargado');
                    HDD.set('encargado','si');
			  	}
			  	if (response.roles.includes('administrador'))
			  	{
			  		HDD.set('rol_navbar','Admin');
                    HDD.set('administrador','si');
			  	}
			  	if (response.roles.includes('empleado'))
			  	{
			  		HDD.set('rol_navbar','empleado');
			  	}
	          	
			  	$("#rol_navbar").text(HDD.get('rol_navbar'));
	          	//setTimeout(function(){ recorrerMenu(); }, 200); 
	          
	      }).fail(function (err)  {
	    }); 


    }
    else
    {
    	location.href = '/'
    }
    
        
}

function cerrar_session(redirect=true, emitir=true) {
	HDD.unset('username');
	HDD.unset('bloqueo_pantalla');
	HDD.unset(settings.jwt_name);
	HDD.unset('nombre_usuario');
	HDD.unset('nombre_apellido');
	HDD.unset('jwt_date');
	HDD.unset('url_perfil');
	HDD.unset('idpersona');
	HDD.unset('last_url');
	HDD.unset('tiempo_bloqueo');
	HDD.unset('roles');
    HDD.unset('administrador');
    HDD.unset('encargado');
    HDD.unset('rol_navbar');
    HDD.unset('encargado_id');

    if (emitir)
    {
    	try{
			var bc_salir = new BroadcastChannel('channel_salir');
			bc_salir.postMessage('Usuario cerro sesion.');
		}catch(err){
			return null;
		}
    }

    Cookies.remove(settings.jwt_name);

	if (redirect)
	{
		setTimeout(function(){ location.href = '/login'; }, 200);
	}
	
}

$("#btn_cerrar_session").on('click',function(e){
	cerrar_session();
})

var session_actual;
var ML = window.location.pathname;
var condicion_redireccion = ML != '/' && ML !='/bloqueo/' && ML !='/registro/' && ML !='/olvido/'
var tiempo_redireccion = parseInt(HDD.get('tiempo_bloqueo'))  ||  (1000*60)*10;

function ini() {
	if (false)
	{
		HDD.set('last_url',ML);
		session_actual = setTimeout('location="/bloqueo/"',tiempo_redireccion);
	}
}

function parar() {
    clearTimeout(session_actual);
	if (false)
	{  
		HDD.set('last_url',ML);
  		session_actual = setTimeout('location="/bloqueo/"',tiempo_redireccion);
  	}
}


$(document).ready(function(){
    // bloqueo pagina
    //ini();
    if (window.location.pathname != '/login/')
    {
	    try{
		    var bc_salir = new BroadcastChannel('channel_salir');
		    bc_salir.onmessage = function (ev) { cerrar_session(true,false) }
    	}catch(err){

    	}

    }
    
});

/*
bloqueo de la pagina
$(document).keypress(function(event) {
    parar();
});

$(document).on('click','body *',function(){
    parar();
});

*/
function get_permiso_usuario(permiso) {

	user_permisos = JSON.parse(HDD.get('roles'));
	if (user_permisos)
	{
		for (var i = 0; i < user_permisos.length; i++) {

			for (var j = 0; j < permiso.length; j++) {
				if (user_permisos[i].includes(permiso[j])) {
					return true;
				} 
			}
			
		}		
	}
	return false;
}

function menu(icono, enlace, nombre_menu, id, permiso=null) {
	try{
		var enlace_menu='<li id="'+ id +'" class="menu"><a href="'+ enlace +'"><i class="'+ icono +'"></i> <span class="nav-label">'+ nombre_menu +'</span></a></li>';
		if (permiso)
		{
			if (get_permiso_usuario(permiso)) return enlace_menu;
			return null;
		}
		else
		{
			return enlace_menu;
		}
	}catch(err){
		return null;
	}
}

function recorrerMenu() {
	var list = [];

	list.push(menu(settings.icono_dashboard,'#/encargados/','Inicio','menu_inicio',['administrador']))
	list.push(menu('fa fa-user','#/encargado/listar','Encargados','menu_encargado',['administrador']))
	list.push(menu('fa fa-table','#/inventario/listar','Inventario','menu_inventario',['administrador','encargado']))
	list.push(menu('fa fa-check-double','#/encargado/ventas','Vender','menu_venta',['encargado']))
	list.push(menu('fa fa-check-double','#/encargado/ventas/listar/' + HDD.get('encargado_id'),'Mis ventas','menu_ventas',['encargado']))
	list.push(menu('fa fa-check-double','#/encargado/ventas/listar','Ventas','menu_ventas',['administrador']))
	list.push(menu('fa fa-chart-bar','#/reporte/cierre','Reportes de cierre','menu_reporte',['encargado']))
	list.push(menu('fa fa-users','#/encargado/clientes/listar/' + HDD.get('encargado_id'),'Mis clientes','menu_clientes',['encargado']))
    list.push(menu('fa fa-male','#/asignar_persona/','Asignar Personas','menu_asinar',['administrador']))
    list.push(menu('fa fa-chart-bar','#/encargado/ventas/reporte','Reporte','menu_reporte_encargado',['encargado']))
    list.push(menu('fa fa-chart-bar','#/encargado/ventas/reporte_metodo','Reporte Metodo','menu_reporte_metodo_encargado',['encargado']))
    list.push(menu('fa fa-chart-bar','#/reporte/listar','Reportes','menu_reportes',['administrador']))
	list.push(menu('fa fa-users','#/encargado/clientes/listar','Clientes','menu_clientes',['administrador']))
    list.push(menu('fa fa-wrench','#/utilidades','Utilidades','menu_utilidades',['administrador','encargado']))

    $("#side-menu").empty()
	for (var i = 0; i < list.length; i++) {
		
		$("#side-menu").append(list[i]);
	}
}

function paises() {
	// Se trae la lista de paises
	$("#pais").append($(document.createElement('option')).text('Paises'));
    $.ajax({
    type: "GET", //GET, POST, PUT
    url: '/api/v1/paises/',  //the url to call
    }).done(function ( data, textStatus, jqXHR ) {
      respuesta = jqXHR.responseJSON 

      for (var i = 0; i < respuesta.length; i++) {
          var option = $(document.createElement('option'));
            option.text(respuesta[i].pais);
            option.val(respuesta[i].id);
            $("#pais").append(option);
      }
      return true;

    }).fail(function (jqXHR, textStatus, data )  {
      return false;
    }); 
}


function documentos(){
	// Se trae la lista de tipo documento
	$("#tipo_documento").append($(document.createElement('option')).text('Tipos'));
    $.ajax({
	    type: "GET", //GET, POST, PUT
	    url: '/api/v1/documentos/',  //the url to call
	    beforeSend: function (xhr) {   //Include the bearer token in header
	      xhr.setRequestHeader("Authorization", 'JWT '+ HDD.get(settings.jwt_name));
	    },
    }).done(function ( data, textStatus, jqXHR ) {
      	respuesta = jqXHR.responseJSON 

      	for (var i = 0; i < respuesta.tipo.length; i++) {
          	var option = $(document.createElement('option'));
			option.text(respuesta.tipo[i].id);
			option.val(respuesta.tipo[i].id);
			$("#tipo_documento").append(option);
      	}
      	return true;

    }).fail(function (jqXHR, textStatus, data )  {

      return false;
    }); 
}



function BASE_GET_LIST(objeto) {

	if (!(typeof objeto.id === 'undefined')){
		id = objeto.id;
	}

	if (!(typeof objeto.url === 'undefined')){
		url = objeto.url;
	}	

	if (!(typeof objeto.label === 'undefined')){
		label = objeto.label;
	}else{
		label = 'nombre';
	}

	if (!(typeof objeto.caption === 'undefined')){
		caption = objeto.caption;
	}else{
		caption = 'Por favor seleccione';
	}

	$("#"+id).empty();
	$("#"+id).append($(document.createElement('option')).text(caption));

    $.ajax({
	    type: "GET", //GET, POST, PUT
	    url: url,  //the url to call
	    beforeSend: function (xhr) {   //Include the bearer token in header
	      xhr.setRequestHeader("Authorization", 'JWT '+ HDD.get(settings.jwt_name));
	    },
    }).done(function ( data, textStatus, jqXHR ) {
	    respuesta = jqXHR.responseJSON;

	    if (!(typeof objeto.antes_success === 'undefined')){
			objeto.antes_success(respuesta);
		}

	    for (var i = 0; i < respuesta.length; i++) {

	        var option = $(document.createElement('option'));
	        option.text(eval("respuesta[i]." + label));
	        option.val(respuesta[i].id);

	        ////preseleccionar
	        if (!(typeof objeto.seleccionar === 'undefined')){
                if (objeto.seleccionar==respuesta[i].id){
                    option.attr('selected', 'selected')
                }
			}

	        $("#"+id).append(option);

	    }

	    if (!(typeof objeto.despues_success === 'undefined')){
			objeto.despues_success(respuesta);
		}	    
	    

	    return true;

    }).fail(function (jqXHR, textStatus, data )  {

     	return false;
    }); 

}



function monedas(...args){

	obj = {
		id: "monedas",
		url: "/api/v1/monedas/",
		caption: "Seleccione una moneda",
		label: "simbolo"
	}

	BASE_GET_LIST(obj);	
}

function gastos(...args){

	obj = {
		id: "gasto",
		url: "/api/v1/gasto/",
		caption: "Seleccione un gasto",
		label: "nombre"
	}

	BASE_GET_LIST(obj);	
}

function productos(...args){

	obj_p = {
		id: "productos",
		url: "/api/v1/producto/",
		caption: "Seleccione un producto"
	}

	BASE_GET_LIST(obj_p);	
}

function productos_con_stock(...args){

	obj_p = {
		id: "productos",
		url: "/api/v1/producto/con_stock/",
		caption: "Seleccione un producto"
	}

	BASE_GET_LIST(obj_p);	
}

function bancos(){

	var objeto_to_banco = {
		id: "bancos",
		url: "/api/v1/banco/",
		caption: "Seleccione un banco",
		label: 'nombre'
	}

    $.ajax({
	    type: "GET", //GET, POST, PUT
	    url: objeto_to_banco.url,  //the url to call
	    beforeSend: function (xhr) {   //Include the bearer token in header
	      xhr.setRequestHeader("Authorization", 'JWT '+ HDD.get(settings.jwt_name));
	    },
    }).done(function ( data, textStatus, jqXHR ) {
	    respuesta = jqXHR.responseJSON;

	    for (var i = 0; i < respuesta.length; i++) {

	        var option = $(document.createElement('option'));
	        option.text(eval("respuesta[i]." + objeto_to_banco.label));
	        option.val(respuesta[i].id);

	        ////preseleccionar
	        if (!(typeof objeto_to_banco.seleccionar === 'undefined')){
                if (objeto_to_banco.seleccionar==respuesta[i].id){
                    option.attr('selected', 'selected')
                }
			}

	        $("#"+objeto_to_banco.id).append(option);

	    }
	    

	    return true;

    }).fail(function (jqXHR, textStatus, data )  {
     	return false;
    });
}


function metodos_pagos(){

	var objeto_to_metodos_pagos = {
		id: "tipo_pago",
		url: "/api/v1/metodo_pago/",
		caption: "Cuentas por pagar",
		label: "metodo"
	}

    $.ajax({
	    type: "GET", //GET, POST, PUT
	    url: objeto_to_metodos_pagos.url,  //the url to call
	    beforeSend: function (xhr) {   //Include the bearer token in header
	      xhr.setRequestHeader("Authorization", 'JWT '+ HDD.get(settings.jwt_name));
	    },
    }).done(function ( data, textStatus, jqXHR ) {
	    respuesta = jqXHR.responseJSON;

	    for (var i = 0; i < respuesta.length; i++) {

	        var option = $(document.createElement('option'));
	        option.text(eval("respuesta[i]." + objeto_to_metodos_pagos.label));
	        option.val(respuesta[i].id);

	        ////preseleccionar
	        if (!(typeof objeto_to_metodos_pagos.seleccionar === 'undefined')){
                if (objeto_to_metodos_pagos.seleccionar==respuesta[i].id){
                    option.attr('selected', 'selected')
                }
			}

	        $("#"+objeto_to_metodos_pagos.id).append(option);

	    }

	    return true;

    }).fail(function (jqXHR, textStatus, data )  {
     	return false;
    });
}

function encargados(...args){

	var config = args.shift();
	if (config)
	{
		obj_enc = {
			id: "encargados",
			seleccionar: config.seleccionar,
			url: "/api/v1/encargado/",
			caption: "Seleccione un encargado",
			label: "nombre + ' - ' + respuesta[i].datos_personales.tipo_documento + ' ' + respuesta[i].datos_personales.numero_documento"
		}		
	}
	else
	{
		obj_enc = {
			id: "encargados",
			url: "/api/v1/encargado/",
			caption: "Seleccione un encargado",
			label: "nombre + ' - ' + respuesta[i].datos_personales.tipo_documento + ' ' + respuesta[i].datos_personales.numero_documento"
		}
	}

	BASE_GET_LIST(obj_enc);
}

function empleados(...args){

	obj = {
		id: "empleados",
		url: "/api/v1/empleado/",
		caption: "Seleccione un empleado",
		label: "datos_personales.primer_nombre + ' - ' + respuesta[i].datos_personales.tipo_documento + ' ' + respuesta[i].datos_personales.numero_documento"
	}

	BASE_GET_LIST(obj);
}

function razones_no_trabajo(...args){

	obj = {
		id: "razones",
		url: "/api/v1/razones_no_trabajo/",
		caption: "Seleccione un razón",
		label: "razon"
	}

	BASE_GET_LIST(obj);	

}

function stock_para_encargado(...args){

	id = args.shift();
	const temporada = (typeof args[args.length-2] === 'string') ? args[args.length-2] : null;
	const despues_success = (typeof args[args.length-1] === 'function') ? args.pop() : undefined;

	if (temporada)
	{
		var url = "/api/v1/encargado/" + id + "/stocks/" + temporada + "/";
	}
	else
	{
		var url = "/api/v1/encargado/" + id + "/stocks/";
	}

	obj = {
		despues_success: despues_success,
		id: "producto",
		url: url,
		caption: "Seleccione un producto",
		label: "datos_producto.nombre"
	}

	BASE_GET_LIST(obj);	
}


function asociar_empleado_encargado(respuesta,sms="Se registro el empleado con exito") {

	var mydata = {}

    if (get_permiso_usuario(['administrador']))
    {

		$.ajax({
		    type: "POST", //GET, POST, PUT, PATCH 
		    url:   '/api/v1/empleado/'+respuesta.id+'/encargado/', 
		    beforeSend: function (xhr) {   //Include the bearer token in header
		    	xhr.setRequestHeader("Authorization", 'JWT '+ HDD.get(settings.jwt_name));
		    },             
		    data:{
	    		encargado: $("#encargados").val(),
	       	},
		    complete: function (Req, textStatus)
	        {
		        var res = JSON.parse(Req.responseText)

		        if (Req.status === 201) 
		        {
		          swal({  
		            title: "",
		            text: sms,
		            type: "success",
		            showCancelButton: false,
		            confirmButtonColor: "#23c6c8",
		            confirmButtonText: "Cerrar",
		            closeOnConfirm: false
		            }, function () {
		                location.href = '/empleado/listar';
		          });
		        }
		    }// fin complete
		}) //fin ajax    	    
    }else{
		$.ajax({
		   type: "POST", //GET, POST, PUT, PATCH 
		   url:   '/api/v1/empleado/'+respuesta.id+'/encargado/', 
		   beforeSend: function (xhr) {   //Include the bearer token in header
		     xhr.setRequestHeader("Authorization", 'JWT '+ HDD.get(settings.jwt_name));
		   },             
		   data:mydata,
		   complete: function (Req, textStatus)
	       {
		        var res = JSON.parse(Req.responseText)

		        if (Req.status === 201) 
		        {
		          swal({  
		            title: "",
		            text: "Se registro el empleado con exito",
		            type: "success",
		            showCancelButton: false,
		            confirmButtonColor: "#23c6c8",
		            confirmButtonText: "Cerrar",
		            closeOnConfirm: false
		            }, function () {
		                location.href = '/empleado/listar';
		          });
		        }
		   }// fin complete
		}) //fin ajax    	
    }


}

/*
function empleados_para_encargado(...args){

	const id = (typeof args[0] === 'string') ? args.shift() : null;	
	const despues_success = (typeof args[args.length-1] === 'function') ? args.pop() : null;	

	BASE_GET_LIST({
		despues_success: despues_success,
		id: "empleados",
		url: "/api/v1/encargado/" + id + "/empleados/",
		caption: "Seleccione un empleado",
		label: "datos_personales.primer_nombre + ' - ' + respuesta[i].datos_personales.primer_apellido"
	});	
}*/

function empleados_para_encargado(...args) {
	// Se trae la lista de paises

	id = args.shift();
	const callback = (typeof args[args.length-2] === 'function') ? args[args.length-2] : null;
	const config = (typeof args[args.length-1] === 'object') ? args[args.length-1] : null;	

	$("#empleados").append($(document.createElement('option')).text('Seleccione un empleado'));


    $.ajax({
    type: "GET", //GET, POST, PUT
    beforeSend: function (xhr) {   //Include the bearer token in header
      xhr.setRequestHeader("Authorization", 'JWT '+ HDD.get(settings.jwt_name));
    },  
    url: '/api/v1/encargado/'+id+'/empleados/',  //the url to call
    }).done(function ( data, textStatus, jqXHR ) {
      respuesta = jqXHR.responseJSON 

      for (var i = 0; i < respuesta.length; i++) {
        var option = $(document.createElement('option'));
        option.text(respuesta[i].datos_personales.primer_nombre + " - " + respuesta[i].datos_personales.tipo_documento + " " + respuesta[i].datos_personales.numero_documento);
        option.val(respuesta[i].id);

        ////preseleccionar
        if (config)
        {
	        if (!(typeof config.seleccionar === 'undefined')){
	            if (config.seleccionar==respuesta[i].id){
	                option.attr('selected', 'selected')
	            }
			}        	
        }


        $("#empleados").append(option);
      }

      callback(respuesta);
      
      return true;

    }).fail(function (jqXHR, textStatus, data )  {
      return false;
    }); 
}


function mergeObject(obj, src) {
  // afecta al objeto inicial
  Object.keys(src).forEach(function(key) { obj[key] = src[key]; });
  return obj;
}

function encargado_de_empleado(empleado_id,callback) {
	$.ajax({
	type: "GET", //GET, POST, PUT
	url: '/api/v1/empleado/'+empleado_id+'/encargado/',
	beforeSend: function (xhr) {   //Include the bearer token in header
              xhr.setRequestHeader("Authorization", 'JWT '+ HDD.get(settings.jwt_name));
    }}).done(function ( data, textStatus, jqXHR ) {
		respuesta = jqXHR.responseJSON 
		callback(respuesta.encargado);

	}); 
}


(function($) {
    $.parametrosUrl = (function(a) {
        if (a == "") return {};
        var b = {};
        for (var i = 0; i < a.length; ++i)
        {
            var p=a[i].split('=');
            if (p.length != 2) continue;
            b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
        }
        return b;
    })(window.location.search.substr(1).split('&'))
})(jQuery);


function _info(mensaje) {
    swal({
        title: "",
        text: mensaje,
        type: "info",
        showCancelButton: false,
        confirmButtonColor: "#23c6c8",
    });
};

function _alerta(mensaje) {
    swal({
        title: "",
        text: mensaje,
        showCancelButton: false,
        confirmButtonColor: "#23c6c8",
        type: "warning",
    });
}

function _error(mensaje) {
    swal({
        title: "Error",
        text: mensaje,
        type: "error",
        showCancelButton: false,
        confirmButtonColor: "#23c6c8",
    });
};

function _error_redirect(mensaje,redirect,titulo='Error') {
    swal({  
        title: titulo,
        text: mensaje,
        type: "error",
        showCancelButton: false,
        confirmButtonColor: "#23c6c8",
        confirmButtonText: "Cerrar",
        closeOnConfirm: false
        }, function () {
            location.href = redirect;
    });
};
;

function _error_reload(mensaje) {
    swal({  
        title: 'Error',
        text: mensaje,
        type: "error",
        showCancelButton: false,
        confirmButtonColor: "#23c6c8",
        confirmButtonText: "Cerrar",
        closeOnConfirm: false
        }, function () {
            location.reload();
    });
};

function _exito(mensaje) {
	swal({
	title: "Informacion",
		text: mensaje,
		type: "success",
		showCancelButton: false,
		confirmButtonColor: "#23C6C8",
		confirmButtonText: "Cerrar",
		closeOnConfirm: false,
	});
};

function _exito_redirect(mensaje,dire) {
	swal({
	title: "Informacion",
		text: mensaje,
		type: "success",
		showCancelButton: false,
		confirmButtonColor: "#23C6C8",
		confirmButtonText: "Cerrar",
		closeOnConfirm: false
	}, function () {
		document.location = dire;
	});
};

function _exito_reload(mensaje) {
	swal({
	title: "Informacion",
		text: mensaje,
		type: "success",
		showCancelButton: false,
		confirmButtonColor: "#23C6C8",
		confirmButtonText: "Cerrar",
		closeOnConfirm: false
	}, function () {
		location.reload();
	});
};



function _confirma(mensaje,si,no) {
    swal({
        title: "Informacion",
        text: mensaje,
        type: "success",
        showCancelButton: true,
        confirmButtonColor: '#E60026',
        confirmButtonText: 'Si!',
        confirmButtonColor: '#0070B8',
        cancelButtonText: "No!",
        closeOnConfirm: false,
        closeOnCancel: false
    },
    function(isConfirm) {

        if (isConfirm) {
        	si();
        } else {
           	no();
        }
    });
}


function guardar_nota(){
	$.ajax({
		type: "POST",
		url: '/api/v1/notas/',
		data:{
			'nota':$("#nota_del_usuario").val()
		},
		beforeSend: function (xhr) {
	        xhr.setRequestHeader("Authorization", 'JWT '+ HDD.get(settings.jwt_name));
	    }}).done(function ( data, textStatus, jqXHR ) {
			_exito('Nota guardada');
			$("#modal_pagina").modal('hide');
	}); 
}


$("#modal_debug").on("show.bs.modal",function(){
	var template_log_modal = 
	templater`
	<div>Variables:<br>
	    <pre>${'variables'}</pre>
	</div><p><div>Ajax:<br>
	    <pre>${'ajax'}</pre>
	</div><p><div>Registros:<br>
	    <pre>${'registros'}</pre>
	</div><p>
	`;
	var html = template_log_modal({
        'variables': HSD.getJ('LOG_variables'),
        'ajax':HSD.getJ('LOG_ajax'),
        'registros':HSD.getJ('LOG_registros'),
    });
	$("#div_para_debug").html(html);
})


function generar_qrs_debug() {
	$("#div_para_debug").html('<div id="qr1"></div><div id="qr2"></div><div id="qr3"></div>');
	var txt = HSD.getJ('LOG_variables');
	txt = JSON.stringify(txt).replace(/^[\s\u3000]+|[\s\u3000]+$/g, '');
	console.log(txt)
	$("#qr1").html(create_qrcode(txt));
}


////////////
/* APP    */
////////////

var handleAjaxMode = function(setting) {
	var emptyHtml = (setting.emptyHtml) ?  setting.emptyHtml : '<div class="p-t-40 p-b-40 text-center f-s-20 content"><i class="fa fa-warning fa-lg text-muted m-r-5"></i> <span class="f-w-600 text-inverse">Pagina no encontrada</span></div>';
	var defaultUrl = (setting.ajaxDefaultUrl) ? setting.ajaxDefaultUrl : '';
	    defaultUrl = (window.location.hash) ? window.location.hash : defaultUrl;
	
	if (defaultUrl === '') {
		$('#content').html(emptyHtml);
	} else {
		renderAjax(defaultUrl, '', true);
	}
    
	function clearElement() {
		$('.jvectormap-label, .jvector-label, .AutoFill_border ,#gritter-notice-wrapper, .ui-autocomplete, .colorpicker, .FixedHeader_Header, .FixedHeader_Cloned .lightboxOverlay, .lightbox, .introjs-hints, .nvtooltip, #float-sub-menu').remove();
		if ($.fn.DataTable) {
			$('.dataTable').DataTable().destroy();
		}
		if ($('#page-container').hasClass('page-sidebar-toggled')) {
			$('#page-container').removeClass('page-sidebar-toggled');
		}
	}
	
	function checkPushState(url) {
		var targetUrl = url.replace('#','');
		var targetUserAgent = window.navigator.userAgent;
		var isIE = targetUserAgent.indexOf('MSIE ');
	
		if (isIE && (isIE > 0 && isIE < 9)) {
			window.location.href = targetUrl;
		} else {
			history.pushState('', '', '#' + targetUrl);
		}
	}
	
	function checkLoading(load) {
		if (!load) {
			if ($('#page-content-loader').length === 0) {
				$('body').addClass('page-content-loading');
				$('#content').append('<div id="page-content-loader"><span class="spinner"></span></div>');
			}
		} else {
			$('#page-content-loader').remove();
			$('body').removeClass('page-content-loading');
		}
	}

	function renderAjax(url, elm, disablePushState) {
		//Pace.restart();
		if (!disablePushState) {
			checkPushState(url);
		}		
		checkLoading(false);
		verifyJwtToken(

		function(){
			var targetContainer= '#content';
			var targetUrl 	   = url.replace('#','');
			var targetType 	   = (setting.ajaxType) ? setting.ajaxType : 'GET';
			var targetDataType = (setting.ajaxDataType) ? setting.ajaxDataType : 'html';
			if (elm) {
				targetDataType = ($(elm).attr('data-type')) ? $(elm).attr('data-type') : targetDataType;
				targetDataDataType = ($(elm).attr('data-data-type')) ? $(elm).attr('data-data-type') : targetDataType;
			}
			
			$.ajax({
				url: targetUrl,
				type: targetType,
				dataType: targetDataType,
				success: function(data) {
					$(targetContainer).html(data);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					$(targetContainer).html(emptyHtml);
				}
			}).done(function() {
				checkLoading(true);
				$('html, body').animate({ scrollTop: 0 }, 0);
			});
		},
		function(){
			location.href = '/login/';
		}
		);
		clearElement();
		
	}
	
	$(window).on('hashchange', function() {
		if (window.location.hash) {
			renderAjax(window.location.hash, '', true);
		}
	});
	
	$(document).on('click', '[data-toggle="ajax"]', function(e) {
		e.preventDefault();
		renderAjax($(this).attr('href'), this);
	});
};


var App = function () {
	"use strict";
	
	var setting;
	
	return {
		//main function
		init: function (option) {
			if (option) {
				setting = option;
			}
			if (setting && setting.ajaxMode) {
				this.initAjax();
			}
		},
		initAjax: function() {
			handleAjaxMode(setting);
			$.ajaxSetup({
				cache: true
			});
		},
		setPageTitle: function(pageTitle) {
			document.title = pageTitle;
		},
		scrollTop: function() {
			$('html, body').animate({
				scrollTop: $('body').offset().top
			}, 0);
		}
  };
}();