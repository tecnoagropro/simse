$(document).ready(function(){
    cerrar_session(false,false);
    $('#login').validate({
        errorPlacement: function (error, element)
        {
            element.before(error);
        },
        rules: {
            username: {
                required: true,
                minlength: 4,
                maxlength: 40
            },
            password: {
                required: true,
                minlength: 4,
                maxlength: 40
            }
        },
        messages: {
            username: {
                required: "Campo obligatorio",
                minlength: "El minimo es de 4 caracteres",
                maxlength: "El maximo es de 40 caracteres"
            },
            password: {
               required: "Campo obligatorio",
               minlength: "El minimo es de 4 caracteres",
               maxlength: "El maximo es de 40 caracteres"
            }
        },
        submitHandler: function (_form)
        {
            $.ajax({
                type: "POST", //GET, POST, PUT
                url: '/api/v1/auth/',  //the url to call
                data: {
                    username: $("#username").val(),
                    password: $("#password").val()
                },
            }).done(function (response) {
                HDD.set(settings.jwt_name,response.token);
                Cookies.set(settings.jwt_name,response.token); 
                HDD.set('jwt_date', new Date());
                HDD.unset('bloqueo_pantalla')
                setTimeout(function(){ location.href = '/'; }, 140); 
            }).fail(function (request, status, error)  {
                console.log("Error: " + request.responseText);
                _error('El usuario o la contraseña es incorrecto');
            });  
  
          }
        //...
    });    
});