var POST = {
    metodo: 'POST',
    funciones: [200,201,400,401,403,404,500]
};

var POST_BULK = {
    metodo: 'POST',
    funciones: [200,201,400,401,403,404,500]
};

var GET = {
    metodo: 'GET',
    funciones: [200,400,401,403,404,500]
};

var PATCH = {
    metodo: 'PATCH',
    funciones: [200,400,401,403,404,500]
};

var PUT = {
    metodo: 'PUT',
    funciones: [200,400,401,403,404,500]
};

var DELETE = {
    metodo: 'DELETE',
    funciones: [204,400,401,403,404,500]
};

function _(conf, data){__(conf,data)};

function __(conf,data) {
if (typeof data.processData === 'undefined')
    {
        processData = true; 
    }
    else
    {
        processData = data.processData
    }
    
    if (typeof data.dataType === 'undefined')
    {
        dataType = null;
    }
    else
    {
        dataType = data.dataType
    }

    if (typeof data.contentType === 'undefined')
    {
        contentType = "application/x-www-form-urlencoded; charset=UTF-8";
    }
    else
    {
        contentType = data.contentType;
    }
    if (typeof data.beforeSend === 'undefined')
    {
        beforeSend = function (xhr) {   //Include the bearer token in header
          xhr.setRequestHeader("Authorization", 'JWT '+ HDD.get('tok_simse'));
        }       
    }
    else
    {
        beforeSend = data.beforeSend;
    }
    

    if (typeof data.caso_401 === 'undefined')
    {
        caso_401 = function(respuesta){
            alert("su session expiro o no esta autorizado")
        };
    }
    else
    {
        caso_401 = data.caso_401
    }

    $.ajax({
        type: conf.metodo,
        url:  data.url,
        data: data.parametros,
        dataType: dataType,
        contentType: contentType,
        beforeSend: beforeSend,
        processData: processData,
        complete: function (Req, textStatus)
        {
            var codigos_respuesta = [200,201,204,303,400,401,403,404,415,500,503,509];
            for (var i = 0; i < codigos_respuesta.length; i++) {
                if (Req.status === codigos_respuesta[i])
                {
                	if (Req.status === 204) data.caso_204();
                	
                	try{
	                	var res = JSON.parse(Req.responseText);
	                    if (conf.funciones.includes(codigos_respuesta[i]))
	                    {
	                        eval("data.caso_"+codigos_respuesta[i]+"(res)");
	                        return false;
	                    }else{
                            
                        }  
                	}catch(err){
						console.log('Respuesta no es json: ' + err);
						return false;
					}
          
                }            
            }
            _error('Fallo la plataforma, contacte a soporte');
        },//fin complete
    });
}