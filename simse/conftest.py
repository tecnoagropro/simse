import os
import pytest
from datetime import datetime

from django.conf import settings

from mixer.backend.django import mixer
from rest_framework.test import APIClient


VALID_MOBILE_NUMBER = "+17342564785"
VALID_MOBILE_NUMBER2 = "+17342564789"


@pytest.fixture
def recaptcha():
    os.environ['RECAPTCHA_TESTING'] = 'True'
    return {"g-recaptcha-response": "PASSED"}


@pytest.fixture
def user():
    user = mixer.blend(settings.AUTH_USER_MODEL, username="test", email="test@test.com")
    user.set_password("testing123")
    user.save()
    user.profile.mobile = VALID_MOBILE_NUMBER
    user.profile.save()
    return user


@pytest.fixture
def chrome_options(chrome_options):
    chrome_options.add_argument('--no-sandbox')
    return chrome_options


@pytest.fixture
def api_factory_authenticate(user):
    factory = APIClient()
    factory.force_authenticate(user)
    return factory

